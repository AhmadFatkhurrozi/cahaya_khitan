
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNeracaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('neraca', function (Blueprint $table) {
        $table->increments('id_neraca');
        $table->dateTime('tgl_neraca');
        $table->integer('kode_reff');
        $table->integer('users_id');
        $table->string('informasi');
        $table->double('debit');
        $table->double('kredit');
        $table->double('saldo')->nullable();
        $table->integer('cabang_id');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('neraca');
    }
}
