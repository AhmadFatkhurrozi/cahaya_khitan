<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_obat', function (Blueprint $table) {
            $table->bigIncrements('id_obat');
            $table->integer('cabang_id')->unsigned();
            $table->string('kode_obat');
            $table->string('nama_obat');
            $table->string('jenis');
            $table->integer('jumlah');
            $table->double('harga');
            $table->string('satuan');
            $table->integer('stok');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_obat');
    }
}
