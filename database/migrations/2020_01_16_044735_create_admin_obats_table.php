<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminObatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_obats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kode_obat');
            $table->string('nama_obat');
            $table->string('jenis_obat');
            $table->string('satuan_obat');
            $table->double('harga_obat');
            $table->integer('stok');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_obats');
    }
}
