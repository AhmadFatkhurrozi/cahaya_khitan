<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaftarMainansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_mainans', function (Blueprint $table) {
            $table->bigIncrements('id_mainan');
            $table->integer('cabang_id')->unsigned(());
            $table->string('kode_mainan');
            $table->string('nama_mainan');
            $table->integer('qty_mainan');
            $table->double('harga_mainan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_mainans');
    }
}
