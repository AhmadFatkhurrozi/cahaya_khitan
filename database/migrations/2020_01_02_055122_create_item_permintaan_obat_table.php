<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPermintaanObatTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_permintaan_obat', function (Blueprint $table) {            
            $table->bigIncrements('id_items');
            $table->integer('obat_id');
            $table->integer('permintaan_id');
            $table->integer('jumlah_permintaan');
            $table->integer('jumlah_acc')->nullable();
            $table->string('komentar');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_permintaan_obat');
    }
}
