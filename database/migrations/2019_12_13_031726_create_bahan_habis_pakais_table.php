<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBahanHabisPakaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bahan_habis_pakais', function (Blueprint $table) {
            $table->bigIncrements('id_bhp');
            $table->integer('kode_bhp')->nullable();
            $table->integer('cabang_id')->unsigned();
            $table->string('nama_barang');
            $table->integer('stok');
            $table->double('harga_satuan');
            $table->double('harga_grosir');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bahan_habis_pakais');
    }
}
