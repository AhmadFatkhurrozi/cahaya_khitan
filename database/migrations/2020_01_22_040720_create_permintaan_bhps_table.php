<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePermintaanBhpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('permintaan_bhps', function (Blueprint $table) {
            $table->bigIncrements('id_permintaan_bhp');
            $table->integer('users_id')->unsigned();
            $table->integer('cabang_id')->unsigned();
            $table->integer('bhp_id')->unsigned();
            $table->integer('qty');
            $table->string('keterangan');
            $table->string('status_permintaan');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('permintaan_bhps');
    }
}
