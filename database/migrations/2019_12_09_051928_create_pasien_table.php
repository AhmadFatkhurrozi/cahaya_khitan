<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePasienTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pasiens', function (Blueprint $table) {
            $table->bigIncrements('id_pasien');
            $table->string('nama');
            $table->date('tgl_lahir');
            $table->string('usia');
            $table->integer('bb');
            $table->string('alamat');
            $table->integer('metode_id');
            $table->string('riwayat_alergi');
            $table->string('riwayat_terdahulu');
            $table->string('nama_orangtua');
            $table->string('referensi');
            $table->date('tanggal');
            $table->string('jam');
            $table->string('tempat');
            $table->string('tempat_khitan');
            $table->integer('users_id')->unsigned();
            $table->string('bhp');
            $table->double('fee_operator');
            $table->double('fee_asisten');
            $table->string('gratis_biaya')->nullable();
            $table->string('keterangan');
            $table->string('status_khitan');
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pasiens');
    }
}
