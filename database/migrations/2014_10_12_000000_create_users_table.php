<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('username');
            $table->string('email');
            $table->string('password');
            $table->string('token')->nullable();
            $table->string('player_id')->nullable();
            $table->string('name');
            $table->string('gender')->nullable()->comment('Laki - Laki / Perempuan');
            $table->text('address')->nullable();
            $table->string('phone')->nullable();
            $table->text('photo')->nullable();
            $table->tinyInteger('level_user')
                  ->comment('1 : Admin; 2 : Owner; 3 : Kepala Cabang; 4 : Operator Cabang; 5 : Bendahara; 6 : Marketing');
            $table->integer('cabang_id')->unsigned();
            $table->tinyInteger('is_banned')->comment('0 : Active; 1 : Banned;');
            $table->timestamp('last_login')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });

        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@cahaya.com',
            'password' => bcrypt('admin'),
            'name' => 'Admin',
            'gender' => 'Laki - Laki',
            'address' => 'Mojokerto',
            'phone' => '081000000000',
            'level_user' => 1,
            'is_banned' => 0,

        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
