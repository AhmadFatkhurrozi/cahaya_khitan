<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetodeSunatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metode_sunats', function (Blueprint $table) {
            $table->bigIncrements('id_metode');
            $table->string('nama_metode');
            $table->double('harga_metode');
            $table->double('fee_operator');
            $table->double('fee_asisten');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metode_sunats');
    }
}
