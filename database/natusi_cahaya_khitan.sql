-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2020 at 06:54 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `natusi_cahaya_khitan`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_bhps`
--

CREATE TABLE `admin_bhps` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `stok_bhp` int(11) NOT NULL,
  `harga_satuan` double NOT NULL,
  `harga_grosir` double NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_bhps`
--

INSERT INTO `admin_bhps` (`id`, `nama_barang`, `stok_bhp`, `harga_satuan`, `harga_grosir`, `created_at`, `updated_at`) VALUES
(1, 'Sarung Tangan', 135, 2000, 1000, '2020-01-16 01:00:23', '2020-01-16 01:00:23'),
(2, 'Suntik', 150, 3000, 2500, '2020-01-16 01:02:16', '2020-01-16 02:07:57');

-- --------------------------------------------------------

--
-- Table structure for table `admin_obats`
--

CREATE TABLE `admin_obats` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kode_obat` varchar(255) NOT NULL,
  `nama_obat` varchar(255) NOT NULL,
  `jenis_obat` varchar(255) NOT NULL,
  `satuan_obat` varchar(255) NOT NULL,
  `harga_obat` double NOT NULL,
  `stok_obat` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_obats`
--

INSERT INTO `admin_obats` (`id`, `kode_obat`, `nama_obat`, `jenis_obat`, `satuan_obat`, `harga_obat`, `stok_obat`, `created_at`, `updated_at`) VALUES
(3, 'amox001', 'Amoxicillyn', 'Tablet', 'kaplet', 5000, 400, '2020-01-16 20:44:36', '2020-01-16 21:01:34'),
(4, 'SR0001', 'Sirup Rasa Jeruk', 'Sirup', 'botol', 15000, 50, '2020-02-04 00:38:31', '2020-02-04 00:38:31'),
(5, '901382', 'Bodrexin', 'Drop', 'tablet', 2000, 200, '2020-02-04 00:41:58', '2020-02-04 00:41:58');

-- --------------------------------------------------------

--
-- Table structure for table `akun`
--

CREATE TABLE `akun` (
  `no_reff` int(11) NOT NULL,
  `jenis_akun` int(11) NOT NULL,
  `nama_reff` varchar(40) NOT NULL,
  `keterangan` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `akun`
--

INSERT INTO `akun` (`no_reff`, `jenis_akun`, `nama_reff`, `keterangan`) VALUES
(111, 1, 'Kas', 'Kas'),
(113, 1, 'Perlengkapan', 'Mainan'),
(121, 1, 'Peralatan', 'BHP/ Alkes'),
(122, 1, 'Persediaan Obat', 'Obat'),
(311, 1, 'Modal', 'Modal'),
(411, 1, 'Pendapatan', 'Pendapatan'),
(511, 2, 'Beban Gaji', 'Beban Gaji'),
(513, 2, 'Beban Fee', 'Fee Operator');

-- --------------------------------------------------------

--
-- Table structure for table `bahan_habis_pakais`
--

CREATE TABLE `bahan_habis_pakais` (
  `id_bhp` bigint(20) UNSIGNED NOT NULL,
  `kode_bhp` int(10) UNSIGNED NOT NULL,
  `cabang_id` int(10) UNSIGNED NOT NULL,
  `nama_barang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `harga_satuan` double NOT NULL,
  `harga_grosir` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bahan_habis_pakais`
--

INSERT INTO `bahan_habis_pakais` (`id_bhp`, `kode_bhp`, `cabang_id`, `nama_barang`, `stok`, `harga_satuan`, `harga_grosir`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 'Sarung Tangan', 64, 2000, 1000, '2020-02-05 00:23:24', '2020-02-05 00:23:24'),
(4, 2, 1, 'Suntik', 49, 3000, 2500, '2020-02-07 02:32:23', '2020-02-07 02:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `biayas`
--

CREATE TABLE `biayas` (
  `id_biaya` bigint(20) UNSIGNED NOT NULL,
  `pasien_id` int(10) UNSIGNED NOT NULL,
  `metode_id` int(10) UNSIGNED NOT NULL,
  `obat_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bhp_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mainan_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `biayas`
--

INSERT INTO `biayas` (`id_biaya`, `pasien_id`, `metode_id`, `obat_id`, `bhp_id`, `mainan_id`, `created_at`, `updated_at`) VALUES
(41, 1, 1, '20, 23', '', 6, '2020-02-03 23:41:46', '2020-02-03 23:41:46'),
(42, 12, 1, '20, 23', '', NULL, '2020-02-04 01:36:10', '2020-02-04 01:36:10'),
(43, 13, 1, '20, 23', '3, 4', 6, '2020-02-07 02:32:49', '2020-02-07 02:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_cabangs`
--

CREATE TABLE `daftar_cabangs` (
  `id_cabang` bigint(20) UNSIGNED NOT NULL,
  `nama_cabang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `posisi` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `daftar_cabangs`
--

INSERT INTO `daftar_cabangs` (`id_cabang`, `nama_cabang`, `posisi`, `alamat`, `telepon`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 'cabang 1 mojokerto', 'Cabang', 'jl mojokerto no.11 empunala', '098765543', 'sudah aktif dan beroprasi sejak setahun lalu', '2020-01-07 19:08:38', '2020-01-07 19:08:38'),
(2, 'Cahaya Khitan Sidoarjo', 'Cabang', 'Sidoarjo', '-', '-', '2020-01-13 02:02:42', '2020-01-13 02:02:42'),
(3, 'Cahaya Khitan Kertosono', 'Cabang', 'Kertosono', '-', '-', '2020-01-13 02:03:09', '2020-01-13 02:03:09');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_mainans`
--

CREATE TABLE `daftar_mainans` (
  `id_mainan` bigint(20) UNSIGNED NOT NULL,
  `cabang_id` int(10) UNSIGNED NOT NULL,
  `kode_mainan` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_mainan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty_mainan` int(11) NOT NULL,
  `harga_mainan` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `daftar_mainans`
--

INSERT INTO `daftar_mainans` (`id_mainan`, `cabang_id`, `kode_mainan`, `nama_mainan`, `qty_mainan`, `harga_mainan`, `created_at`, `updated_at`) VALUES
(4, 2, 'reff_200122052039', 'Mainan Sidoarjo', 10, 20000, '2020-01-21 22:20:39', '2020-01-21 22:23:32'),
(5, 1, 'reff_200204052059', 'Kuda-kudaan', 48, 50000, '2020-02-03 22:20:59', '2020-02-03 22:20:59'),
(6, 1, 'reff_200204052157', 'Mobil-mobilan', 98, 25000, '2020-02-03 22:21:57', '2020-02-03 22:21:57');

-- --------------------------------------------------------

--
-- Table structure for table `fee_masters`
--

CREATE TABLE `fee_masters` (
  `id_fee` bigint(20) UNSIGNED NOT NULL,
  `cabang_id` int(10) UNSIGNED NOT NULL,
  `pasien_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fee_masters`
--

INSERT INTO `fee_masters` (`id_fee`, `cabang_id`, `pasien_id`, `created_at`, `updated_at`) VALUES
(10, 1, 1, '2020-02-03 23:41:46', '2020-02-03 23:41:46'),
(11, 1, 12, '2020-02-04 01:36:10', '2020-02-04 01:36:10'),
(12, 1, 13, '2020-02-07 02:32:49', '2020-02-07 02:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `master_gajis`
--

CREATE TABLE `master_gajis` (
  `id_gaji` bigint(20) UNSIGNED NOT NULL,
  `posisi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_level` tinyint(4) NOT NULL,
  `nominal_gaji` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_gajis`
--

INSERT INTO `master_gajis` (`id_gaji`, `posisi`, `users_level`, `nominal_gaji`, `created_at`, `updated_at`) VALUES
(14, 'Cabang', 4, 4000000, '2020-01-23 01:52:59', '2020-01-23 01:52:59'),
(15, 'Cabang', 5, 3500000, '2020-01-23 01:56:41', '2020-01-23 01:56:41'),
(16, 'Cabang', 6, 3000000, '2020-01-29 01:23:47', '2020-01-29 01:23:47'),
(17, 'Cabang', 3, 6000000, '2020-01-29 02:47:55', '2020-01-29 02:47:55'),
(18, 'Cabang', 2, 6000000, '2020-01-29 02:48:04', '2020-01-29 02:48:04');

-- --------------------------------------------------------

--
-- Table structure for table `master_obat`
--

CREATE TABLE `master_obat` (
  `id_obat` bigint(20) UNSIGNED NOT NULL,
  `cabang_id` int(10) UNSIGNED NOT NULL,
  `kode_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_obat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jumlah` int(11) NOT NULL,
  `harga` double NOT NULL,
  `satuan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_obat`
--

INSERT INTO `master_obat` (`id_obat`, `cabang_id`, `kode_obat`, `nama_obat`, `jenis`, `jumlah`, `harga`, `satuan`, `stok`, `created_at`, `updated_at`) VALUES
(20, 1, 'amox001', 'amoxicillyn', 'Tablet', 200, 5000, 'kotak', 244, '2020-01-07 20:17:46', '2020-01-07 20:17:46'),
(23, 1, 'bod002', 'bodrex', 'Tablet', 12, 80000, 'pack', 194, '2020-01-07 20:30:39', '2020-01-07 20:30:39'),
(25, 1, 'SR0001', 'Sirup Rasa Jeruk', 'Sirup', 25, 15000, 'botol', 50, '2020-02-04 22:22:48', '2020-02-04 22:22:48'),
(26, 2, 'amox001', 'Amoxicillyn', 'Tablet', 100, 5000, 'kaplet', 100, '2020-02-05 21:43:27', '2020-02-05 21:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `metode_sunats`
--

CREATE TABLE `metode_sunats` (
  `id_metode` bigint(20) UNSIGNED NOT NULL,
  `nama_metode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_metode` double NOT NULL,
  `fee_operator` double NOT NULL,
  `fee_asisten` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metode_sunats`
--

INSERT INTO `metode_sunats` (`id_metode`, `nama_metode`, `harga_metode`, `fee_operator`, `fee_asisten`, `created_at`, `updated_at`) VALUES
(1, 'Metode Stapler', 800000, 10000, 5000, '2020-01-07 19:14:19', '2020-01-07 19:14:19');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_12_09_051928_create_pasien_table', 1),
(3, '2019_12_09_052005_create_master_obat_table', 1),
(4, '2019_12_13_031521_create_metode_sunats_table', 1),
(5, '2019_12_13_031649_create_daftar_mainans_table', 1),
(6, '2019_12_13_031726_create_bahan_habis_pakais_table', 1),
(7, '2019_12_13_031744_create_daftar_cabangs_table', 1),
(8, '2019_12_16_031552_create_master_gajis_table', 1),
(9, '2019_12_17_044105_create_fee_operators_table', 1),
(10, '2019_12_30_020304_create_biayas_table', 1),
(11, '2020_01_02_034107_create_permintaan_obats_table', 1),
(12, '2020_01_02_055122_create_item_permintaan_obat_table', 1),
(13, '2020_01_02_080414_create_pengeluarans_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `neraca`
--

CREATE TABLE `neraca` (
  `id_neraca` int(10) UNSIGNED NOT NULL,
  `tgl_neraca` datetime NOT NULL,
  `kode_reff` int(11) NOT NULL,
  `users_id` int(11) DEFAULT NULL,
  `informasi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `debit` double NOT NULL,
  `kredit` double NOT NULL,
  `saldo` double DEFAULT NULL,
  `cabang_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `neraca`
--

INSERT INTO `neraca` (`id_neraca`, `tgl_neraca`, `kode_reff`, `users_id`, `informasi`, `debit`, `kredit`, `saldo`, `cabang_id`) VALUES
(976, '2020-02-04 12:22:36', 113, 10, 'Mobil-mobilan @100', 0, 2500000, NULL, 1),
(977, '2020-02-04 12:24:14', 113, 10, 'Kuda-kudaan @50', 0, 2500000, NULL, 1),
(978, '2020-02-04 06:41:46', 411, 14, 'Khitan Metode Metode Stapler , Pasien: Ahmad Fatkhurrozi', 800000, 0, NULL, 1),
(980, '2020-02-04 14:40:41', 122, 1, 'Sirup Rasa Jeruk', 0, 1500000, NULL, 0),
(981, '2020-02-04 14:42:17', 122, 1, 'Bodrexin', 0, 400000, NULL, 0),
(982, '2020-02-04 08:36:10', 411, 14, 'Khitan Metode Metode Stapler, Pasien: Rahmad', 800000, 0, NULL, 1),
(983, '2020-01-05 02:53:36', 411, 1, 'Permintaan Obat : @50 Amoxicillyn-cabang 1 mojokerto', 250000, 0, NULL, 0),
(984, '2020-01-05 02:53:36', 122, 10, 'Persediaan Obat : @50 Amoxicillyn', 0, 250000, NULL, 1),
(985, '2020-02-05 05:12:59', 411, 1, 'Permintaan Obat : @25 Sirup Rasa Jeruk-cabang 1 mojokerto', 375000, 0, NULL, 0),
(986, '2020-02-05 05:12:59', 122, 10, 'Persediaan Obat : @25 Sirup Rasa Jeruk', 0, 375000, NULL, 1),
(987, '2020-02-05 06:59:38', 411, 1, 'Permintaan Alkes : @20 Sarung Tangan-cabang 1 mojokerto', 20000, 0, NULL, 0),
(988, '2020-02-05 06:59:38', 121, 10, 'Persediaan Alkes/ BHP : @20 Sarung Tangan', 0, 20000, NULL, 1),
(989, '2020-02-05 07:25:41', 411, 1, 'Permintaan Alkes : @45 Sarung Tangan-cabang 1 mojokerto', 45000, 0, NULL, 0),
(990, '2020-02-05 07:25:41', 121, 10, 'Persediaan Alkes/ BHP : @45 Sarung Tangan', 0, 45000, NULL, 1),
(991, '2020-02-05 09:00:24', 411, 1, 'Permintaan Obat : @25 Sirup Rasa Jeruk-cabang 1 mojokerto', 375000, 0, NULL, 0),
(992, '2020-02-05 09:00:24', 122, 10, 'Persediaan Obat : @25 Sirup Rasa Jeruk', 0, 375000, NULL, 1),
(993, '2020-02-06 04:43:14', 411, 1, 'Permintaan Obat : @100 Amoxicillyn-Cahaya Khitan Sidoarjo', 500000, 0, NULL, 0),
(994, '2020-02-06 04:43:14', 122, 11, 'Persediaan Obat : @100 Amoxicillyn', 0, 500000, NULL, 2),
(995, '2020-02-07 09:32:16', 411, 1, 'Permintaan Alkes : @50 Suntik-cabang 1 mojokerto', 125000, 0, NULL, 0),
(996, '2020-02-07 09:32:16', 121, 10, 'Persediaan Alkes/ BHP : @50 Suntik', 0, 125000, NULL, 1),
(997, '2020-02-07 09:32:49', 411, 14, 'Khitan Metode Metode Stapler, Pasien: Coba', 800000, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pasiens`
--

CREATE TABLE `pasiens` (
  `id_pasien` bigint(20) UNSIGNED NOT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `usia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bb` int(11) NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `metode_id` int(11) NOT NULL,
  `riwayat_alergi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `riwayat_terdahulu` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_orangtua` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `referensi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `jam` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tempat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cabang_id` int(11) NOT NULL,
  `tempat_khitan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `bhp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fee_operator` double NOT NULL,
  `fee_asisten` double NOT NULL,
  `gratis_biaya` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_khitan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pasiens`
--

INSERT INTO `pasiens` (`id_pasien`, `nama`, `tgl_lahir`, `usia`, `bb`, `alamat`, `metode_id`, `riwayat_alergi`, `riwayat_terdahulu`, `nama_orangtua`, `referensi`, `tanggal`, `jam`, `tempat`, `cabang_id`, `tempat_khitan`, `users_id`, `bhp`, `fee_operator`, `fee_asisten`, `gratis_biaya`, `keterangan`, `status_khitan`, `created_at`, `updated_at`) VALUES
(1, 'Ahmad Fatkhurrozi', '2004-09-26', '23', 50, 'Dsn. Kalibening RT 004 RW 002 Ds. Tanggalrejo\r\nKec. Mojoagung', 1, '-', '-', 'Imam', 'Facebook', '2020-02-04', '07:00:00', 'Klinik', 1, 'Meri', 14, 'Tablet', 10000, 5000, 'gratis', '-', 'khitan', '2020-02-03 22:12:11', '2020-02-03 23:41:46'),
(11, 'Hamba', '2007-02-04', '13', 15, 'Meri', 1, 'Corona', '-', 'Bapak Hamba', 'Internet (website)', '2020-02-08', '07:00:00', 'Rumah Pasien', 1, 'Meri', 14, 'Sirup', 10000, 5000, NULL, '-', 'menunggu', '2020-02-04 00:46:44', '2020-02-04 00:46:44'),
(12, 'Rahmad', '2009-02-04', '11', 30, '-', 1, '-', '-', 'Bay', 'Internet (website)', '2020-02-06', '07:00:00', 'Rumah Pasien', 1, 'Meri', 14, 'Tablet', 10000, 5000, NULL, '-', 'khitan', '2020-02-04 01:34:51', '2020-02-04 01:36:10'),
(13, 'Coba', '2004-02-07', '16', 36, '-', 1, '-', '-', 'Coba-coba', 'Teman', '2020-03-01', '07:00:00', 'Klinik', 1, '-', 14, 'Tablet', 10000, 5000, NULL, '-', 'khitan', '2020-01-01 02:14:01', '2020-02-07 02:32:49');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ahmadfatkhurrozi99@gmail.com', '$2y$10$aI4DGDJjoAnhsMsg7Rw1Rua77i/hIXd9xZ85B.plk2nTSPWCJYxVe', '2020-02-07 03:01:58');

-- --------------------------------------------------------

--
-- Table structure for table `pengeluarans`
--

CREATE TABLE `pengeluarans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kode_reff` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_reff` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` int(11) NOT NULL,
  `nominal` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pengeluarans`
--

INSERT INTO `pengeluarans` (`id`, `users_id`, `jenis`, `kode_reff`, `nama_reff`, `qty`, `nominal`, `created_at`, `updated_at`) VALUES
(10, 1, 'Obat', 'reff_200117040143', 'Amoxicillyn', 100, 5000, '2020-01-16 21:01:43', '2020-01-16 21:01:43'),
(12, 1, 'Bhp', 'reff_200117041619', 'Suntik', 50, 2000, '2020-01-16 21:16:19', '2020-01-16 21:16:19'),
(13, 14, 'Bhp', 'reff_200117041751', 'Sarung Tangan', 50, 1000, '2020-01-16 21:17:51', '2020-01-16 21:17:51');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan_bhps`
--

CREATE TABLE `permintaan_bhps` (
  `id_permintaan_bhp` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `cabang_id` int(11) UNSIGNED NOT NULL,
  `bhp_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_permintaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permintaan_bhps`
--

INSERT INTO `permintaan_bhps` (`id_permintaan_bhp`, `users_id`, `cabang_id`, `bhp_id`, `qty`, `keterangan`, `status_permintaan`, `created_at`, `updated_at`) VALUES
(6, 10, 1, 1, 20, '-', 'diterima', '2020-01-28 18:57:38', '2020-02-05 00:23:24'),
(8, 10, 1, 1, 45, '-', 'diterima', '2020-02-05 00:25:26', '2020-02-05 00:25:52'),
(10, 10, 1, 2, 50, '-', 'diterima', '2020-02-07 02:32:04', '2020-02-07 02:32:23');

-- --------------------------------------------------------

--
-- Table structure for table `permintaan_obats`
--

CREATE TABLE `permintaan_obats` (
  `id_permintaan_obat` bigint(20) UNSIGNED NOT NULL,
  `users_id` int(10) UNSIGNED NOT NULL,
  `cabang_id` int(11) UNSIGNED NOT NULL,
  `obat_id` int(10) UNSIGNED NOT NULL,
  `qty` int(11) NOT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_permintaan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permintaan_obats`
--

INSERT INTO `permintaan_obats` (`id_permintaan_obat`, `users_id`, `cabang_id`, `obat_id`, `qty`, `keterangan`, `status_permintaan`, `created_at`, `updated_at`) VALUES
(9, 10, 1, 4, 10, 'pesen 1 mas', 'tolak', '2020-02-04 20:39:37', '2020-02-04 22:07:14'),
(10, 10, 1, 4, 25, '-', 'diterima', '2020-02-04 22:06:58', '2020-02-04 22:22:48'),
(12, 10, 1, 4, 25, 'pesen boss', 'diterima', '2020-02-05 02:00:00', '2020-02-05 02:01:40'),
(13, 11, 2, 3, 100, '-', 'diterima', '2020-02-05 21:43:01', '2020-02-05 21:43:27');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Laki - Laki / Perempuan',
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level_user` tinyint(4) NOT NULL COMMENT '1 : Admin; 2 : Owner; 3 : Kepala Cabang; 4 : Operator Cabang; 5 : Bendahara; 6 : Marketing',
  `cabang_id` bigint(20) DEFAULT NULL,
  `is_banned` tinyint(4) NOT NULL COMMENT '0 : Active; 1 : Banned;',
  `last_login` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `token`, `name`, `gender`, `address`, `phone`, `photo`, `level_user`, `cabang_id`, `is_banned`, `last_login`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@cahaya.com', '$2y$10$F.ob/fTvH40h/s4dA8Wsge.YY.GOb21F7mTks9zhlvgK1QBGSfwaK', '8s1A8slAehdBGOTd0db3c8483373ee', 'Ahmad Fatkhurrozi', 'Laki - Laki', 'Mojoagung', '085655949778', 'uploads/users/Admin/20200207-090032_admin.jpg', 1, 0, 0, '2020-02-10 05:40:04', NULL, NULL, '2020-02-10 05:40:04'),
(10, 'bendaharamojokerto', 'bendahara@mojokerto.com', '$2y$10$acUWTmbtEMsO7Z9VHPx0W.IKzOEqnwvF4pO7vfoVUgZba1u9UQt8S', NULL, 'Bendahara Mojokerto', 'Laki - Laki', 'Meri', '-', NULL, 5, 1, 0, '2020-02-10 05:48:18', NULL, '2020-01-13 02:06:03', '2020-02-10 05:48:18'),
(11, 'bendaharasidoarjo', 'bendahara@sidoarjo.com', '$2y$10$ezyyNdb3cn2sw1eYwfxPD.ycPBtPZ3rvS9PmYkhUxznrAOqPc2uAy', NULL, 'Bendahara Sidoarjo', 'Laki - Laki', 'Sidoarjo', '-', NULL, 5, 2, 0, '2020-02-06 05:01:03', NULL, '2020-01-13 02:07:12', '2020-02-06 05:01:03'),
(12, 'bendaharakertosono', 'bendahara@kertosono.com', '$2y$10$AkECO8F3C3iak4seTuyJ4.wVEhLEqCIafjxHFRfXVD5jUk/zMm09y', NULL, 'Bendahara Kertosono', 'Perempuan', '-', '-', NULL, 5, 3, 0, NULL, NULL, '2020-01-13 02:08:02', '2020-01-13 02:08:02'),
(14, 'operatormojokerto', 'operator@mojokerto.com', '$2y$10$ywXGDSRJgxfID7Z5cvBc/.okLqc//QwerWIWg6gozo5EeYzl63nNe', 'L5bo43Uoj5VhJBx257197caa18f59e', 'operator mojokerto', 'Laki - Laki', 'Mojokerto', '-', 'uploads/users/Admin/20200207-090009_operatormojokerto.jpg', 4, 1, 0, '2020-02-07 09:14:52', NULL, '2020-01-13 02:37:47', '2020-02-07 09:14:52'),
(15, 'operatorsidoarjo', 'operator@sidoarjo.com', '$2y$10$1gHMgbuB84bZ5sLUJRZBVuXXDetIkAudBh6QpqHX3QGZZp5wP6wzO', NULL, 'operator sidoarjo', 'Perempuan', 'sidoarjo', '-', NULL, 4, 2, 0, '2020-02-04 01:43:21', NULL, '2020-01-13 02:40:03', '2020-02-04 01:43:21'),
(16, 'operatorkertosono', 'operator@kertosono.com', '$2y$10$cYcSR.vIB9hDnVaJYFV5TujW536y/vV82M.pexFcjV.ohnCZnMpCq', NULL, 'operator kertosono', 'Laki - Laki', 'kertosono', '-', NULL, 4, 3, 0, '2020-01-14 03:21:45', NULL, '2020-01-13 02:40:51', '2020-01-14 03:21:45'),
(17, 'marketingmojokerto', 'marketing@mojokerto.com', '$2y$10$LkOGLYSRGWDyRnZRNQFzFusHHPa.AWWdhkYAiRZOIwfRQrJ/puZ6e', NULL, 'Marketing Mojokerto', 'Perempuan', 'Mojokerto', '-', NULL, 6, 1, 0, '2020-02-10 01:31:59', NULL, '2020-01-13 03:06:37', '2020-02-10 01:31:59'),
(18, 'kepalamojokerto', 'kepala@mojokerto.com', '$2y$10$vszPQSJ2s27UHaWBuzntheb2dKp2F4gWR1KBmOvqG6agYqgTHj77i', 'h0VW7XVK2s08E2Q4e9f2ac7d843eb7', 'Kepala Mojokerto', 'Laki - Laki', 'Meri', '-', 'uploads/users/Admin/20200207-085413_kepalamojokerto.jpg', 3, 1, 0, '2020-02-10 01:36:15', NULL, '2020-01-14 22:43:02', '2020-02-10 01:36:15'),
(21, 'ownermojokerto', 'owner@mojokerto.com', '$2y$10$IKUQ/CjTqG2L1VL7eL/nw.yvoxU08taiWti2ZpURwAXsByMNj/KBe', NULL, 'Owner Moker', 'Laki - Laki', 'Prajurit Kulon', '0987654321', 'uploads/users/Owner/20200120-043120_ownermojokerto.jpg', 2, 1, 0, '2020-02-10 01:37:24', NULL, '2020-01-19 21:31:20', '2020-02-10 01:37:24'),
(27, 'fatkhurrozi99', 'ahmadfatkhurrozi99@gmail.com', '$2y$10$QzrcBuF5a.tg0YSDXya0nOqKdNCCWcPHkQzlNi/KQ4q0Oy58mngaC', 'xJhPh1v73aBEm9C8bfbec4cedebc72', 'Ahmad Fatkhurrozi', 'Laki - Laki', 'Kalibening', '085655949778', 'uploads/users/Operator/20200129-041613_fatkhurrozi99.png', 4, 1, 0, '2020-02-10 04:46:59', NULL, '2020-01-28 21:16:13', '2020-02-09 21:56:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_bhps`
--
ALTER TABLE `admin_bhps`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_obats`
--
ALTER TABLE `admin_obats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `akun`
--
ALTER TABLE `akun`
  ADD PRIMARY KEY (`no_reff`);

--
-- Indexes for table `bahan_habis_pakais`
--
ALTER TABLE `bahan_habis_pakais`
  ADD PRIMARY KEY (`id_bhp`);

--
-- Indexes for table `biayas`
--
ALTER TABLE `biayas`
  ADD PRIMARY KEY (`id_biaya`);

--
-- Indexes for table `daftar_cabangs`
--
ALTER TABLE `daftar_cabangs`
  ADD PRIMARY KEY (`id_cabang`);

--
-- Indexes for table `daftar_mainans`
--
ALTER TABLE `daftar_mainans`
  ADD PRIMARY KEY (`id_mainan`);

--
-- Indexes for table `fee_masters`
--
ALTER TABLE `fee_masters`
  ADD PRIMARY KEY (`id_fee`);

--
-- Indexes for table `master_gajis`
--
ALTER TABLE `master_gajis`
  ADD PRIMARY KEY (`id_gaji`);

--
-- Indexes for table `master_obat`
--
ALTER TABLE `master_obat`
  ADD PRIMARY KEY (`id_obat`);

--
-- Indexes for table `metode_sunats`
--
ALTER TABLE `metode_sunats`
  ADD PRIMARY KEY (`id_metode`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `neraca`
--
ALTER TABLE `neraca`
  ADD PRIMARY KEY (`id_neraca`);

--
-- Indexes for table `pasiens`
--
ALTER TABLE `pasiens`
  ADD PRIMARY KEY (`id_pasien`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `pengeluarans`
--
ALTER TABLE `pengeluarans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permintaan_bhps`
--
ALTER TABLE `permintaan_bhps`
  ADD PRIMARY KEY (`id_permintaan_bhp`);

--
-- Indexes for table `permintaan_obats`
--
ALTER TABLE `permintaan_obats`
  ADD PRIMARY KEY (`id_permintaan_obat`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cabang_id` (`cabang_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_bhps`
--
ALTER TABLE `admin_bhps`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `admin_obats`
--
ALTER TABLE `admin_obats`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bahan_habis_pakais`
--
ALTER TABLE `bahan_habis_pakais`
  MODIFY `id_bhp` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `biayas`
--
ALTER TABLE `biayas`
  MODIFY `id_biaya` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `daftar_cabangs`
--
ALTER TABLE `daftar_cabangs`
  MODIFY `id_cabang` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `daftar_mainans`
--
ALTER TABLE `daftar_mainans`
  MODIFY `id_mainan` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `fee_masters`
--
ALTER TABLE `fee_masters`
  MODIFY `id_fee` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `master_gajis`
--
ALTER TABLE `master_gajis`
  MODIFY `id_gaji` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `master_obat`
--
ALTER TABLE `master_obat`
  MODIFY `id_obat` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `metode_sunats`
--
ALTER TABLE `metode_sunats`
  MODIFY `id_metode` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `neraca`
--
ALTER TABLE `neraca`
  MODIFY `id_neraca` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=998;

--
-- AUTO_INCREMENT for table `pasiens`
--
ALTER TABLE `pasiens`
  MODIFY `id_pasien` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `pengeluarans`
--
ALTER TABLE `pengeluarans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `permintaan_bhps`
--
ALTER TABLE `permintaan_bhps`
  MODIFY `id_permintaan_bhp` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `permintaan_obats`
--
ALTER TABLE `permintaan_obats`
  MODIFY `id_permintaan_obat` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
