<?php 

namespace App\Http\Libraries;

class Notif  
{
    public static function SendNotif($judul, $data, $player_id)
    {    	
    	$content = array(
	        "en" => $judul
	    );

	    $fields = array(
	        'app_id' => "c69e97b8-1c9e-4c90-80a4-d3ac46e8ce5d",
	        'include_player_ids' => $player_id,
	        'data' => $data,
	        // 'android_channel_id' => "b491f9ec-a2bb-4b4f-8c29-08be67584e33",
	        'contents' => $content
	    );

	    $fields = json_encode($fields);

	    $ch = curl_init();
	    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8'));
	        // 'Authorization: Basic NDYxMGJkNjktNjMzNC00NzVmLWIzMGQtZDYwMjI3M2FlNjVk'));
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	    curl_setopt($ch, CURLOPT_HEADER, FALSE);
	    curl_setopt($ch, CURLOPT_POST, TRUE);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

	    $response = curl_exec($ch);
	    curl_close($ch);
    }
}
