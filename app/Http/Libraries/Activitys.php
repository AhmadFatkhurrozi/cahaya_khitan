<?php
namespace App\Http\Libraries;
use App\Models\ActivitySystems;
use Auth;

class Activitys
{
	public static function add($request)
	{
		date_default_timezone_set('Asia/Jakarta');
		$addActivity = new ActivitySystems;
		$addActivity->created_at = date("Y-m-d H:i:s");
		$addActivity->action_type = $request['action_type'];
		$addActivity->user_id = Auth::getUser()->id;
		$addActivity->actor_id = Auth::getUser()->level_user;
		$addActivity->created_name = Auth::getUser()->name;
		$addActivity->ip_address = $_SERVER['REMOTE_ADDR'];
		$addActivity->message = $request['message'];
		$addActivity->save();

		if ($addActivity) {
			$return = ['status'=>'success', 'code'=>'200', 'message'=>'Atifitas Berhasil Disimpan !!','data'=>$addActivity];
		}else{
			$return = ['status'=>'error', 'code'=>'500', 'message'=>'Atifitas Gagal Disimpan !!','data'=>''];
		}
		return $return;
	}
}
