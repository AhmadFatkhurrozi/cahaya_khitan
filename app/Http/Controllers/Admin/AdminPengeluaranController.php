<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Biaya;
use App\Models\Pengeluaran;
use App\Models\Master_gaji;
use App\Models\Admin_obat;
use App\Models\Admin_bhp;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Validator, Auth;
use Carbon\Carbon;

class AdminPengeluaranController extends Controller
{
    public function main(Request $request)
    {
        $now = Carbon::now();

        $this->data['title']        = "Pengeluaran";
        $this->data['mn_active']    = "pengeluaran";
        $this->data['submn_active'] = "Pengeluaran";
        $this->data['smallTitle']   = "";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => Pengeluaran::select(DB::raw('YEAR(pengeluarans.created_at) year, MONTH(pengeluarans.created_at) month, monthname(pengeluarans.created_at) longMonth'))->join('users', 'users.id', 'pengeluarans.users_id')->where('level_user', Auth::getUser()->level_user)->groupby('year','month', 'longMonth')->get(),
        ];

        return view('admin/pengeluaran/main', $data)->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Pengeluaran::getJsonPengeluaran($request);
        return response()->json($data);
    }

    // PENGELUARAN OBAT

    public function formObat(Request $request)
    {
        $data['obat']  = Admin_obat::all();
        $data['waktu'] = Carbon::now();
        $content = view('admin/pengeluaran/formAddObat', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function addExpObat(Request $request)
    {
        $rules = array(
            'harga'     => 'required',
            'stok'      => 'required'
        );
        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );
        $validator  = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) {
            $data                 = new Pengeluaran;
            $data->users_id       = Auth::getUser()->id;
            $data->jenis          = "Obat";
            $data->kode_reff      = "reff_".date('ymdHis');
            $data->nama_reff      = $request->nama_obat;
            $data->qty            = $request->stok;
            $data->nominal        = $request->harga;
            $data->save();

            if ($data) {
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }
    }

    // PENGELUARAN BHP

    public function formBhp(Request $request)
    {
        $data['bhp']   = Admin_bhp::all();
        $data['waktu'] = Carbon::now();
        $content = view('admin/pengeluaran/formAddBhp', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function addExpBhp(Request $request)
    {
        $rules = array(
            'harga'     => 'required',
            'stok'      => 'required'
        );
        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );
        $validator  = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) {
            $data                 = new Pengeluaran;
            $data->users_id       = Auth::getUser()->id;
            $data->jenis          = "Bhp";
            $data->kode_reff      = "reff_".date('ymdHis');
            $data->nama_reff      = $request->nama_obat;
            $data->qty            = $request->stok;
            $data->nominal        = $request->harga;
            $data->save();

            if ($data) {
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }
    }

    // END PENGELUARAN BHP

    public function detail_exp(Request $request)
    {
        $data = [
            'bulan' => $request->longMonth,
            'tahun' => $request->year,
            'data'  => Pengeluaran::select('pengeluarans.created_at', 'jenis', 'nama_reff', 'qty', 'nominal')
                                ->join('users', 'users.id', 'pengeluarans.users_id')
                                ->where('level_user', Auth::getUser()->level_user)
                                ->whereMonth('pengeluarans.created_at', '=', $request->month)
                                ->whereYear('pengeluarans.created_at', '=', $request->year)->get(),
        ];

        $content = view('admin/pengeluaran/detailPengeluaran',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function searchPeng(Request $request)
    {
        $now = Carbon::now();

        $this->data['title']        = "Pengeluaran";
        $this->data['mn_active']    = $this->menuActive;
        $this->data['submn_active'] = "Pengeluaran";
        $this->data['smallTitle']   = "";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => Pengeluaran::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))
                            ->whereMonth('created_at', '=', $request->month)
                            ->whereYear('created_at', '=', $request->year)
                            ->groupby('year','month', 'longMonth')->get(),
                            'filter' => Pengeluaran::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))->groupby('year','month', 'longMonth')->get(),
        ];

        return view($this->menuActive.'.biaya.pengSearch', $data)->with('data', $this->data);
    }
}
