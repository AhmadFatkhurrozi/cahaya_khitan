<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin_bhp;
use Validator,Session,Redirect,Auth;

class AdminBhpController extends Controller
{
    private $title = "Data Alkes / BHP";
    private $menuActive = "bhp";
    private $submnActive = "bhp";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view('admin/bhp/main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Admin_bhp::getJsonBhpAdmin($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data['data'] = (!empty($request->id)) ? Admin_bhp::find($request->id) : "";
        $content = view('admin/bhp/formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        $rules = array(
            'nama_barang'     => 'required',
            'stok'            => 'required',
            'harga_satuan'    => 'required',
            'harga_grosir'    => 'required',
        );

        $messages = array('required'  => 'Kolom Harus Diisi');
        
        $validator  = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) {
            $dtBhp = (!empty($request->id)) ? Admin_bhp::find($request->id) : new Admin_bhp;
            $dtBhp->nama_barang           = $request->nama_barang;
            $dtBhp->stok_bhp              = $request->stok;
            $dtBhp->harga_satuan          = str_replace('.', '', $request->harga_satuan);
            $dtBhp->harga_grosir          = str_replace('.', '', $request->harga_grosir);
            $dtBhp->save();

            if ($dtBhp) {
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }
    }

    public function show(Request $request)
    {
        $id = $request->id;
        $data['data'] = Admin_bhp::find($id);

        if ($data['data']) {
            $content = view('admin/bhp/show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $do_delete = Admin_bhp::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
