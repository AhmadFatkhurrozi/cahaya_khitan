<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin_obat;
use App\Http\Libraries\CusFormat;
use App\Models\Pengeluaran;
use Auth, Redirect, Validator, DB;

class AdminObatController extends Controller
{
    private $title          = "Data Obat";
    private $menuActive     = "obat";
    private $submnActive    = "stok_obat";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view('admin/obat/main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Admin_obat::getJsonObatAdmin($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data['data'] = (!empty($request->id)) ? Admin_obat::find($request->id) : "";
        $content = view('admin/obat/formAdd', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        $rules = array(
                'kode_obat'    => 'required',
                'nama_obat'    => 'required',
                'jenis'        => 'required',
                'harga'        => 'required',
                'satuan'       => 'required',
                'stok'         => 'required'
            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );
            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $newdata = (!empty($request->id)) ? Admin_obat::find($request->id) : new Admin_obat;
                $newdata->kode_obat             = $request->kode_obat;
                $newdata->nama_obat             = $request->nama_obat;
                $newdata->jenis_obat            = $request->jenis;
                $newdata->harga_obat            = str_replace('.', '', $request->harga);
                $newdata->satuan_obat           = $request->satuan;
                $newdata->stok_obat             = $request->stok;
                $newdata->save();

                if ($newdata) {
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
                }
                return response()->json($return);
            } else {
                return $validator->messages();
            }
    }

    public function show(Request $request)
    {
        $id = $request->id;
        $data['data'] = Admin_obat::find($id);

        if ($data['data']) {
            $content = view('admin/obat/show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }

        return ['status'=>'failed','content'=>'error'];
    }

    public function delete(Request $request)
    {
        $id = $request->id;
        $do_delete = Admin_obat::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }

}
