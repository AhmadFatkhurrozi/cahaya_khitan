<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Users;
use Auth, Redirect, Validator, Session, Hash;

class ProfileController extends Controller
{
    private $title = "My Profil";
    private $menuActive = "profile";
    private $submnActive = "";

    public function index()
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";
        
        return view($this->menuActive.'.main')->with('data', $this->data);
    }

    public function update(Request $request)
    {   
        $id = $request->id;
        $pass = bcrypt($request->password);

        $profile            = Users::find($id);
        $profile->email     = $request->email;
        $profile->name      = $request->nama;
        $profile->address   = $request->alamat;
        $profile->phone     = $request->telp;
        if ($request->password != null) {
            $profile->password = $pass;
        }
        $profile->save();

        if($profile){
            return redirect(route('profile'));
        }
    }
}
