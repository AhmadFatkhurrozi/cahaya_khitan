<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Daftar_mainan;
use App\Models\Pengeluaran;
use App\Http\Libraries\CusFormat;
use Validator;
use Session,Redirect,Auth;

class DaftarMainanController extends Controller
{
    private $title = "Daftar Mainan";
    private $menuActive = "master_data";
    private $submnActive = "daftar_mainan";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Daftar_mainan::getJsonDaftarmainan($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data['data'] = (!empty($request->id_mainan)) ? Daftar_mainan::find($request->id_mainan)->first() : "";
        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        {
            $rules = array(
                'nama_mainan'   => 'required',
                'qty_mainan'    => 'required',
                'harga_mainan'  => 'required'
            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );

            $kode = "reff_".date('ymdHis');

            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $dtMainan = (!empty($request->id_mainan)) ? Daftar_mainan::find($request->id_mainan) : new Daftar_mainan;
                $dtMainan->nama_mainan        = $request->nama_mainan;
                if (empty($request->kode)) {
                    $dtMainan->kode_mainan      = $kode;
                }
                $dtMainan->cabang_id          = Auth::getUser()->cabang_id;
                $dtMainan->qty_mainan         = $request->qty_mainan;
                $dtMainan->harga_mainan       = str_replace('.', '', $request->harga_mainan);
                $dtMainan->save();

                if ($dtMainan) {
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
                }
                return response()->json($return);
            } else {
                return $validator->messages();
            }
        }
    }

    public function show(Request $request)
    {
        $id_mainan = $request->id_mainan;
        $data['data'] = Daftar_mainan::find($id_mainan);

        if ($data['data']) {
            $content = view($this->menuActive.'.daftar_mainan'.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function delete(Request $request)
    {
        $id_mainan = $request->id_mainan;
        $do_delete = Daftar_mainan::find($id_mainan);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
