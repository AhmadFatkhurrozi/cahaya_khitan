<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pasien;
use Validator;
use Session,Redirect,Auth;

class CalendarController extends Controller
{
    private $title = "Calendar";
    private $menuActive = "calendar";
    private $submnActive = "calendar";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";
        $pasien = Pasien::get();
        $data_pasien = [];
        if(!empty($pasien)){
            foreach ($pasien as $pas) {
                $datanya = ['title'=>$pas->nama,'start'=>$pas->tanggal];
                array_push($data_pasien,$datanya);
            }
        }
        $this->data['pasien'] = json_encode($data_pasien);

        return view($this->menuActive.'.main')->with('data', $this->data);
    }
}
