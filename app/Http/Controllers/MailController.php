<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CahayaEmail;
use App\Models\Users;
use Illuminate\Support\Facades\Mail;
use Validator;

class MailController extends Controller
{   
    public function index(Request $request)
    {   
        $this->data['title'] = 'Login';
        $this->data['next_url'] = empty($request->next_url) ? '' : $request->next_url;
        return view('component.reset')->with('data', $this->data);
    }

    public function sendMail(Request $request){
        
        $email = $request->email;

        $cekEmail = Users::where('email', $email)->first();
        
        if (!empty($cekEmail)) {
            
            $id = Users::where('email', $email)->first()->id;
            
            Mail::send('mail.reset', array('email' => base64_encode($email), 'users_id' => base64_encode($id)) , function($pesan) use($request){
                $pesan->to($request->email)->subject('Reset Password');
                $pesan->from(env('Cahaya Khitan','no_reply@cahayakhitan.com'));
            });

            return "Email untuk reset password telah dikirim ".'<a href="'.url('/').'">Home</a>';

        }else{

            return "Email tidak terdaftar ".'<a href="'.url('/reset').'">Kembali</a>';
        }
    }

    public function reset($id, $email)
    {
       $data = [
            'pengguna' => Users::where('id', base64_decode($id))->where('email', base64_decode($email))->first()
       ];

        $this->data['title'] = 'Reset Password';
        return view('component.reset_password', $data)->with('data', $this->data);
    }

    public function update(Request $request)
    {   
        $messages = [
            'password' => 'Invalid current password.',
        ];

        $validator = Validator::make(request()->all(), [
            'password'              => 'required|min:6|confirmed',
            'password_confirmation' => 'required',
 
        ], $messages);
 
        if ($validator->fails()) {
            return redirect()
                ->back()
                ->withErrors($validator->errors());
        }else{

            $user = Users::find($request->users_id);
            $user->password = bcrypt($request->password);
            $user->save();
        }
        
        if ($user) {
            return "Password Berhasil Di Ubah, Login Sekarang ".'<a href="'.url('/').'">Login</a>';
        }
    }
}