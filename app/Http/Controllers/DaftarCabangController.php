<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Daftar_cabang;
use Validator;
use Session,Redirect,Auth;

class DaftarCabangController extends Controller
{
    private $title = "Data Klinik";
    private $menuActive = "master_data";
    private $submnActive = "daftar_cabang";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Daftar_cabang::getJsonDaftarcabang($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data['data'] = (!empty($request->id_cabang)) ? Daftar_cabang::find($request->id_cabang)->first() : "";
        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        {
            $rules = array(
                'nama_cabang'   => 'required',
                'alamat'        => 'required',
                'telepon'       => 'required',
                'tentang'       => 'required',

            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );
            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $dtCabang                        = (!empty($request->id_cabang)) ? Daftar_cabang::find($request->id_cabang) : new Daftar_cabang;
                $dtCabang->nama_cabang      = $request->nama_cabang;
                $dtCabang->alamat           = $request->alamat;
                $dtCabang->telepon          = $request->telepon;
                $dtCabang->posisi           = $request->posisi;
                $dtCabang->keterangan       = $request->tentang;

                if (!empty($dtCabang->cabang_id)) {
                    $dtCabang->id_cabang     = $request->id_cabang;
                }else{
                    $dtCabang->id_cabang     = 0;
                }

                $dtCabang->save();

                if ($dtCabang) {
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
                }
                return response()->json($return);
            } else {
                return $validator->messages();
            }
        }
    }

    public function show(Request $request)
    {
        $id_cabang = $request->id_cabang;
        $data['data'] = Daftar_cabang::find($id_cabang);

        if ($data['data']) {
            $content = view($this->menuActive.'.daftar_cabang'.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function delete(Request $request)
    {
        $id_cabang = $request->id_cabang;
        $do_delete = Daftar_cabang::find($id_cabang);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
