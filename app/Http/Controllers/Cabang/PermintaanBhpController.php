<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use App\Models\PermintaanBhp;
use Illuminate\Http\Request;
use App\Http\Libraries\CusFormat;
use App\Models\Item_permintaan;
use App\Models\Users;
use App\Models\Neraca;
use App\Models\Admin_bhp;
use App\Models\Bahan_habis_pakai;
use App\User;
use App\Models\Daftar_cabang;;
use Auth, Redirect, Validator, DB, Session;

class PermintaanBhpController extends Controller
{
    private $title = "Permintaan Alkes/ BHP";
    private $menuActive = "bhp";
    private $submnActive = "permintaan_bhp";

    public function main()
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = PermintaanBhp::getJsonPermintaan($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {       
        $data['bhp']        = Admin_bhp::all();
        $data['permintaan'] = Daftar_cabang::join('users','daftar_cabangs.id_cabang', '=', 'users.cabang_id')->where('users.id', Auth::User()->id)->first();

        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        $rules = array(
            'bhp_id'           => 'required',
            'jumlah_permintaan' => 'required',
            'komentar'          => 'required',
        );

        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );

        $getID = Daftar_cabang::join('users','daftar_cabangs.id_cabang', '=', 'users.cabang_id')
        ->where('users.id', Auth::User()->id)->first();
        $validator  = Validator::make($request->all(), $rules, $messages);
        if ($request->jumlah_permintaan <= $request->stok && !$validator->fails()) { 

            $save_permin                      = new PermintaanBhp;
            $save_permin->users_id            = Auth::User()->id;
            $save_permin->cabang_id           = $getID->id_cabang;
            $save_permin->bhp_id              = $request->bhp_id;       
            $save_permin->qty                 = $request->jumlah_permintaan;
            $save_permin->keterangan          = $request->komentar;
            $save_permin->status_permintaan   = 'menunggu';
            $save_permin->save();  

            if ($save_permin){
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);

        } elseif($request->jumlah_permintaan > $request->stok) {
            
            return ['status'=>'error', 'code'=>'400', 'message'=>'Maaf!! Stok Tidak Mencukupi !!'];
        
        } else {
            return $validator->messages();
        }
    }  

    public function show(Request $request)
    {   
        $id = $request->id_permintaan_bhp;
        $data['permin'] = PermintaanBhp::select('id_permintaan_bhp', 'nama_cabang', 'permintaan_bhps.created_at', 'permintaan_bhps.keterangan', 'ao.nama_barang', 'permintaan_bhps.qty','status_permintaan')
                                        ->where('permintaan_bhps.id_permintaan_bhp', $id)
                                        ->join('admin_bhps as ao', 'ao.id', 'permintaan_bhps.bhp_id')
                                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'permintaan_bhps.cabang_id')
                                        ->get();

        $data['stok'] = Admin_bhp::where('id', $request->bhp_id)->first();
        if ($data['permin']) {
            $content = view($this->menuActive.'.'.$this->submnActive.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function konfirmasi_permintaan(Request $request)
    {
        $id_permintaan_bhp  = $request->id_permintaan_bhp;
        $users_id           = $request->users_id;
        $nama_barang        = $request->nama_barang;   
        $bhp_id             = $request->bhp_id;
        $harga_satuan       = $request->harga_satuan;
        $harga_grosir       = $request->harga_grosir;
        $qty                = $request->qty;
        $total              = $request->total;
        $cabang_id          = $request->cabang_id;
        $nama_cabang        = $request->nama_cabang;    

        // Tabel Permintaan Bhp
        $FileApp = PermintaanBhp::find($id_permintaan_bhp);
        $FileApp->status_permintaan = "selesai";
        $FileApp->save();

        // Tabel Admin Obat
        DB::select("UPDATE admin_bhps SET stok_bhp=stok_bhp-".$qty." WHERE id =".'"'.$bhp_id.'"');

        // Tabel Neraca Admin
        $neraca             = new Neraca;
        $neraca->tgl_neraca = date('Y-m-d H:i:s');
        $neraca->kode_reff  = "411";
        $neraca->users_id   = Auth::getUser()->id;
        $neraca->informasi  = "Permintaan Alkes : @".$qty.' '.$nama_barang.'-'.$nama_cabang;
        $neraca->debit      = $total;
        $neraca->kredit     = 0;
        $neraca->cabang_id  = 0;
        $neraca->save();

        if ($neraca) {
            // Tabel Neraca Cabang
            $nrc             = new Neraca;
            $nrc->tgl_neraca = date('Y-m-d H:i:s');
            $nrc->kode_reff  = "121";
            $nrc->users_id   = $users_id;
            $nrc->informasi  = "Persediaan Alkes/ BHP : @".$qty.' '.$nama_barang;
            $nrc->debit      = 0;
            $nrc->kredit     = $total;
            $nrc->cabang_id  = $cabang_id;
            $nrc->save();
        }

        if ($nrc) {
            return ['status' => 'success','id_permintaan_bhp'=>$id_permintaan_bhp, 'status_permintaan' => "Sukses"];   
        }else{
            return ['status' => 'Gagal','id_permintaan_bhp'=>$id_permintaan_bhp, 'status_permintaan' => "Gagal"];   
        }
    }

    public function konfirmasi_pesanan(Request $request)
    {   
        $id_permintaan_bhp  = $request->id_permintaan_bhp;
        $users_id           = $request->users_id;
        $nama_barang        = $request->nama_barang;   
        $bhp_id             = $request->bhp_id;
        $harga_satuan       = $request->harga_satuan;
        $harga_grosir       = $request->harga_grosir;
        $qty                = $request->qty;
        $total              = $request->total;
        $cabang_id          = $request->cabang_id;
        $nama_cabang        = $request->nama_cabang;    

        // Tabel Permintaan
        $FileApp = PermintaanBhp::find($id_permintaan_bhp);
        $FileApp->status_permintaan = "diterima";
        $FileApp->save();

        $cek_kode_bhp = Bahan_habis_pakai::where('kode_bhp', $bhp_id)->where('cabang_id', $cabang_id)->first();

        // Tabel Master Obat
        if( $cek_kode_bhp != ''){
            DB::select("UPDATE bahan_habis_pakais SET stok=stok+".$qty." WHERE kode_bhp =".'"'.$bhp_id.'"');
        }else{
            $bhp       =   new Bahan_habis_pakai;
            $bhp->cabang_id    = $cabang_id;
            $bhp->kode_bhp     = $bhp_id;
            $bhp->nama_barang  = $nama_barang;
            $bhp->harga_grosir = $harga_grosir;
            $bhp->harga_satuan = $harga_satuan;
            $bhp->stok         = $qty;
            $bhp->save();
        }

        if ($FileApp) {
            return ['status' => 'success','id_permintaan_bhp'=>$id_permintaan_bhp, 'status_permintaan' => "Sukses"];   
        }else{
            return ['status' => 'Gagal','id_permintaan_bhp'=>$id_permintaan_bhp, 'status_permintaan' => "Gagal"];   
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id_permintaan_bhp;
        $do_delete = PermintaanBhp::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }

    public function reject(Request $request)
    {
        $id = $request->id_permintaan_bhp;
        $permin = PermintaanBhp::find($id);
        $permin->status_permintaan = "tolak";
        $permin->save();

        if($permin){
            return ['status' => 'success'];
        }else{
            return ['status' => 'error'];
        }
    }
}