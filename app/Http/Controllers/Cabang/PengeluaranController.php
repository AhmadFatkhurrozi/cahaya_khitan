<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use App\Models\Biaya;
use App\Models\Pengeluaran;
use Illuminate\Http\Request;
use App\Models\Master_gaji;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Validator, Auth;

class PengeluaranController extends Controller
{
    private $title = "Pengeluaran";
    private $menuActive = "pengeluaran";

    public function main(Request $request)
    {
        $now = Carbon::now();

        $this->data['title']        = "Pengeluaran";
        $this->data['mn_active']    = $this->menuActive;
        $this->data['submn_active'] = "Pengeluaran";
        $this->data['smallTitle']   = "";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => Pengeluaran::select(DB::raw('YEAR(pengeluarans.created_at) year, MONTH(pengeluarans.created_at) month, monthname(pengeluarans.created_at) longMonth'))
                                        ->join('users', 'users.id', 'pengeluarans.users_id')
                                        ->where('users.cabang_id', Auth::getUser()->cabang_id)
                                        ->groupby('year','month', 'longMonth')->get(),
        ];

        return view('cabang/'.$this->menuActive.'/main', $data)->with('data', $this->data);
    }

    public function datagridPengeluaran(Request $request)
    {
        $data = Pengeluaran::getJsonPengeluaran($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data = [
            'data'  => (!empty($request->id)) ? Pengeluaran::find($request->id) : ""
        ];

        $content = view('cabang.pengeluaran.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function AddPengeluaran(Request $request)
    {
        $rules = array(
            'jenis'     => 'required',
            'reff'      => 'required',
            'nominal'   => 'required',
            'qty'       => 'required'
        );
        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );
        $validator  = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) {
            $data                 = new Pengeluaran;
            $data->jenis          = $request->jenis;
            $data->nama_reff      = $request->reff;
            $data->qty            = $request->qty;
            $data->nominal        = $request->nominal;
            $data->save();

            if ($data) {
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }
    }

    public function formEditPengeluaran(Request $request)
    {
        $data = [
            'data'  => Pengeluaran::find($request->id)
        ];

        $content = view($this->menuActive.'.biaya.formEditKeluar',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function detailPeng(Request $request)
    {
        $data = [
            'bulan' => $request->longMonth,
            'tahun' => $request->year,
            'data'  => Pengeluaran::join('users', 'users.id', 'pengeluarans.users_id')
                                    ->whereMonth('pengeluarans.created_at', '=', $request->month)
                                    ->whereYear('pengeluarans.created_at', '=', $request->year)
                                    ->where('users.cabang_id', Auth::getUser()->cabang_id)
                                    ->get(),   
            'gaji'  => "0"
        ];

        $content = view('cabang/'.$this->menuActive.'/detail',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function searchPeng(Request $request)
    {
        $now = Carbon::now();

        $this->data['title']        = "Pengeluaran";
        $this->data['mn_active']    = $this->menuActive;
        $this->data['submn_active'] = "Pengeluaran";
        $this->data['smallTitle']   = "";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => Pengeluaran::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))
                            ->whereMonth('created_at', '=', $request->month)
                            ->whereYear('created_at', '=', $request->year)
                            ->groupby('year','month', 'longMonth')->get(),
                            'filter' => Pengeluaran::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))->groupby('year','month', 'longMonth')->get(),
        ];

        return view('cabang.pengeluaran.search', $data)->with('data', $this->data);
    }
}
