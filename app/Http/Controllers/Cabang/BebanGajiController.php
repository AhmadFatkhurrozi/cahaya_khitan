<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Libraries\CusFormat;
use App\Models\Users;
use Session, Redirect, Validator, DB, Auth;

class BebanGajiController extends Controller
{
    private $title = "Beban Gaji";
    private $menuActive = "cabang";
    private $submnActive = "beban_gaji";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        // $select = 'users.name, daftar_cabangs.posisi, master_gajis.nominal_gaji, IF(users_level = "1", "Admin", IF(users_level = "2", "Owner", IF(users_level = "3", "Kepala Cabang", IF(users_level = "4", "Operator Cabang", IF(users_level = "5", "Bendahara Cabang", IF(users_level = "6", "Marketing Cabang", "tidak ada")))))) as jabatan';

        $data = [
            'gaji' => Users::selectRaw('users.name, dc.posisi, mg.nominal_gaji, IF(users_level = "1", "Admin",
                            IF(users_level = "2", "Owner",
                            IF(users_level = "3", "Kepala Cabang",
                            IF(users_level = "4", "Operator Cabang",
                            IF(users_level = "5", "Bendahara Cabang",
                            IF(users_level = "6", "Marketing Cabang", "tidak ada")))))) as jabatan')
                        ->leftJoin('master_gajis as mg', 'mg.users_level', 'users.level_user')
                        ->join('daftar_cabangs as dc', 'users.cabang_id', 'dc.id_cabang')
                        ->where('users.cabang_id', Auth::getUser()->cabang_id)
                        ->groupBy('users.id')
                        ->get(),
        ];

        return view($this->menuActive.'.'.$this->submnActive.'.main', $data)->with('data', $this->data);
    }

}
