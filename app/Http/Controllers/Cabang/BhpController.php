<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bahan_habis_pakai;
use App\Models\Neraca;
use Validator, Session, Redirect, Auth;


class BhpController extends Controller
{
    private $title = "Data Alkes / BHP";
    private $menuActive = "bhp";
    private $submnActive = "stok_bhp";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function lokal(Request $request)
    {
        $this->data['title'] = "Pengadaan Lokal";
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = "pengadaan_lokal";
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.lokal')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Bahan_habis_pakai::getJsonBhp($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data['data'] = (!empty($request->id_bhp)) ? Bahan_habis_pakai::find($request->id_bhp)->first() : "";
        $content = view($this->menuActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        {
            $rules = array(
                'nama_barang'            => 'required',
                'stok'            => 'required',
                'harga_satuan'    => 'required',
                'harga_grosir'    => 'required',
            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );
            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $dtBhp                        = (!empty($request->id_bhp)) ? Bahan_habis_pakai::find($request->id_bhp) : new Bahan_habis_pakai;
                $dtBhp->nama_barang           = $request->nama_barang;
                $dtBhp->stok                  = $request->stok;
                $dtBhp->harga_satuan          = $request->harga_satuan;
                $dtBhp->harga_grosir          = $request->harga_grosir;

                $dtBhp->save();

                if ($dtBhp) {
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
                }
                return response()->json($return);
            } else {
                return $validator->messages();
            }
        }
    }

    public function show(Request $request)
    {
        $id_bhp = $request->id_bhp;
        $data['data'] = Bahan_habis_pakai::find($id_bhp);

        if ($data['data']) {
            $content = view($this->menuActive.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function delete(Request $request)
    {
        $id_bhp = $request->id;
        $do_delete = Bahan_habis_pakai::find($id_bhp);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }

    public function addBhpLokal(Request $request)
    {   
        $harga = str_replace('.', '', $request->harga);
       
        $bhp       =   new Bahan_habis_pakai;
        $bhp->cabang_id    = Auth::getUser()->cabang_id;
        $bhp->nama_barang  = $request->nama_barang;
        $bhp->harga_grosir = $harga;
        $bhp->harga_satuan = $harga;
        $bhp->stok         = $request->stok;
        $bhp->save();

        if ($bhp) {

            $nrc             = new Neraca;
            $nrc->tgl_neraca = date('Y-m-d H:i:s');
            $nrc->kode_reff  = "122";
            $nrc->users_id   = Auth::getUser()->id;
            $nrc->informasi  = "Pengadaan Alkes/ BHP : @".$request->stok.' '.$request->nama_barang;
            $nrc->debit      = 0;
            $nrc->kredit     = $harga*$request->stok;
            $nrc->cabang_id  = Auth::getUser()->cabang_id;
            $nrc->save();

            return Redirect(route('bhp'));
        }else{
            return Redirect::back()->withErrors(['msg', 'Pengadaan Alkes Gagal']);
        }
    }
}
