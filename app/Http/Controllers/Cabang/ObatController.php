<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Obat;
use App\Models\Neraca;
use App\Http\Libraries\CusFormat;
use App\Models\Pengeluaran;
use Session, Redirect, Validator, DB, Auth;

class ObatController extends Controller
{
    private $title = "Master Obat";
    private $menuActive = "obat";
    private $submnActive = "stok_obat";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function lokal(Request $request)
    {
        $this->data['title'] = "Pengadaan Lokal";
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = "pengadaan_lokal";
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.lokal')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Obat::getJsonObat($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
// return $data;
        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd')->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        {
            $rules = array(
                'kode_obat'    => 'required',
                'nama_obat'    => 'required',
                'jenis'        => 'required',
                'jumlah'       => 'required',
                'harga'        => 'required',
                'satuan'       => 'required',
                'stok'         => 'required',
                'tanggal_exp'  => 'required',
            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );
            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $newdata                        = (!empty($request->id_obat)) ? Obat::find($request->id_obat) : new Obat;
                $newdata->kode_obat             = $request->kode_obat;
                $newdata->nama_obat             = $request->nama_obat;
                $newdata->jenis                 = $request->jenis;
                $newdata->jumlah                = $request->jumlah;
                $newdata->harga                 = $request->harga;
                $newdata->satuan                = $request->satuan;
                $newdata->stok                  = $request->stok;
                $newdata->tanggal_exp           = date('Y-m-d', strtotime($request->tanggal_exp));

                $newdata->save();

                $pengeluaran = new Pengeluaran;
                $pengeluaran->jenis       = 'obat';
                $pengeluaran->kode_reff   = $request->kode_obat;
                $pengeluaran->nama_reff   = $request->nama_obat;
                $pengeluaran->qty         = $request->stok;
                $pengeluaran->nominal     = $request->harga;
                $pengeluaran->save();

                if ($newdata) {
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
                }
                return response()->json($return);
            } else {
                return $validator->messages();
            }
        }
    }

    public function show(Request $request)
    {
        $id_obat = $request->id_obat;
        $data['data'] = Obat::find($id_obat);

        if ($data['data']) {
            $content = view($this->menuActive.'.'.$this->submnActive.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }

        return ['status'=>'failed','content'=>'error'];
    }

    public function update(Request $request)
    {
        $id_obat = $request->id;
        $data['obat'] = Obat::find($id_obat);

        $content = view($this->menuActive.'.'.$this->submnActive.'.update',$data)->render();
        return ['status'=>'success','content'=>$content];
        return ['status'=>'failed','content'=>''];
    }

    public function do_update(Request $request)
    {
        $id_obat = $request->id_obat;
        $update            = Obat::find($id_obat);
        $newdata->kode_obat             = $request->kode_obat;
        $newdata->nama_obat             = $request->nama_obat;
        $newdata->jenis                 = $request->jenis;
        $newdata->jumlah                = $request->jumlah;
        $newdata->harga                 = $request->harga;
        $newdata->satuan                = $request->satuan;
        $newdata->stok                  = $request->stok;
        $newdata->tanggal_exp           = date('Y-m-d', strtotime($request->tanggal_exp));

        $update->save();

        if ($update) {
            $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return $return;
    }

    public function delete(Request $request)
    {
        $id_obat = $request->id;
        $do_delete = Obat::find($id_obat);
        $do_delete->delete();

        if($do_delete){
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }

    public function addObatLokal(Request $request)
    {
        $harga = str_replace('.', '', $request->harga);
       
        $obat       =   new Obat;
        $obat->cabang_id    = Auth::getUser()->cabang_id;
        $obat->kode_obat    = $request->kode_obat;
        $obat->nama_obat    = $request->nama_obat;
        $obat->jenis        = $request->jenis;
        $obat->harga        = $harga;
        $obat->satuan       = $request->satuan;
        $obat->stok         = $request->stok;
        $obat->jumlah       = $request->stok;
        $obat->save();

        if ($obat) {

            $nrc             = new Neraca;
            $nrc->tgl_neraca = date('Y-m-d H:i:s');
            $nrc->kode_reff  = "122";
            $nrc->users_id   = Auth::getUser()->id;
            $nrc->informasi  = "Pengadaan Obat : @".$request->stok.' '.$request->nama_obat;
            $nrc->debit      = 0;
            $nrc->kredit     = $harga*$request->stok;
            $nrc->cabang_id  = Auth::getUser()->cabang_id;
            $nrc->save();

            return Redirect(route('obat'));
        }else{
            return Redirect::back()->withErrors(['msg', 'Pengadaan Obat Gagal']);
        }
    }
}
