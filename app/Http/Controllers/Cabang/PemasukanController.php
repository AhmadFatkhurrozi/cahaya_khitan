<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use App\Models\Biaya;
use App\Models\Pengeluaran;
use Illuminate\Http\Request;
use App\Models\Master_gaji;
use App\Models\Users;
use Illuminate\Support\Facades\DB;
use Validator;
use Carbon\Carbon;

class PemasukanController extends Controller
{
    private $title = "Pemasukan";
    private $menuActive = "pemasukan";

    public function main(Request $request)
    {
        $now = Carbon::now();

        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = "Pemasukan";
        $this->data['smallTitle'] = "";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => Biaya::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))->groupby('year','month', 'longMonth')->get(),
        ];

        return view('cabang/pemasukan/main', $data)->with('data', $this->data);
    }


    public function datagrid(Request $request)
    {
        $data = Biaya::getJsonBiaya($request);
        return response()->json($data);
    }

    public function detailPem(Request $request)
    {
        $data = [
            'bulan' => $request->longMonth,
            'tahun' => $request->year,
            'data'  => Biaya::select(DB::raw('biayas.created_at, biayas.pasien_id, pasiens.nama, metode_sunats.nama_metode, metode_sunats.harga_metode, users.name'))
                        ->whereMonth('biayas.created_at', '=', $request->month)
                        ->whereYear('biayas.created_at', '=', $request->year)
                        ->join('metode_sunats', 'metode_sunats.id_metode', 'biayas.metode_id')
                        ->join('pasiens', 'pasiens.id_pasien', 'biayas.pasien_id')
                        ->join('users', 'users.id', 'pasiens.users_id')
                        ->leftJoin('daftar_mainans', 'daftar_mainans.id_mainan', 'biayas.mainan_id')
                        ->get(),
        ];

        $content = view('cabang/pemasukan/detail',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function searchPem(Request $request)
    {
        $now = Carbon::now();

        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = "Pemasukan";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => Biaya::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))->groupby('year','month', 'longMonth')->get(),
            'filter'     => Biaya::select(DB::raw('YEAR(created_at) year, MONTH(created_at) month, monthname(created_at) longMonth'))->groupby('year','month', 'longMonth')->get(),
        ];

        return view('cabang/pemasukan/search', $data)->with('data', $this->data);
    }
}
