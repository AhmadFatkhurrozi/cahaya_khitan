<?php

namespace App\Http\Controllers\Cabang;

use App\Http\Controllers\Controller;
use App\Models\PermintaanObat;
use Illuminate\Http\Request;
use App\Http\Libraries\CusFormat;
use App\Models\Item_permintaan;
use App\Models\Users;
use App\Models\Admin_obat;
use App\Models\Neraca;
use App\Models\Obat;
use App\User;
use App\Models\Daftar_cabang;;

use Auth, Redirect, Validator, DB, Session;

class PermintaanObatController extends Controller
{
    private $title = "Permintaan Obat";
    private $menuActive = "obat";
    private $submnActive = "permintaan_obat";

    public function main()
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = PermintaanObat::getJsonPermintaan($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {       
        $data['obat'] = Admin_obat::all();
        $data['permintaan'] = Daftar_cabang::join('users','daftar_cabangs.id_cabang', '=', 'users.cabang_id')->where('users.id', Auth::User()->id)->first();

        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {   
        $rules = array(
            'obat_id'           => 'required',
            'jumlah_permintaan' => 'required',
            'komentar'          => 'required',
        );

        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );

        $getID = Daftar_cabang::join('users','daftar_cabangs.id_cabang', '=', 'users.cabang_id')
        ->where('users.id', Auth::User()->id)->first();
        $validator  = Validator::make($request->all(), $rules, $messages);
        if ($request->jumlah_permintaan <= $request->stok && !$validator->fails()) { 

            $save_permin                      = new PermintaanObat;
            $save_permin->users_id            = Auth::User()->id;
            $save_permin->cabang_id           = $getID->id_cabang;
            $save_permin->obat_id             = $request->obat_id;       
            $save_permin->qty                 = $request->jumlah_permintaan;
            $save_permin->keterangan          = $request->komentar;
            $save_permin->status_permintaan   = 'menunggu';
            $save_permin->save();  

            if ($save_permin){
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        
        } elseif($request->jumlah_permintaan > $request->stok) {
            return ['status'=>'error', 'code'=>'400', 'message'=>'Maaf!! Stok Tidak Mencukupi !!'];
        } else {
            return $validator->messages();
        }
    }  

    public function show(Request $request)
    {
        $id_permintaan = $request->id_permintaan_obat;
        $data['permin'] = PermintaanObat::select('id_permintaan_obat', 'nama_cabang', 'permintaan_obats.created_at', 'permintaan_obats.keterangan', 'nama_obat', 'permintaan_obats.qty', 'status_permintaan')
                                        ->where('permintaan_obats.id_permintaan_obat', $id_permintaan)
                                        ->join('admin_obats as ao', 'ao.id', 'permintaan_obats.obat_id')
                                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'permintaan_obats.cabang_id')
                                        ->get();

        $data['stok'] = Admin_obat::where('id', $request->obat_id)->first();
        if ($data['permin']) {
            $content = view($this->menuActive.'.'.$this->submnActive.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function konfirmasi_permintaan(Request $request)
    {        
        $id_permintaan_obat = $request->id_permintaan_obat;
        $keterangan         = $request->keterangan;
        $users_id           = $request->users_id;
        $nama_obat          = $request->nama_obat;   
        $obat_id            = $request->obat_id;
        $harga_obat         = $request->harga_obat;
        $qty                = $request->qty;
        $total              = $request->total;
        $cabang_id          = $request->cabang_id;
        $nama_cabang        = $request->nama_cabang;    
        $kode_obat          = $request->kode_obat;

        // Tabel Permintaan
        $FileApp = PermintaanObat::find($id_permintaan_obat);
        $FileApp->status_permintaan = "selesai";
        $FileApp->save();

        // Tabel Admin Obat
        DB::select("UPDATE admin_obats SET stok_obat=stok_obat-".$qty." WHERE kode_obat =".'"'.$kode_obat.'"');

        // Tabel Neraca Admin
        $neraca             = new Neraca;
        $neraca->tgl_neraca = date('Y-m-d H:i:s');
        $neraca->kode_reff  = "411";
        $neraca->users_id   = Auth::getUser()->id;
        $neraca->informasi  = "Permintaan Obat : @".$qty.' '.$nama_obat.'-'.$nama_cabang;
        $neraca->debit      = $total;
        $neraca->kredit     = 0;
        $neraca->cabang_id  = 0;
        $neraca->save();

        if ($neraca) {
            // Tabel Neraca Cabang
            $nrc             = new Neraca;
            $nrc->tgl_neraca = date('Y-m-d H:i:s');
            $nrc->kode_reff  = "122";
            $nrc->users_id   = $users_id;
            $nrc->informasi  = "Persediaan Obat : @".$qty.' '.$nama_obat;
            $nrc->debit      = 0;
            $nrc->kredit     = $total;
            $nrc->cabang_id  = $cabang_id;
            $nrc->save();
        }

        if ($nrc) {
            return ['status' => 'success','id_permintaan_obat'=>$id_permintaan_obat, 'status_permintaan' => "Sukses"];   
        }else{
            return ['status' => 'Gagal','id_permintaan_obat'=>$id_permintaan_obat, 'status_permintaan' => "Gagal"];   
        }
    }

    public function konfirmasi_pesanan(Request $request)
    {        
        $id_permintaan_obat = $request->id_permintaan_obat;
        $keterangan         = $request->keterangan;
        $users_id           = $request->users_id;
        $nama_obat          = $request->nama_obat;   
        $obat_id            = $request->obat_id;
        $harga_obat         = $request->harga_obat;
        $qty                = $request->qty;
        $total              = $request->total;
        $cabang_id          = $request->cabang_id;
        $nama_cabang        = $request->nama_cabang;    
        $kode_obat          = $request->kode_obat;
        $jenis_obat         = $request->jenis_obat;
        $satuan_obat        = $request->satuan_obat;

        // Tabel Permintaan
        $FileApp = PermintaanObat::find($id_permintaan_obat);
        $FileApp->status_permintaan = "diterima";
        $FileApp->save();

        $cek_kode_obat = Obat::where('kode_obat', $kode_obat)->where('cabang_id', $cabang_id)->first();

        // Tabel Master Obat
        if( $cek_kode_obat != ''){
            DB::select("UPDATE master_obat SET stok=stok+".$qty." WHERE kode_obat =".'"'.$kode_obat.'"');
        }else{
            $obat       =   new Obat;
            $obat->cabang_id    = $cabang_id;
            $obat->kode_obat    = $kode_obat;
            $obat->nama_obat    = $nama_obat;
            $obat->jenis        = $jenis_obat;
            $obat->jumlah       = $qty;
            $obat->harga        = $harga_obat;
            $obat->satuan       = $satuan_obat;
            $obat->stok         = $qty;
            $obat->save();
        }

        if ($FileApp) {
            return ['status' => 'success','id_permintaan_obat'=>$id_permintaan_obat, 'status_permintaan' => "Sukses"];   
        }else{
            return ['status' => 'Gagal','id_permintaan_obat'=>$id_permintaan_obat, 'status_permintaan' => "Gagal"];   
        }
    }

    public function reject(Request $request)
    {
        $id = $request->id_permintaan_obat;
        $permin = PermintaanObat::find($id);
        $permin->status_permintaan = "tolak";
        $permin->save();

        if($permin){
            return ['status' => 'success'];
        }else{
            return ['status' => 'error'];
        }
    }

    public function delete(Request $request)
    {
        $id = $request->id_permintaan_obat;
        $do_delete = PermintaanObat::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}