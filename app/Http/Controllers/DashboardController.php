<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Biaya;
use App\Models\Daftar_cabang;
use App\Models\Pengeluaran;
use App\Models\Pasien;
use App\Models\Neraca;
use Auth, Redirect, Validator, Session;

class DashboardController extends Controller
{
    private $title = "Dashboard";
    private $menuActive = "dashboard";
    private $submnActive = "";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        $now        = Carbon::now();
        $id_cabang  = Auth::user()->cabang_id;

        $dateNow    = date('Y-m-d');
        $dateEnd    = date('Y-m-d',strtotime('+28 day'));

        if (Auth::user()->level_user == 1) {
            $omset  = Neraca::selectRaw('SUM(neraca.debit) as omset')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', 0)
                            ->whereYear('tgl_neraca', $now->year)
                            ->whereMonth('tgl_neraca', $now->month)
                            ->first();

            $laba_rugi = Neraca::selectRaw('SUM(debit - kredit) as laba_rugi')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', 0)
                            ->whereYear('tgl_neraca', $now->year)
                            ->whereMonth('tgl_neraca', $now->month)->first();

            $di_khitan  = Pasien::where('status_khitan', 'khitan')
                                ->whereYear('pasiens.created_at', $now->year)
                                ->whereMonth('pasiens.created_at', $now->month)
                                ->count();

            $ggl_khitan = Pasien::where('status_khitan', 'batal')
                                ->whereYear('pasiens.created_at', $now->year)
                                ->whereMonth('pasiens.created_at', $now->month)
                                ->count();

            $menunggu   = Pasien::where('status_khitan', 'menunggu')
                                ->whereYear('pasiens.created_at', $now->year)
                                ->whereMonth('pasiens.created_at', $now->month)
                                ->count();

            $pasien     = Pasien::whereBetween('pasiens.tanggal', [$dateNow, $dateEnd])
                                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'pasiens.cabang_id')
                                ->where('status_khitan', 'menunggu')
                                ->orderBy('pasiens.tanggal', 'ASC')->get();

            $referensi = Pasien::selectRaw('referensi as name')->groupBy('referensi')->get();
            if (!empty($referensi)) {
                foreach ($referensi as $dk) {                
                    $arr = [];
                    $total = DB::select("SELECT COUNT(referensi) AS data FROM pasiens WHERE referensi = '$dk->name' GROUP BY referensi");
                        $neh = (!empty($total)) ? $total[0]->data : 0;

                        array_push($arr, (int)$neh);

                    $dk->data = $arr;
                }
            }

            $grafik_dk = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->get();
            if (!empty($grafik_dk)) {
                foreach ($grafik_dk as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(p.status_khitan = 'khitan')IS NOT NULL, SUM(p.status_khitan = 'khitan'), 0) AS data
                            FROM  pasiens p 
                            WHERE p.cabang_id = '$dk->id_cabang' AND  MONTH(p.tanggal) = '$i' AND YEAR(p.tanggal) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

            $grafik_gk = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->get();
            if (!empty($grafik_gk)) {
                foreach ($grafik_gk as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(p.status_khitan = 'batal')IS NOT NULL, SUM(p.status_khitan = 'batal'), 0) AS data
                            FROM  pasiens p 
                            WHERE p.cabang_id = '$dk->id_cabang' AND  MONTH(p.tanggal) = '$i' AND YEAR(p.tanggal) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

            $grafik_pc = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->get();
            if (!empty($grafik_pc)) {
                foreach ($grafik_pc as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(n.debit)IS NOT NULL, SUM(n.debit), 0) AS data
                            FROM  neraca n 
                            WHERE n.cabang_id = '$dk->id_cabang' AND  MONTH(n.tgl_neraca) = '$i' AND YEAR(n.tgl_neraca) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

            $grafik_lr = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->get();
            if (!empty($grafik_lr)) {
                foreach ($grafik_lr as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(n.debit - n.kredit)IS NOT NULL, SUM(n.debit - n.kredit), 0) AS data
                            FROM  neraca n 
                            WHERE n.cabang_id = '$dk->id_cabang' AND  MONTH(n.tgl_neraca) = '$i' AND YEAR(n.tgl_neraca) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

        }else{

            $omset  = Neraca::selectRaw('SUM(neraca.debit) as omset')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->where('neraca.cabang_id', $id_cabang)
                            ->whereYear('tgl_neraca', $now->year)
                            ->whereMonth('tgl_neraca', $now->month)
                            ->first();

            $laba_rugi = Neraca::selectRaw('SUM(debit - kredit) as laba_rugi')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->where('neraca.cabang_id', $id_cabang)
                            ->whereYear('tgl_neraca', $now->year)
                            ->whereMonth('tgl_neraca', $now->month)->first();

            $di_khitan  = Pasien::where('status_khitan', 'khitan')
                                ->whereYear('pasiens.created_at', $now->year)
                                ->whereMonth('pasiens.created_at', $now->month)
                                ->where('pasiens.cabang_id', $id_cabang)->count();

            $ggl_khitan = Pasien::where('status_khitan', 'batal')
                                ->whereYear('pasiens.created_at', $now->year)
                                ->whereMonth('pasiens.created_at', $now->month)
                                ->where('pasiens.cabang_id', $id_cabang)->count();

            $menunggu   = Pasien::where('status_khitan', 'menunggu')
                                ->whereYear('pasiens.created_at', $now->year)
                                ->whereMonth('pasiens.created_at', $now->month)
                                ->where('pasiens.cabang_id', $id_cabang)->count();

            $pasien = Pasien::whereBetween('pasiens.tanggal', [$dateNow, $dateEnd])
                            ->where('cabang_id', $id_cabang)
                            ->where('status_khitan', 'menunggu')
                            ->orderBy('pasiens.tanggal', 'ASC')->get();
          
            $referensi = Pasien::selectRaw('referensi as name')->where('cabang_id', $id_cabang)->groupBy('referensi')->get();
            if (!empty($referensi)) {
                foreach ($referensi as $dk) {                
                    $arr = [];
                    $total = DB::select("SELECT COUNT(referensi) AS data FROM pasiens WHERE cabang_id = '$id_cabang' AND referensi = '$dk->name' GROUP BY referensi");
                        $neh = (!empty($total)) ? $total[0]->data : 0;

                        array_push($arr, (int)$neh);

                    $dk->data = $arr;
                }
            }

            $grafik_dk = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->where('id_cabang', $id_cabang)->get();
            if (!empty($grafik_dk)) {
                foreach ($grafik_dk as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(p.status_khitan = 'khitan')IS NOT NULL, SUM(p.status_khitan = 'khitan'), 0) AS data
                            FROM  pasiens p 
                            WHERE p.cabang_id = '$dk->id_cabang' AND  MONTH(p.tanggal) = '$i' AND YEAR(p.tanggal) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

            $grafik_gk = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->where('id_cabang', $id_cabang)->get();
            if (!empty($grafik_gk)) {
                foreach ($grafik_gk as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(p.status_khitan = 'batal')IS NOT NULL, SUM(p.status_khitan = 'batal'), 0) AS data
                            FROM  pasiens p 
                            WHERE p.cabang_id = '$dk->id_cabang' AND  MONTH(p.tanggal) = '$i' AND YEAR(p.tanggal) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

            $grafik_pc = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->where('id_cabang', $id_cabang)->get();
            if (!empty($grafik_pc)) {
                foreach ($grafik_pc as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(n.debit)IS NOT NULL, SUM(n.debit), 0) AS data
                            FROM  neraca n 
                            WHERE n.cabang_id = '$dk->id_cabang' AND  MONTH(n.tgl_neraca) = '$i' AND YEAR(n.tgl_neraca) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }

            $grafik_lr = Daftar_cabang::selectRaw("id_cabang, nama_cabang as name")->where('id_cabang', $id_cabang)->get();
            if (!empty($grafik_lr)) {
                foreach ($grafik_lr as $dk) {                
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(n.debit - n.kredit)IS NOT NULL, SUM(n.debit - n.kredit), 0) AS data
                            FROM  neraca n 
                            WHERE n.cabang_id = '$dk->id_cabang' AND  MONTH(n.tgl_neraca) = '$i' AND YEAR(n.tgl_neraca) = YEAR(CURDATE())");
                        $neh = (!empty($total)) ? $total[0]->data : 0;
                        array_push($arr, (int)$neh);
                    }

                    $dk->data = $arr;
                }
            }
        }

        $data_pasien = [];
        if(!empty($pasien)){
            foreach ($pasien as $pas) {
                $datanya = ['title'=>$pas->nama,'start'=>$pas->tanggal];
                array_push($data_pasien,$datanya);
            }
        }

        $data = [
            'waktu'         => $now,
            'di_khitan'     => $di_khitan,
            'ggl_khitan'    => $ggl_khitan,
            'menunggu'      => $menunggu,
            'omset'         => $omset,
            'labarugi'      => $laba_rugi,
            'referensi'     => $referensi,
            'kalender'      => json_encode($data_pasien),
            'tahun'         => "Tahun ".date('Y'),
            'grafik_dk'     => $grafik_dk,
            'grafik_gk'     => $grafik_gk,
            'grafik_pc'     => $grafik_pc,
            'grafik_lr'     => $grafik_lr,
        ]; 

        return view($this->menuActive.'.main', $data)->with('data', $this->data);
    }
}
