<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\FeeOperator;
use Validator, Session,Redirect,Auth;

class FeeOperatorController extends Controller
{
    private $title = "Data Fee Operator";
    private $menuActive = "cabang";
    private $submnActive = "fee_operator";

    public function main(Request $request)
    {   
        $now = Carbon::now();

        $this->data['title']        = $this->title;
        $this->data['mn_active']    = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle']   = "";
        $this->data['waktu']        = $now;

        $data = [
            'bulanTahun' => FeeOperator::selectRaw('YEAR(fee_masters.created_at) year, MONTH(fee_masters.created_at) month, monthname(fee_masters.created_at) longMonth, SUM(pasiens.fee_operator + pasiens.fee_asisten) as total, users_id')
                                        ->leftJoin('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                        ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                        ->groupBy('year','month', 'longMonth')->get(),

            'tahun' => FeeOperator::selectRaw('YEAR(fee_masters.created_at) year, MONTH(fee_masters.created_at) month')
                                        ->leftJoin('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                        ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                        ->groupBy('year')->get(),
        ];

        return view($this->menuActive.'.'.$this->submnActive.'.main', $data)->with('data', $this->data);
    }

    public function search(Request $request)
    {   
        $month = $request->month;
        $year  = $request->year;

        $this->data['title']        = $this->title;
        $this->data['mn_active']    = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle']   = "";

        $data = [
            'bulan_ini' => $month.'-'.$year,
            'bulanTahun' => FeeOperator::selectRaw('YEAR(fee_masters.created_at) year, MONTH(fee_masters.created_at) month, monthname(fee_masters.created_at) longMonth')
                                        ->leftJoin('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                        ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                        ->groupBy('year','month', 'longMonth')->get(),

            'tahun' => FeeOperator::selectRaw('YEAR(fee_masters.created_at) year, MONTH(fee_masters.created_at) month')
                                        ->leftJoin('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                        ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                        ->groupBy('year')->get(),

            'fee'   =>  FeeOperator::selectRaw('YEAR(fee_masters.created_at) year, MONTH(fee_masters.created_at) month, monthname(fee_masters.created_at) longMonth, SUM(pasiens.fee_operator + pasiens.fee_asisten) as total, users_id')
                                        ->leftJoin('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                        ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                        ->whereMonth('fee_masters.created_at', $month)
                                        ->whereYear('fee_masters.created_at', $year)
                                        ->groupBy('year','month', 'longMonth')
                                        ->get(),
        ];

        return view($this->menuActive.'.'.$this->submnActive.'.search', $data)->with('data', $this->data);
    }

    public function perOp(Request $request)
    {   
        $data = [
            'operator'      => FeeOperator::selectRaw('name, users_id, YEAR(fee_masters.created_at) year, MONTH(fee_masters.created_at) month, monthname(fee_masters.created_at) longMonth, SUM(pasiens.fee_operator + pasiens.fee_asisten) as total')
                                        ->whereMonth('fee_masters.created_at', '=', $request->month)
                                        ->whereYear('fee_masters.created_at', '=', $request->year)
                                        ->join('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                        ->leftJoin('users', 'users.id', 'pasiens.users_id')
                                        ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                        ->groupby('name')->get(),

            'longMonth'    => $request->longMonth,
            'year'         => $request->year,
        ];

        $content = view($this->menuActive.'.'.$this->submnActive.'.perOp', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function detail(Request $request)
    {
        $data = [
            'bulan' => $request->longMonth,
            'tahun' => $request->year,
            'data'  => FeeOperator::selectRaw('pasiens.fee_operator, pasiens.fee_asisten, nama_metode, nama, name, fee_masters.created_at')
                                    ->join('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                    ->leftJoin('users', 'users.id', 'pasiens.users_id')
                                    ->join('metode_sunats', 'pasiens.metode_id', 'metode_sunats.id_metode')
                                    ->whereMonth('fee_masters.created_at', '=', $request->month)
                                    ->whereYear('fee_masters.created_at', '=', $request->year)
                                    ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                    ->get(),
        ];

        $content = view($this->menuActive.'.'.$this->submnActive.'.detail', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function detailOp(Request $request)
    {
        $data = [
            'bulan' => $request->longMonth,
            'tahun' => $request->year,
            'nama'  => $request->name,
            'data'  => FeeOperator::selectRaw('pasiens.fee_operator, pasiens.fee_asisten, nama_metode, nama, fee_masters.created_at')
                                    ->join('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                    ->leftJoin('users', 'users.id', 'pasiens.users_id')
                                    ->join('metode_sunats', 'pasiens.metode_id', 'metode_sunats.id_metode')
                                    ->whereMonth('fee_masters.created_at', '=', $request->month)
                                    ->whereYear('fee_masters.created_at', '=', $request->year)
                                    ->where('pasiens.users_id', $request->users_id)
                                    ->where('fee_masters.cabang_id', Auth::getUser()->cabang_id)
                                    ->get(),
        ];

        $content = view($this->menuActive.'.'.$this->submnActive.'.detailOp', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

}
