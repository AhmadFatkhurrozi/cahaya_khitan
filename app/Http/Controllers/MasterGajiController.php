<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Master_gaji;
use App\Models\Users;
use App\Http\Libraries\CusFormat;
use Validator;
use Session,Redirect,Auth;

class MasterGajiController extends Controller
{
    private $title = "Master Gaji";
    private $menuActive = "master_data";
    private $submnActive = "gaji";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Master_gaji::getJsonMasterGaji($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
// $sudah = Master_gaji::select('users_level')->get();
        $data = [
            'data'  => (!empty($request->id_gaji)) ? Master_gaji::find($request->id_gaji) : ""
        ];
        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        // if ($request->users_level == "Admin") {//     $posisi    = 0; //     $level     = 1; // }elseif($request->users_level == "Owner"){//     $posisi    = 0; //     $level     = 2; // }elseif($request->users_level == "Kepala Cabang"){//     $posisi    = 0; //     $level     = 3; // }elseif($request->users_level == "Operator Cabang"){//     $posisi    = 0; //     $level     = 4; // }elseif($request->users_level == "Bendahara Pusat"){//     $posisi    = 1; //     $level     = 5; // }elseif($request->users_level == "Bendahara Cabang"){//     $posisi    = 2; //     $level     = 5; // }elseif($request->users_level == "Marketing Pusat"){//     $posisi    = 1; //     $level     = 6; // }elseif($request->users_level == "Marketing Cabang"){//     $posisi    = 2; //     $level     = 6; // }

        $rules = array(
                'nominal_gaji'  => 'required',
                'users_level'   => 'required',
                'posisi'        => 'required',
            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );
            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $dtGaji                 = new Master_gaji;
                $dtGaji->nominal_gaji   = str_replace('.', '', $request->nominal_gaji);

                $where = Master_gaji::where('users_level', $request->users_level)->where('posisi', $request->posisi)->get();

                if (count($where) == 0 ) {     
                    $dtGaji->users_level    = $request->users_level;
                    $dtGaji->posisi         = $request->posisi;
                    $dtGaji->save();
                    
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Sudah Ada !!'];
                }

                return response()->json($return);
            } else {
                return $validator->messages();
            }
    }

    public function edit(Request $request)
    {
        $id_gaji = $request->id_gaji;

        $data['data'] = Master_gaji::find($id_gaji);

        $content = view($this->menuActive.'.'.$this->submnActive.'.formUpdate',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function update(Request $request)
    {
        $dtGaji                 = Master_gaji::find($request->id_gaji);
        $dtGaji->nominal_gaji   = str_replace('.', '', $request->nominal_gaji);
        $dtGaji->save();

        if ($dtGaji) {
            $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
        }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return $return;
    }

    public function delete(Request $request)
    {
        $id_gaji = $request->id_gaji;
        $do_delete = Master_gaji::find($id_gaji);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }

}