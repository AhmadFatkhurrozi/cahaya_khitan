<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Users;
use Auth, Redirect, Validator, DB;

class OtherController extends Controller
{
    public function getEmail(Request $request)
    {
      if (!empty($request->id)) {
        $data = Users::where('email', $request->email)->where('id','!=',$request->id)->first();
      }else{
        $data = Users::where('email', $request->email)->first();
      }
      if (!empty($data)) {
        $return =  ['status' => 'success', 'code' => '200', 'message' => 'Data ditemukan !!', 'row' => $data];
      }else{
        $return =  ['status' => 'error', 'code' => '404', 'message' => 'Data tidak ditemukan !!', 'row' => ''];
      }
      return response()->json($return);
    }

    public function getUsername(Request $request)
    {
      if (!empty($request->id)) {
        $data = Users::where('username', $request->username)->where('id','!=',$request->id)->first();
      }else{
        $data = Users::where('username', $request->username)->first();
      }
      if (!empty($data)) {
        $return =  ['status' => 'success', 'code' => '200', 'message' => 'Data ditemukan !!', 'row' => $data];
      }else{
        $return =  ['status' => 'error', 'code' => '404', 'message' => 'Data tidak ditemukan !!', 'row' => ''];
      }
      return response()->json($return);
    }
}
