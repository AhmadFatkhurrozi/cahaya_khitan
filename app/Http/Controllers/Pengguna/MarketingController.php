<?php

namespace App\Http\Controllers\Pengguna;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\Daftar_cabang;
use App\Http\Libraries\CompresImage;
use Auth, Redirect, Validator, DB;

class MarketingController extends Controller
{
    private $title = "Marketing";
    private $menuActive = "pengguna";
    private $submnActive = "marketing";

    public function main(Request $request)
    {
      $this->data['title'] = $this->title;
      $this->data['mn_active'] = $this->menuActive;
      $this->data['submn_active'] = $this->submnActive;
      $this->data['smallTitle'] = "";

      return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
      $data = Users::getJsonMarketing($request);
  		return response()->json($data);
    }

    public function formAdd(Request $request)
    {
      $data['cabang'] = Daftar_cabang::all();

      $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd', $data)->render();
      return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
      $rules = array(
        'email'     => 'required',
        'username'  => 'required',
        'name'      => 'required',
        'gender'    => 'required',
        'address'   => 'required',
        'phone'     => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $cekUsername = Users::where('username', $request->username)->first();
        if (empty($cekUsername)) {
          $cekEmail = Users::where('email', $request->email)->first();
          if (empty($cekEmail)) {
            $dtUser               = new Users;
            $dtUser->username     = $request->username;
            $dtUser->email        = $request->email;
            $dtUser->password     = bcrypt($request->username);
            $dtUser->name         = $request->name;
            $dtUser->gender       = $request->gender;
            $dtUser->address      = $request->address;
            $dtUser->phone        = $request->phone;

            if (Auth::getUser()->level_user == 1) {
                $dtUser->cabang_id    = $request->cabang_id;
            }else{
                $dtUser->cabang_id    = Auth::getUser()->cabang_id;    
            }

            if (!empty($request->photo)) {
              $ukuranFile = filesize($request->photo);
              if ($ukuranFile <= 500000) {
                $ext_foto         = $request->photo->getClientOriginalExtension();
                $filename         = "uploads/users/Marketing/".date('Ymd-His')."_".$request->username.".".$ext_foto;
                $temp_foto        = 'uploads/users/Marketing/';
                $proses           = $request->photo->move($temp_foto, $filename);
                $dtUser->photo    = $filename;
              }else{
                $file=$_FILES['photo']['name'];
                if(!empty($file)){
                    $direktori    = "uploads/users/Marketing/"; //tempat upload foto
                    $name         = 'photo'; //name pada input type file
                    $namaBaru     = "Marketing/".date('Ymd-His')."_".$request->username; //name pada input type file
                    $quality      = 50; //konversi kualitas gambar dalam satuan %
                    $upload       = CompresImage::UploadCompress($namaBaru,$name,$direktori,$quality);
                }
                $ext_foto         = $request->photo->getClientOriginalExtension();
                $dtUser->photo    = $namaBaru.".".$ext_foto;
              }
            }
            $dtUser->level_user   = '6';
            $dtUser->is_banned    = '0';
            $dtUser->save();

            if ($dtUser) {
              $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
              $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
          }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Email Sudah Digunakan, Silahkan menggunakan Email Lain !!'];
          }
        }else{
          $return = ['status'=>'error', 'code'=>'500', 'message'=>'Username Sudah Digunakan, Silahkan menggunakan Username Lain !!'];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
    public function show(Request $request)
    {
      $id = $request->id;
      $data['data'] = Users::find($id);

      if ($data['data']) {
        $content = view($this->menuActive.'.marketing'.'.show',$data)->render();

        return ['status'=>'success','content'=>$content];
      }

      return ['status'=>'failed','content'=>''];
    }

    public function update(Request $request)
    {
        $id = $request->id;
        $data['users'] = Users::find($id);

        $content = view($this->menuActive.'.admin'.'.update',$data)->render();
        return ['status'=>'success','content'=>$content];
    }

    public function do_update(REquest $request)
    {
      $id = $request->id;

      $dtUser               = Users::find($id);
      $dtUser->name         = $request->name;
      $dtUser->gender       = $request->gender;
      $dtUser->address      = $request->address;
      $dtUser->phone        = $request->phone;
      if (!empty($request->photo)) {
        $ukuranFile = filesize($request->photo);
          $ext_foto         = $request->photo->getClientOriginalExtension();
          $filename         = "uploads/users/Admin/".date('Ymd-His')."_".$request->username.".".$ext_foto;
          $temp_foto        = 'uploads/users/Admin/';
          $proses           = $request->photo->move($temp_foto, $filename);
          $dtUser->photo    = $filename;
      }
      $dtUser->save();

      if ($dtUser) {
        $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
      }else{
        $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
      }
      return $return;
    }

    public function delete(Request $request)
	  {
	  	$id = $request->id;
    	$do_delete = Users::find($id);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
	  }
    public function nonActive(Request $request)
    {
        $id = $request->id;
        $user = Users::find($id);
        $user->is_banned = '1';
        $user->save();

        if($user){
            return ['status' => 'success'];
        }
        return ['status'=>'error'];
    }
    public function active(Request $request)
    {
        $id = $request->id;
        $user = Users::find($id);
        $user->is_banned = '0';
        $user->save();

        if($user){
            return ['status' => 'success'];
        }
        return ['status'=>'error'];
    }
}
