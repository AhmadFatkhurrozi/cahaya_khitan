<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Metode_sunat;
use Validator;
use Session,Redirect,Auth;
use App\Http\Libraries\CusFormat;

class MetodeSunatController extends Controller
{
    private $title = "Data Metode Sunat";
    private $menuActive = "master_data";
    private $submnActive = "metode_sunat";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.'.$this->submnActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Metode_sunat::getJsonMetodesunat($request);
        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data['data'] = (!empty($request->id_metode)) ? Metode_sunat::find($request->id_metode)->first() : "";
        $content = view($this->menuActive.'.'.$this->submnActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        {
            $rules = array(
                'nama_metode'   => 'required',
                'harga_metode'  => 'required',
                'fee_operator'  => 'required',
                'fee_asisten'   => 'required',
            );
            $messages = array(
                'required'  => 'Kolom Harus Diisi',
            );
            $validator  = Validator::make($request->all(), $rules, $messages);
            if (!$validator->fails()) {
                $dtMetode                        = (!empty($request->id_metode)) ? Metode_sunat::find($request->id_metode) : new Metode_sunat;
                $dtMetode->nama_metode           = $request->nama_metode;
                $dtMetode->harga_metode          = str_replace('.', '', $request->harga_metode);
                $dtMetode->fee_operator          = str_replace('.', '', $request->fee_operator);
                $dtMetode->fee_asisten           = str_replace('.', '', $request->fee_asisten);

                $dtMetode->save();

                if ($dtMetode) {
                    $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
                }
                return response()->json($return);
            } else {
                return $validator->messages();
            }
        }
    }

    public function show(Request $request)
    {
        $id_metode = $request->id_metode;
        $data['data'] = Metode_sunat::find($id_metode);

        if ($data['data']) {
            $content = view($this->menuActive.'.metode_sunat'.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];

        }
        return ['status'=>'failed','content'=>''];
    }

    public function delete(Request $request)
    {
        $id_metode = $request->id_metode;
        $do_delete = Metode_sunat::find($id_metode);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success'];
        }else{
            return ['status'=>'error'];
        }
    }
}
