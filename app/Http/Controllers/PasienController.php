<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Libraries\Notif;
use App\Models\Pasien;
use App\Models\Biaya;
use App\Models\Metode_sunat;
use App\Models\Users;
use App\Models\Obat;
use App\Models\Neraca;
use App\Models\Daftar_mainan;
use App\Models\FeeOperator;
use App\Models\Bahan_habis_pakai;
use App\Models\Daftar_cabang;
use Validator,Session,Redirect,Auth, DB, PDF, OneSignal;


class PasienController extends Controller
{
    private $title = "Data Pasien";
    private $menuActive = "pasien";
    private $submnActive = "pasien";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.main')->with('data', $this->data);
    }

    public function datagrid(Request $request)
    {
        $data = Pasien::getJsonPasien($request);
        return response()->json($data);
    }
    public function formAdd(Request $request)
    {
        $data['metode_sunat'] = Metode_sunat::all();
        $data['operator'] = Users::where('level_user', '4')->where('cabang_id', Auth::getUser()->cabang_id)->get();

        $content = view($this->menuActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        $rules = array(
            'nama'              => 'required',
            'tgl_lahir'         => 'required',
            'usia'              => 'required',
            'bb'                => 'required',
            'alamat'            => 'required',
            'metode_id'         => 'required',
            'riwayat_alergi'    => 'required',
            'riwayat_terdahulu' => 'required',
            'nama_orangtua'     => 'required',
            'bhp'               => 'required',
            'users_id'          => 'required',
            'tanggal'           => 'required',
            'jam'               => 'required',
            'tempat_khitan'     => 'required',
            'alamat_khitan'     => 'required',
            'referensi'         => 'required',
            'keterangan'        => 'required',
        );
        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );

        $getMetode = Metode_sunat::find($request->metode_id);
        $validator  = Validator::make($request->all(), $rules, $messages);
        if (!$validator->fails()) {

            $dtPasien                        = new Pasien;
            $dtPasien->nama                  = $request->nama;
            $dtPasien->tgl_lahir             = date('Y-m-d', strtotime($request->tgl_lahir));
            $dtPasien->usia                  = $request->usia;
            $dtPasien->bb                    = $request->bb;
            $dtPasien->alamat                = $request->alamat;
            $dtPasien->metode_id             = $request->metode_id;
            $dtPasien->bhp                   = $request->bhp;
            $dtPasien->users_id              = $request->users_id;
            $dtPasien->riwayat_alergi        = $request->riwayat_alergi;
            $dtPasien->riwayat_terdahulu     = $request->riwayat_terdahulu;
            $dtPasien->nama_orangtua         = $request->nama_orangtua;
            $dtPasien->tanggal               = date('Y-m-d', strtotime($request->tanggal));
            $dtPasien->jam                   = date('H:i:s', strtotime($request->jam));
            $dtPasien->tempat                = $request->tempat_khitan;
            $dtPasien->tempat_khitan         = $request->alamat_khitan;
            $dtPasien->gratis_biaya          = $request->gratis_biaya;
            $dtPasien->keterangan            = $request->keterangan;
            $dtPasien->status_khitan         = 'menunggu';
            $dtPasien->referensi             = $request->referensi;
            $dtPasien->cabang_id             = Auth::getUser()->cabang_id;

            if (!empty($dtPasien->gratis_biaya)) {
                $dtPasien->fee_operator          = 0;
                $dtPasien->fee_asisten           = 0;
            }else{
                $dtPasien->fee_operator          = $getMetode->fee_operator;
                $dtPasien->fee_asisten           = $getMetode->fee_asisten;
            }

            $dtPasien->save();

            if ($dtPasien) {
                $namaMetode = Metode_sunat::where('id_metode', $request->metode_id)->first()->nama_metode;
                $namaOp = Users::where('id', $request->users_id)->first()->name;
                $namaCabang = Daftar_cabang::where('id_cabang', Auth::getUser()->cabang_id)->first()->nama_cabang;

                $player = Users::where('cabang_id', Auth::getUser()->cabang_id)->get();
                $arr_player = [];

                if ($player->count() != 0) {
                    foreach ($player as $key) {
                        if($key->player_id != null){
                            array_push($arr_player, $key->player_id);
                        }
                    }
                }

                $judul =  "Khitan a.n ".$request->nama.", Ny/tn ".$request->nama_orangtua.", ".$namaMetode.", Operator: ". $namaOp;
                
                $data = [
                    'judul'     => "Cahaya Khitan ".$namaCabang,
                    'tickerText'=> $judul,
                    'pasien'    => $dtPasien,
                ];

                Notif::SendNotif($judul, $data, $arr_player);

                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }
    }

    public function formUpdate(Request $request)
    {

        $id_pasien = $request->id_pasien;
        $data['metode_sunat'] = Metode_sunat::all();
        $data['operator'] = Users::where('level_user', '4')->get();
        $data['pasien'] = Pasien::find($id_pasien);
        $content = view($this->menuActive.'.formUpdate',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function addupdate(Request $request)
    {
        $id_pasien = $request->id_pasien;
        $getMetode = Metode_sunat::find($request->metode_id);
        $dtPasien                        = Pasien::find($id_pasien);
        $dtPasien->nama                  = $request->nama;
        $dtPasien->tgl_lahir             = date('Y-m-d', strtotime($request->tgl_lahir));
        $dtPasien->usia                  = $request->usia;
        $dtPasien->bb                    = $request->bb;
        $dtPasien->alamat                = $request->alamat;
        $dtPasien->metode_id             = $request->metode_id;
        $dtPasien->fee_operator          = $getMetode->fee_operator;
        $dtPasien->fee_asisten           = $getMetode->fee_asisten;
        $dtPasien->bhp                   = $request->bhp;
        $dtPasien->users_id              = $request->users_id;
        $dtPasien->riwayat_alergi        = $request->riwayat_alergi;
        $dtPasien->riwayat_terdahulu     = $request->riwayat_terdahulu;
        $dtPasien->nama_orangtua         = $request->nama_orangtua;
        $dtPasien->tanggal               = date('Y-m-d', strtotime($request->tanggal));
        $dtPasien->jam                   = date('H:i:s', strtotime($request->jam));
        $dtPasien->tempat                = $request->tempat_khitan;
        $dtPasien->tempat_khitan         = $request->alamat_khitan;
        $dtPasien->status_khitan         = 'menunggu';
        $dtPasien->referensi             = $request->referensi;

        $dtPasien->save();

        if ($dtPasien) {
            $namaMetode = Metode_sunat::where('id_metode', $request->metode_id)->first()->nama_metode;
            $namaOp = Users::where('id', $request->users_id)->first()->name;
            $namaCabang = Daftar_cabang::where('id_cabang', Auth::getUser()->cabang_id)->first()->nama_cabang;

            $player = Users::where('cabang_id', Auth::getUser()->cabang_id)->get();
            $arr_player = [];

            if ($player->count() != 0) {
                foreach ($player as $key) {
                    if($key->player_id != null){
                        array_push($arr_player, $key->player_id);
                    }
                }
            }

            $judul =  "Khitan a.n ".$request->nama.", Ny/tn ".$request->nama_orangtua.", ".$namaMetode.", Operator: ". $namaOp;
            
            $data = [
                'judul'     => "Cahaya Khitan ".$namaCabang,
                'tickerText'=> $judul,
                'pasien'    => $dtPasien,
            ];

            Notif::SendNotif($judul, $data, $arr_player);

            $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Diperbarui !!'];
        }else{
            $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
        }
        return $return;
    }


    public function show(Request $request)
    {
        $id_pasien = $request->id_pasien;
        $data['data'] = Pasien::find($id_pasien);

        if ($data['data']) {
            $content = view($this->menuActive.'.show',$data)->render();

            return ['status'=>'success','content'=>$content];
        }
        return ['status'=>'failed','content'=>''];
    }

    public function delete(Request $request)
    {
        $id_pasien = $request->id_pasien;
        $do_delete = Pasien::find($id_pasien);
        if(!empty($do_delete)){
            $do_delete->delete();
            return ['status' => 'success', 'code'=>'200', 'message'=>'Data Berhasil Dihapus !!'];
        }else{
            return ['status'=>'error'];
        }
    }

    public function konfirmasi_pasien(Request $request)
    {
        $id_pasien = $request->id_pasien;
        $jenis     = $request->jenis;
        $data = [
            'title'   =>'Details Data Pengajuan',
            'pasien'  => Pasien::find($id_pasien),
            'mainan'  => Daftar_mainan::where('cabang_id', Auth::getUser()->cabang_id)->get(),
            'biaya'   => Biaya::where('pasien_id', $id_pasien)->first(),
            'obat'    => Obat::where('jenis', $jenis)->where('master_obat.cabang_id', Auth::getUser()->cabang_id)->get(),
            'bhp'    => Bahan_habis_pakai::where('cabang_id', Auth::getUser()->cabang_id)->get(),
        ];

        $content = view($this->menuActive.'.modal',$data)->render();
        return ['status'=>'success','content'=>$content];
    }

    public function approve_khitan(Request $request)
    {   
        $id_pasien      = $request->id_pasien;
        $status_khitan  = $request->status_khitan;
        $mainan         = $request->mainan;

        $FileApp = Pasien::find($id_pasien);
        $FileApp->status_khitan = $status_khitan;
        $FileApp->save();

        if ($status_khitan == "khitan"){
            $biaya   = new Biaya;
            $biaya->pasien_id = $id_pasien;
            $biaya->metode_id = $request->metode_id;
            if($mainan == "ya"){
                $biaya->mainan_id = $request->mainan_id;
            }
            $biaya->obat_id = implode(", ", $request->obat_id); 
            $biaya->bhp_id = implode(", ", $request->bhp_id); 
            $biaya->save();
            
            $id_obat = implode(",", $request->obat_id); 
            $bhp_id = implode(",", $request->bhp_id);

            DB::select('UPDATE master_obat SET stok=stok-1 WHERE id_obat IN('.$id_obat.')'); // pengurangan stok obat
            DB::select('UPDATE bahan_habis_pakais SET stok=stok-1 WHERE id_bhp IN('.$bhp_id.')'); // pengurangan stok bhp
            DB::select('UPDATE daftar_mainans SET qty_mainan=qty_mainan-1 WHERE id_mainan ='.$request->mainan_id ); // pengurangan stok mainan

            if($biaya){
                $neraca             = new Neraca;
                $neraca->tgl_neraca = date('Y-m-d H:i:s');
                $neraca->kode_reff  = "411";
                $neraca->users_id   = Auth::getUser()->id;
                $neraca->informasi  = "Khitan ".$request->nama_metode.", Pasien: ". $request->nama_pasien;
                $neraca->debit      = $request->harga_metode;
                $neraca->kredit     = 0;
                $neraca->cabang_id  = Auth::getUser()->cabang_id;
                $neraca->save();

                $dtFee              = new FeeOperator;
                $dtFee->pasien_id   = $id_pasien;
                $dtFee->cabang_id   = Auth::getUser()->cabang_id; 
                $dtFee->save();

                if ($dtFee) {
                    $return = ['status' => 'success','id_pasien'=>$id_pasien, 'status_khitan' => $status_khitan];
                }else{
                    $return = ['status'=>'error', 'code'=>'500', 'message'=>'Gagal !!'];
                }
            }

        }else{
            $return = ['status'=>'success', 'code'=>'500', 'message'=>'Khitan Dibatalkan !!'];
        }
        return $return;
    }

    public function rincian_pasien($id)
    {
        $this->data['title'] = "Rincian Data Pasien";
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = $this->submnActive;

        $biaya = Biaya::join('metode_sunats', 'metode_sunats.id_metode', 'biayas.metode_id')
        ->join('pasiens', 'pasiens.id_pasien', 'biayas.pasien_id')
        ->join('master_obat', 'master_obat.id_obat', 'biayas.obat_id')
        ->leftJoin('daftar_mainans', 'daftar_mainans.id_mainan', 'biayas.mainan_id')
        ->where('pasien_id', $id)->first();

        $arr_obat = explode(', ', $biaya->obat_id);
        $arr_bhp  = explode(', ', $biaya->bhp_id);

        $data = [
            'biaya'=> $biaya,
            'obat' => Obat::whereIn('id_obat', $arr_obat)->get(),
            'bhp'  => Bahan_habis_pakai::whereIn('id_bhp', $arr_bhp)->get(),
            'id'   => $id
        ]; 

        return view($this->menuActive.'.rincian', $data)->with('data', $this->data);
    }

    public function cetak_struk($id)
    {
        $data = [
            'biaya' => Biaya::selectRaw('id_biaya, dc.nama_cabang, dc.alamat, dc.telepon, ms.nama_metode, ms.harga_metode, p.nama, p.keterangan, u.name')
            ->join('metode_sunats as ms', 'ms.id_metode', 'biayas.metode_id')
            ->join('pasiens as p', 'p.id_pasien', 'biayas.pasien_id')
            ->join('users as u', 'p.users_id', 'u.id')
            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'p.cabang_id')
            ->where('pasien_id', $id)->first()
        ]; 

        return view('pasien.cetak', $data);

    }
}
