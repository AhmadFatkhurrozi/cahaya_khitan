<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Models\Users;
use Auth, Redirect, Validator;

class AuthController extends Controller
{
    use AuthenticatesUsers;

    protected $redirectTo = '/login';

    public function __construct()
    {
      $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
      $this->data['title'] = 'Login';
      $this->data['next_url'] = empty($request->next_url) ? '' : $request->next_url;
        return view('component.login')->with('data', $this->data);
    }

    public function doLogin(Request $request)
    {
      $cekUsername = Users::where('username', $request->email)->first();
      if (empty($cekUsername)) {
        $cekEmail = Users::where('email', $request->email)->first();
        if (empty($cekEmail)) {
          $return = ['status'=>'error', 'message'=>'Akun Tidak Dapat Ditemukan, Silahkan Cek Kembali Email / Username Anda !!'];
          // return response()->json($return);
        }else{
          $data['email'] = strip_tags($request->email);
        }
      }else{
        $data['username'] = strip_tags($request->email);
      }
      $data['password'] = strip_tags($request->password);
      $cek = Auth::attempt($data);

      if($cek){
        if (Auth::user()->is_banned == 0) {
          date_default_timezone_set('Asia/Jakarta');
          $user = Users::find(Auth::user()->id);
          $user->last_login = date("Y-m-d H:i:s");
          $user->save();

          $return = ['status'=>'success', 'message'=>'Login Berhasil'];
        }else{
          Auth::logout();
          $return = ['status'=>'error', 'message'=>'Akun sedang di Non-Aktfikan, Silahkan menghubungin Admin untuk Aktifkan Akun !!'];
        }
      }else{
        $return = ['status'=>'error', 'message'=>'Email atau Password Salah, silahkan Coba Kembali !!'];
      }
      return response()->json($return);
    }

    public function logout(Request $request)
    {
      $logout = Auth::logout();
        return Redirect::route('login')->with('title', 'Berhasil !')->with('message', 'Anda telah logout.')->with('type', 'success');
    }
}
