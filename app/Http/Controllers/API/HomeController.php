<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Users;
use App\Models\Daftar_cabang;
use App\Models\Pasien;
use App\Models\Neraca;
use Redirect, Validator, Session;

class HomeController extends Controller
{
    public function index(Request $request)
    {
        $id_cabang = Users::select('cabang_id')->where('token', $request->token)->first();
        $level = Users::select('level_user')->where('token', $request->token)->first();

        $now        = Carbon::now();
        $dateNow    = date('Y-m-d');
        $dateEnd    = date('Y-m-d',strtotime('+28 day'));

        $profile = Users::where('token', $request->token)->first();

        if ($level->level_user == '1') {

            $di_khitan     = DB::select("SELECT dc.id_cabang, dc.nama_cabang, if(p.tanggal is null, DATE_FORMAT(CURDATE(),'%M %Y'), DATE_FORMAT(tanggal, '%M %Y')) as bulan , 
                                if(p.id_pasien is null, 0, COUNT(p.id_pasien) ) as jumlah_dikhitan FROM daftar_cabangs as dc 
                                LEFT JOIN pasiens as p ON p.cabang_id = dc.id_cabang 
                                WHERE IF(p.status_khitan is null, 'khitan', p.status_khitan) = 'khitan' &&  IF(p.tanggal is null, DATE_FORMAT(CURDATE(),'%M %Y'), DATE_FORMAT(tanggal, '%M %Y')) = DATE_FORMAT(CURDATE(),'%M %Y') 
                                GROUP BY dc.id_cabang");

            $pasien     = Pasien::whereBetween('pasiens.tanggal', [$dateNow, $dateEnd])
                                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'pasiens.cabang_id')
                                ->where('status_khitan', 'menunggu')
                                ->orderBy('pasiens.tanggal', 'ASC')->get();

        }else{

            $di_khitan  = Pasien::selectRaw('dc.id_cabang, dc.nama_cabang, DATE_FORMAT(tanggal, "%M %Y") as bulan, COUNT(pasiens.id_pasien) as jumlah_dikhitan')
            ->leftJoin('daftar_cabangs as dc', 'dc.id_cabang', 'pasiens.cabang_id')
            ->where('status_khitan', 'khitan')
            ->whereYear('pasiens.created_at', $now->year)
            ->whereMonth('pasiens.created_at', $now->month)
            ->where('pasiens.cabang_id', $id_cabang->cabang_id)
            ->groupBy('pasiens.cabang_id')->get();

            $pasien = Pasien::whereBetween('pasiens.tanggal', [$dateNow, $dateEnd])
                            ->where('cabang_id', $id_cabang->cabang_id)
                            ->where('status_khitan', 'menunggu')
                            ->orderBy('pasiens.tanggal', 'ASC')->get();
        }

        $data_pasien = [];
        if(!empty($pasien)){
            foreach ($pasien as $pas) {
                $datanya = [
                    'id_pasien'         => $pas->id_pasien, 
                    'nama'              => $pas->nama,
                    'tgl_lahir'         => $pas->tgl_lahir,
                    'usia'              => $pas->usia,
                    'bb'                => $pas->bb,
                    'alamat'            => $pas->alamat,
                    'nama_metode'       => $pas->nama_metode,
                    'riwayat_alergi'    => $pas->riwayat_alergi,
                    'riwayat_terdahulu' => $pas->riwayat_terdahulu,
                    'nama_orangtua'     => $pas->nama_orangtua,
                    'referensi'         => $pas->referensi,
                    'tanggal'           => $pas->tanggal,
                    'jam'               => $pas->jam,
                    'tempat_khitan'     => $pas->tempat_khitan,
                    'keterangan'        => $pas->keterangan,
                    'status_khitan'     => $pas->status_khitan,
                    'id_cabang'         => $pas->id_cabang,
                    'nama_cabang'       => $pas->nama_cabang,
                    'telepon'           => $pas->telepon,
                ];
                array_push($data_pasien,$datanya);
            }
        }

        $hasil = date('F Y');

        $data = [
            'rekap'   =>  $hasil,
            'jadwal'  =>  $data_pasien,
            'laporan' =>  $di_khitan,
            'profile' =>  $profile,
        ];

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $data];
        return response()->json($return);

    }

    public function jadwal(Request $request)
    {
        $id_cabang = Users::select('cabang_id')->where('token', $request->token)->first();
        $level = Users::select('level_user')->where('token', $request->token)->first();

        $dateNow = date('Y-m-d');
        $dateEnd = date('Y-m-d',strtotime('+28 day'));

        if ($level->level_user == '1') {
            $jadwal = Pasien::whereBetween('pasiens.tanggal', [$dateNow, $dateEnd])
            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'pasiens.cabang_id')
            ->where('status_khitan', 'menunggu')
            ->orderBy('pasiens.tanggal', 'ASC')
            ->get();
        }else {
            $jadwal = Pasien::whereBetween('pasiens.tanggal', [$dateNow, $dateEnd])
            ->orderBy('pasiens.tanggal', 'ASC')
            ->where('status_khitan', 'menunggu')
            ->where('cabang_id', $id_cabang->cabang_id)->get();
        }

        if(count($jadwal) > 0 ){
            return ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !!', 'data' => $jadwal];
        }else{
            return ['status' => 'success', 'code' => 500, 'message' => 'Data Tidak Ditemukan !!'];
        }
    }


    public function chart(Request $request)
    {
        $year = date('Y');

        $id_cabang  = Users::select('cabang_id')->where('token', $request->token)->first();
        $level      = Users::select('level_user')->where('token', $request->token)->first();

        if ($level->level_user == '1') {

            $di_khitan  = Daftar_cabang::selectRaw("id_cabang, nama_cabang")->get();
            if (!empty($di_khitan)) {
                foreach ($di_khitan as $dk) {
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(p.status_khitan = 'khitan')IS NOT NULL, SUM(p.status_khitan = 'khitan'), 0) AS total_dk
                            FROM  pasiens p 
                            WHERE p.cabang_id = '$dk->id_cabang' AND  MONTH(p.tanggal) = '$i'");
                        $neh = [
                            'bulan'=>$i,
                            'total'=>(!empty($total)) ? $total[0]->total_dk : 0,
                        ];
                        array_push($arr, $neh);
                    }

                    $dk->total_dk = $arr;
                }
            }

            $ggl_khitan  = Daftar_cabang::selectRaw("id_cabang, nama_cabang")->get();
            if (!empty($ggl_khitan)) {
                foreach ($ggl_khitan as $dk) {
                    $arr = [];
                    for ($i=1; $i < 13; $i++) { 
                        $total = DB::select("
                            SELECT IF(SUM(p.status_khitan = 'batal')IS NOT NULL, SUM(p.status_khitan = 'batal'), 0) AS total_gk
                            FROM  pasiens p 
                            WHERE p.cabang_id = '$dk->id_cabang' AND  MONTH(p.tanggal) = '$i'");
                        $neh = [
                            'bulan'=>$i,
                            'total'=>(!empty($total)) ? $total[0]->total_gk : 0,
                        ];
                        array_push($arr, $neh);
                    }

                    $dk->total_gk = $arr;
                }
            }

            $referensi = Pasien::selectRaw('DATE_FORMAT(created_at, "%m") as bulan, referensi, COUNT(referensi) as total')
                                ->whereYear('created_at', $year)
                                ->groupBy('referensi')
                                ->orderBy('created_at', 'ASC')
                                ->get();

            $laba_rugi = Neraca::selectRaw('DATE_FORMAT(tgl_neraca, "%M %Y") as bulan, dc.id_cabang, dc.nama_cabang, SUM(neraca.debit) as omset, SUM(debit - kredit) as laba_rugi')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->groupBy('neraca.cabang_id', 'bulan')->orderBy('bulan')->get();

        }else{

            $di_khitan  = Pasien::selectRaw('DATE_FORMAT(tanggal, "%m") as bulan, dc.id_cabang, dc.nama_cabang, COUNT(pasiens.id_pasien) as total')
                                ->leftJoin('daftar_cabangs as dc', 'dc.id_cabang', 'pasiens.cabang_id')
                                ->where('status_khitan', 'khitan')
                                ->where('pasiens.cabang_id', $id_cabang->cabang_id)
                                ->groupBy('bulan')
                                ->orderBy('bulan', 'ASC')->get();

            $ggl_khitan = Pasien::selectRaw('DATE_FORMAT(tanggal, "%m") as bulan, dc.id_cabang, dc.nama_cabang, COUNT(pasiens.id_pasien) as total')
                                ->leftJoin('daftar_cabangs as dc', 'dc.id_cabang', 'pasiens.cabang_id')
                                ->where('status_khitan', 'batal')
                                ->where('pasiens.cabang_id', $id_cabang->cabang_id)
                                ->groupBy('bulan')
                                ->orderBy('bulan', 'ASC')->get();

            $referensi = Pasien::selectRaw('DATE_FORMAT(created_at, "%m") as bulan, referensi, COUNT(referensi) as total')
                                ->whereYear('created_at', $year)
                                ->where('pasiens.cabang_id', $id_cabang->cabang_id)
                                ->groupBy('referensi')
                                ->orderBy('created_at', 'ASC')
                                ->get();

            $laba_rugi = Neraca::selectRaw('DATE_FORMAT(tgl_neraca, "%M %Y") as bulan, dc.id_cabang, dc.nama_cabang, SUM(neraca.debit) as omset, SUM(debit - kredit) as laba_rugi')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->where('neraca.cabang_id', $id_cabang->cabang_id)
                            ->groupBy('bulan')->orderBy('bulan')->get();
        }

        $data = [
            'di_khitan'     =>  $di_khitan,
            'ggl_khitan'    =>  $ggl_khitan,
            'referensi'     =>  $referensi,
            'report'        =>  $laba_rugi,
        ];

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $data];
        return response()->json($return);

    }

  
}
