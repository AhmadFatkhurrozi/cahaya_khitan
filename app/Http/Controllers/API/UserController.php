<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Validator, Hash, Auth;

class UserController extends Controller
{

    public $successStatus = 200;

    public function login(Request $request){
      // return $request->all();
        $email = strip_tags($request->email);
        $password = strip_tags($request->password);

        $getUser = User::where('email',$email)->orWhere('username',$email)->get();
        if($getUser->count()!=0){
          $getUser = User::where('email',$email)->orWhere('username',$email)->first();
          $data['email'] = $getUser->email;
          $data['password'] = $password;

          $cek = Auth::attempt($data);
          if($cek){
            $user = User::find(Auth::id());
            $user->player_id = $request->player_id;

            if($user->token == null){
              $user->token = str_random(15).substr(md5(date('Y-m-d H:i:s', strtotime('now'))), -15);
            }
            
            $user->save();

            $return = ['status' => 'success', 'code' => 200, 'message' => 'Login Berhasil !', 'data' => $user];
          }else{
            $return = ['status' => 'error', 'code' => 500, 'message' => 'Login Gagal !', 'data' => ''];
          }
        }else{
          $return = ['status' => 'error', 'code' => 500, 'message' => 'User tidak ditemukan !', 'data' => ''];
        }
        return response()->json($return);
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['token'] =  $user->createToken('nApp')->accessToken;
        $success['name'] =  $user->name;

        return response()->json(['success'=>$success], $this->successStatus);
    }

    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }


    public function changePassword(Request $request)
    {
      $rules = array(
        'password_lama'     => 'required',
        'password_baru'     => 'required',
      );
      $messages = array(
        'required'  => 'Kolom Harus Diisi',
      );
      $validator 	= Validator::make($request->all(), $rules, $messages);
      if (!$validator->fails()) {
        $password_baru = $request->password_baru;
        $password_lama = $request->password_lama;

        $user = User::where('token', $request->token)->first();
        if (Hash::check($password_lama,$user->password)) {
          $user->password = Hash::make($password_baru);
          $user->save();

          if ($user) {
            $return = ['status' => 'success', 'code' => 200, 'message' => 'Password Berhasil Diperbaharui !!', 'data' => $user];
          }else{
            $return = ['status' => 'error', 'code' => 500, 'message' => 'Password Gagal Diperbaharui !!', 'data' => []];
          }
        }else{
          $return = ['status' => 'error', 'code' => 500, 'message' => 'Password Lama Tidak Sesuai !!', 'data' => []];
        }
        return response()->json($return);
      } else {
        return $validator->messages();
      }
    }
}
