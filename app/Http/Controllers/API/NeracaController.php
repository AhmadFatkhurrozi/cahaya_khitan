<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Neraca;
use App\Models\Users;

class NeracaController extends Controller
{

    public function index(Request $request)
    {
        $id_cabang = Users::select('cabang_id')->where('token', $request->token)->first();
        $level = Users::select('level_user')->where('token', $request->token)->first();

        if ($level->level_user == '1') {
            $neraca =  Neraca::selectRaw('dc.id_cabang, neraca.id_neraca, neraca.users_id, neraca.tgl_neraca, neraca.kode_reff, u.name, dc.nama_cabang, neraca.informasi, neraca.debit, neraca.kredit')
                        ->join('users as u', 'u.id', 'neraca.users_id')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                        ->orderBy('id_neraca', 'ASC')
                        ->get();

            $total = Neraca::selectRaw('DATE_FORMAT(tgl_neraca, "%M %Y") as bulan, dc.nama_cabang, SUM(debit) as debit, SUM(kredit) as kredit')
                        ->join('users as u', 'u.id', 'neraca.users_id')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                        ->groupBy('bulan', 'dc.nama_cabang')
                        ->get();
        }else{
            $neraca = Neraca::selectRaw('dc.id_cabang, neraca.id_neraca, neraca.users_id, neraca.tgl_neraca, neraca.kode_reff, u.name, dc.nama_cabang, neraca.informasi, neraca.debit, neraca.kredit')
                        ->join('users as u', 'u.id', 'neraca.users_id')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                        ->where('neraca.cabang_id', $id_cabang->cabang_id)
                        ->orderBy('id_neraca', 'ASC')
                        ->get();

            $total = Neraca::selectRaw('DATE_FORMAT(tgl_neraca, "%M %Y") as bulan, dc.nama_cabang, SUM(debit) as debit, SUM(kredit) as kredit')
                        ->join('users as u', 'u.id', 'neraca.users_id')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                        ->where('neraca.cabang_id', $id_cabang->cabang_id)
                        ->groupBy('bulan', 'dc.nama_cabang')
                        ->get();
        }

        $data = [
            'neraca'    => $neraca,
            'total'     => $total
        ];

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $data];
        return response()->json($return);
    }
}
