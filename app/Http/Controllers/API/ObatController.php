<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin_obat;
use App\Models\Obat;
use App\Models\Users;

class ObatController extends Controller
{

    public function index(Request $request)
    {
        $id_cabang = Users::select('cabang_id')->where('token', $request->token)->first();
        $level = Users::select('level_user')->where('token', $request->token)->first();

        if ($level->level_user == '1') {
            $obat = Admin_obat::selectRaw('id as id_obat, kode_obat, nama_obat, satuan_obat, harga_obat, stok_obat')
                            ->get();
        }else{
            $obat = Obat::selectRaw('id_obat, kode_obat, nama_obat, satuan as satuan_obat, harga as harga_obat, stok as stok_obat')
                            ->where('cabang_id', $id_cabang->cabang_id)
                            ->get();
        }

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $obat];
        return response()->json($return);
    }
}
