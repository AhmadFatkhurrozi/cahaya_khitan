<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Bahan_habis_pakai;
use App\Models\Admin_bhp;
use App\Models\Users;
class BahanHabisPakaiController extends Controller
{

    public function index(Request $request)
    {
        $id_cabang = Users::select('cabang_id')->where('token', $request->token)->first();
        $level = Users::select('level_user')->where('token', $request->token)->first();

        if ($level->level_user == '1') {
            $bhp = Admin_bhp::selectRaw('id as id_bhp, nama_barang, stok_bhp, harga_satuan, harga_grosir')
                            ->get();
        }else{
            $bhp = Bahan_habis_pakai::selectRaw('id_bhp, nama_barang, stok as stok_bhp, harga_satuan, harga_grosir')
                            ->where('cabang_id', $id_cabang->cabang_id)
                            ->get();
        }

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $bhp];
        return response()->json($return);
    }
}
