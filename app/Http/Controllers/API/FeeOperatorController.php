<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Users;
use App\Models\Pasien;
use App\Models\FeeOperator;

class FeeOperatorController extends Controller
{

    public function index(Request $request)
    {
        $id_cabang = Users::select('cabang_id')->where('token', $request->token)->first();
        $level      = Users::select('level_user')->where('token', $request->token)->first();

        if ($level->level_user == '1') {
             $fee = FeeOperator::selectRaw('fee_masters.id_fee, DATE_FORMAT(fee_masters.created_at, "%d %M %Y") as tgl, users.cabang_id, dc.nama_cabang, pasiens.fee_operator, pasiens.fee_asisten, nama_metode, nama as nama_pasien, name as nama_operator')
                                ->join('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                ->leftJoin('users', 'users.id', 'pasiens.users_id')
                                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'fee_masters.cabang_id')
                                ->join('metode_sunats', 'pasiens.metode_id', 'metode_sunats.id_metode')
                                ->get();
        }else{
             $fee = FeeOperator::selectRaw('fee_masters.id_fee, DATE_FORMAT(fee_masters.created_at, "%d %M %Y") as tgl, users.cabang_id, dc.nama_cabang, pasiens.fee_operator, pasiens.fee_asisten, nama_metode, nama as nama_pasien, name as nama_operator')
                                ->join('pasiens', 'pasiens.id_pasien', 'fee_masters.pasien_id')
                                ->leftJoin('users', 'users.id', 'pasiens.users_id')
                                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'fee_masters.cabang_id')
                                ->join('metode_sunats', 'pasiens.metode_id', 'metode_sunats.id_metode')
                                ->where('fee_masters.cabang_id', $id_cabang->cabang_id)
                                ->get();
        }

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $fee];
        return response()->json($return);
     }
}
