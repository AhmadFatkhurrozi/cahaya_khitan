<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\Users;
use File;

class ProfileController extends Controller
{

    public function index(Request $request)
    {
        $data = Users::where('token', $request->token)->get();

        $return = ['status' => 'success', 'code' => 200, 'message' => 'Data Ditemukan !', 'data' => $data];
        return response()->json($return);
    }

    public function update(Request $request)
    {
        $pass = bcrypt($request->password);

        $profile            = Users::where('token', $request->token)->first();
        if ($request->email != null) {
            $profile->email     = $request->email;
        }

        if ($request->name != null) {
            $profile->name      = $request->name;
        }

        if ($request->address != null) {
            $profile->address   = $request->address;
        }

        if ($request->phone != null) {
            $profile->phone     = $request->phone;
        }
        
        if ($request->password != null) {
            $profile->password = $pass;
        }

        $profile->save();

        if ($profile) {
            $return = ['status'=>'success', 'code' => 200, 'message'=>'Profil berhasil diperbarui', 'data' => $profile];
        }else{
            $return = ['status'=>'error', 'code' => 500, 'message'=>'Update gagal, silahkan Coba Kembali !!', 'data' => ""];
        }

        return response()->json($return);
    }

    public function UpdateFoto(Request $request)
    {   
        $user            = Users::where('token', $request->token)->first();

        if (!empty($request->photo)) {
          if ($user->photo != null) {
            if (is_file($user->photo)) {
              File::delete($user->photo);
            }
          }
          $ext_foto         = $request->photo->getClientOriginalExtension();
          $filename         = "uploads/users/Admin/".date('Ymd-His')."_".$user->username.".".$ext_foto;
          $temp_foto        = 'uploads/users/Admin/';
          $proses           = $request->photo->move($temp_foto, $filename);
          $user->photo      = $filename;
        }

        $user->save();

        if ($user) {
            $return = ['status'=>'success', 'code' => 200, 'message'=>'Foto Profil berhasil diperbarui', 'data' => $user];
        }else{
            $return = ['status'=>'error', 'code' => 500, 'message'=>'Update gagal, silahkan Coba Kembali !!', 'data' => ""];
        }

        return response()->json($return);
    }

    public function lupa_password(Request $request)
    {   
        $email = $request->email;

        $cekEmail = Users::where('email', $email)->first();
        
        if (!empty($cekEmail)) {
            
            $id = Users::where('email', $email)->first()->id;
            
            Mail::send('mail.reset', array('email' => base64_encode($email), 'users_id' => base64_encode($id)) , function($pesan) use($request){
                $pesan->to($request->email)->subject('Reset Password');
                $pesan->from(env('Cahaya Khitan','no_reply@cahayakhitan.com'));
            });

            $return = ['status'=>'success', 'code' => 200, 'message'=>'Email untuk reset password telah dikirim', 'data' => ""];

        }else{
            $return = ['status'=>'error', 'code' => 500, 'message'=>'Email tidak terdaftar', 'data' => ""];
        }

        return response()->json($return);
    }
}
