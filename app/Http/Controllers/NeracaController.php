<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Neraca;
use App\Models\Users;
use App\Models\Daftar_cabang;
use App\Http\Libraries\CusFormat;
use Validator, Session, Redirect, Auth, DB, PDF;

class NeracaController extends Controller
{
    private $title = "Neraca";
    private $menuActive = "neraca";

    public function main(Request $request)
    {
        $this->data['title'] = $this->title;
        $this->data['mn_active'] = $this->menuActive;
        $this->data['submn_active'] = "neraca";
        $this->data['smallTitle'] = "";

        return view($this->menuActive.'.main')->with('data', $this->data);
    }

    public function bagi_hasil()
    {
        $this->data['title'] = "Bagi Hasil";
        $this->data['mn_active'] = "bagi_hasil";
        $this->data['submn_active'] = "bagi_hasil";

        $bulan = date('m');

        $data = [
            'bulan_ini' => date('m-Y'),
            'bulan'     => Neraca::selectRaw('YEAR(tgl_neraca) year, MONTH(tgl_neraca) month, monthname(tgl_neraca) longMonth')
                            ->groupBy('year','month', 'longMonth')
                            ->get(),
            'tahun'     => Neraca::selectRaw('YEAR(tgl_neraca) year')
                            ->groupBy('year')
                            ->get(),
            'mainan'    => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '113')
                            ->whereMonth('tgl_neraca', $bulan)
                            ->first(),
            'bhp'       => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '121')
                            ->whereMonth('tgl_neraca', $bulan)
                            ->first(),
            'obat'      => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '122')
                            ->whereMonth('tgl_neraca', $bulan)
                            ->first(),
            'gaji'      => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '511')
                            ->whereMonth('tgl_neraca', $bulan)
                            ->first(),    
            'fee'       => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '513')
                            ->whereMonth('tgl_neraca', $bulan)
                            ->first(),
            'pemasukan' => Neraca::selectRaw('DATE_FORMAT(tgl_neraca, "%d-%m-%Y") as tgl_neraca, informasi, debit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '411')
                            ->whereMonth('tgl_neraca', $bulan)
                            ->get(),
        ];
        
        return view('neraca.bagi_hasil', $data)->with('data', $this->data);
    }

    public function bagi_hasil_per(Request $request)
    {   
        $month = $request->month;
        $year  = $request->year;

        $data = [
            'bulan_ini' => $month.'-'.$year,
            'bulan'     => Neraca::selectRaw('YEAR(tgl_neraca) year, MONTH(tgl_neraca) month, monthname(tgl_neraca) longMonth')
                            ->groupBy('year','month', 'longMonth')
                            ->get(),
            'tahun'     => Neraca::selectRaw('YEAR(tgl_neraca) year')
                            ->groupBy('year')
                            ->get(),
            'mainan'    => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '113')
                            ->whereMonth('tgl_neraca', $month)
                            ->whereYear('tgl_neraca', $year)
                            ->first(),
            'bhp'       => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '121')
                            ->whereMonth('tgl_neraca', $month)
                            ->whereYear('tgl_neraca', $year)
                            ->first(),
            'obat'      => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '122')
                            ->whereMonth('tgl_neraca', $month)
                            ->whereYear('tgl_neraca', $year)
                            ->first(),
            'gaji'      => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '511')
                            ->whereMonth('tgl_neraca', $month)
                            ->whereYear('tgl_neraca', $year)
                            ->first(),    
            'fee'       => Neraca::selectRaw('SUM(kredit) as total')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '513')
                            ->whereMonth('tgl_neraca', $month)
                            ->whereYear('tgl_neraca', $year)
                            ->first(),
            'pemasukan' => Neraca::selectRaw('DATE_FORMAT(tgl_neraca, "%d-%m-%Y") as tgl_neraca, informasi, debit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->where('kode_reff', '=', '411')
                            ->whereMonth('tgl_neraca', $month)
                            ->whereYear('tgl_neraca', $year)
                            ->get(),
        ];
        
        $content = view('neraca.bagi_hasil_per', $data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function datagrid(Request $request)
    {   
        if ($request->bulan != null) { 
            $data = Neraca::getJsonBulan($request, $request->bulan);
        }else{ 
            $data = Neraca::getJsonNeraca($request);
        }

        return response()->json($data);
    }

    public function neraca_cabang(Request $request)
    {
        $this->data['title'] = "Neraca Cabang";
        $this->data['mn_active'] = "neraca_cabang";
        $this->data['submn_active'] = "neraca_cabang";
        $this->data['smallTitle'] = "";

        $data = [
            'cabang'    =>  Daftar_cabang::select('id_cabang', 'nama_cabang')->get()
        ];
        return view($this->menuActive.'.neraca_cabang', $data)->with('data', $this->data);
    }

    public function datagrid_cabang(Request $request)
    {
        if ($request->bulan != null && $request->cabang != null) { 

            $data = Neraca::getJsonFilter($request, $request->bulan, $request->cabang);
        
        }elseif($request->bulan != null && $request->cabang == null){
            
            $data = Neraca::getJsonFilterBulan($request, $request->bulan);

        }elseif($request->bulan == null && $request->cabang != null){
            
            $data = Neraca::getJsonFilterCabang($request, $request->cabang);

        }else{ 
            $data = Neraca::getJsonNeracaCabang($request);
        }

        return response()->json($data);
    }

    public function formAdd(Request $request)
    {
        $data = [
            'akun'  => DB::table('akun')->get(),
            'data'  => (!empty($request->id_neraca)) ? Neraca::find($request->id_neraca) : "",
        ];
        $content = view($this->menuActive.'.formAdd',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function formUpdate(Request $request)
    {   
        $data = [
            'akun'  => DB::table('akun')->get(),
            'neraca'  => Neraca::join('akun', 'akun.no_reff', 'neraca.kode_reff')->where('id_neraca', $request->id_neraca)->first(),
        ];
        $content = view($this->menuActive.'.formUpdate',$data)->render();
        return ['status' => 'success', 'content' => $content];
    }

    public function add(Request $request)
    {
        $rules = array(
            'tgl_neraca'=> 'required',
            'kode_reff' => 'required',
            'informasi' => 'required',
            'debit'     => 'required',
            'kredit'    => 'required',
            'jenis'     => 'required'
        );

        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );

        $validator  = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {
            $data                 = new Neraca;
            $data->tgl_neraca     = date("Y-m-d H:i:s",strtotime($request->tgl_neraca));
            $data->kode_reff      = $request->kode_reff;
            $data->informasi      = $request->informasi;
            $data->users_id       = Auth::getUser()->id;   

                if (Auth::getUser()->level_user == 1) {
                    $data->cabang_id      = "0";   
                }else{
                    $data->cabang_id      = Auth::getUser()->cabang_id;
                }

                if ($request->jenis == "debit") {
                    $data->debit          = str_replace('.', '', $request->debit);
                    $data->kredit         = "0"; 
                }else{
                    $data->debit          = "0";
                    $data->kredit         = str_replace('.', '', $request->kredit); 
                }
            
            $data->save();

            if ($data) {
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Disimpan !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Disimpan !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }

        // return $request->all();
    }

    public function updateNeraca(Request $request)
    {   
        $rules = array(
            'tgl_neraca'=> 'required',
            'kode_reff' => 'required',
            'informasi' => 'required',
            'debit'     => 'required',
            'kredit'    => 'required',
            'jenis'     => 'required'
        );

        $messages = array(
            'required'  => 'Kolom Harus Diisi',
        );

        $validator  = Validator::make($request->all(), $rules, $messages);

        if (!$validator->fails()) {
            $data                 = Neraca::find($request->id_neraca);
            $data->tgl_neraca     = date("Y-m-d H:i:s",strtotime($request->tgl_neraca));
            $data->kode_reff      = $request->kode_reff;
            $data->informasi      = $request->informasi;
            $data->users_id       = Auth::getUser()->id;   

                if (Auth::getUser()->level_user == 1) {
                    $data->cabang_id      = "0";   
                }else{
                    $data->cabang_id      = Auth::getUser()->cabang_id;
                }

                if ($request->jenis == "debit") {
                    $data->debit          = str_replace('.', '', $request->debit);
                    $data->kredit         = "0"; 
                }else{
                    $data->debit          = "0";
                    $data->kredit         = str_replace('.', '', $request->kredit); 
                }
            
            $data->save();

            if ($data) {
                $return = ['status'=>'success', 'code'=>'200', 'message'=>'Data Berhasil Diperbarui !!'];
            }else{
                $return = ['status'=>'error', 'code'=>'500', 'message'=>'Data Gagal Diperbarui !!'];
            }
            return response()->json($return);
        } else {
            return $validator->messages();
        }

    }

    public function delete(Request $request)
    {   
        $do_delete = Neraca::find($request->id);
        if($do_delete != ""){
            $do_delete->delete();
            return ['status' => 'success', 'code'=>'200', 'message'=>'Data Berhasil Dihapus !!'];
        }else{
            return ['status'=>'error', 'code'=>'500', 'message'=>'Hapus Gagal !!'];
        }
    }

    public function total(Request $request)
    {
        if ($request->bulan != null) {
            $bulan = substr($request->bulan, 0, 2);
            $tahun = substr($request->bulan, 3, 4);

            if (Auth::getUser()->level_user == 1) {
                $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', 0)
                            ->whereMonth('tgl_neraca', '=', $bulan)
                            ->whereYear('tgl_neraca', '=', $tahun)
                            ->get();
            }else{
                $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->whereMonth('tgl_neraca', '=', $bulan)
                            ->whereYear('tgl_neraca', '=', $tahun)
                            ->get();
            }
        }else{
            if (Auth::getUser()->level_user == 1) {
                $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->where('neraca.cabang_id', 0)
                            ->get();
            }else{
                $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                            ->get();
            }
        }

        ($neraca[0]->debit != null) ? $data['debit'] = $neraca[0]->debit : $data['debit'] = 0;
        ($neraca[0]->kredit != null) ? $data['kredit'] = $neraca[0]->kredit : $data['kredit'] = 0;
        
        return ['status' => 'success', 'messages' =>'Hasil Ditemukan', 'data'=>$data];
    }

     public function totalNeracaCabang(Request $request)
    {
        $cabang = $request->cabang;
        $bulan = substr($request->bulan, 0, 2);
        $tahun = substr($request->bulan, 3, 4);
        
        if($request->bulan != null && $request->cabang != null) {

            $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->whereMonth('tgl_neraca', '=', $bulan)
                            ->whereYear('tgl_neraca', '=', $tahun)
                            ->where('neraca.cabang_id', $cabang)
                            ->get();

        }elseif($request->bulan != null && $request->cabang == null) {
            $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->whereMonth('tgl_neraca', '=', $bulan)
                            ->whereYear('tgl_neraca', '=', $tahun)
                            ->get();
        }elseif($request->bulan == null && $request->cabang != null) {
            $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->where('neraca.cabang_id', $cabang)
                            ->get();    
        }else{
            $neraca = Neraca::selectRaw('SUM(debit) as debit, SUM(kredit) as kredit')
                            ->join('users as u', 'u.id', 'neraca.users_id')
                            ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                            ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                            ->get();
        }

        ($neraca[0]->debit != null) ? $data['debit'] = $neraca[0]->debit : $data['debit'] = 0;
        ($neraca[0]->kredit != null) ? $data['kredit'] = $neraca[0]->kredit : $data['kredit'] = 0;
        
        return ['status' => 'success', 'messages' =>'Hasil Ditemukan', 'data'=>$data];
    }

    public function cetak(Request $request)
    {   
        $id         = Auth::getUser()->id;
        $cabang_id  = Auth::getUser()->cabang_id;
        $bulan      = $request->bulan;

        if (Auth::getUser()->level_user == 1) {
            $data['judul'] = $bulan;
            $data['cabang'] = "Pusat";
            $data['neraca'] = Neraca::selectRaw('neraca.kode_reff, neraca.tgl_neraca, neraca.informasi, neraca.debit, neraca.kredit, u.name')
                ->join('users as u', 'u.id', 'neraca.users_id')
                ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                ->where('neraca.cabang_id', 0)
                ->orderBy('tgl_neraca','ASC')->get()->chunk(100);
        }else{
            $data['judul'] = $bulan;
            $data['cabang'] = Daftar_cabang::select('nama_cabang')->where('id_cabang', $cabang_id)->first();
            $data['neraca'] = Neraca::selectRaw('neraca.kode_reff, neraca.tgl_neraca, neraca.informasi, neraca.debit, neraca.kredit, u.name, dc.nama_cabang')
                    ->join('users as u', 'u.id', 'neraca.users_id')
                    ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                    ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                    ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                    ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                    ->where('neraca.cabang_id', Auth::getUser()->cabang_id)
                    ->orderBy('tgl_neraca','ASC')->get()->chunk(100);
        }
        
        // return view('neraca.cetak', $data);

        $pdf = PDF::loadView('neraca.cetak', $data)
                  ->setPaper('a4', 'landscape');

        return $pdf->stream('Cetak Laporan Keuangan.pdf');
    }

    public function cetak_cabang(Request $request)
    {   
        $bulan          = $request->bulan;
        $cabang         = $request->cabang;

        if($cabang != null){
            $nama_cabang    = Daftar_cabang::where('id_cabang', $cabang)->first()->nama_cabang;
        }

        if ($bulan != null && $cabang != null) {

            $data['judul']  = "Bulan".$bulan;
            $data['cabang'] = "Neraca Keuangan ".$nama_cabang;
            $data['neraca'] = Neraca::selectRaw('dc.nama_cabang, neraca.kode_reff, neraca.tgl_neraca, neraca.informasi, neraca.debit, neraca.kredit, u.name')
                ->join('users as u', 'u.id', 'neraca.users_id')
                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                ->where('neraca.cabang_id', '!=', 0)
                ->where('neraca.cabang_id', '=', $cabang)
                ->orderBy('tgl_neraca','ASC')->get()->chunk(100);

        }elseif($bulan != null && $cabang == null){

            $data['judul']  = "Bulan ".$bulan;
            $data['cabang'] = "Neraca Keuangan Semua Cabang";
            $data['neraca'] = Neraca::selectRaw('dc.nama_cabang, neraca.kode_reff, neraca.tgl_neraca, neraca.informasi, neraca.debit, neraca.kredit, u.name')
                ->join('users as u', 'u.id', 'neraca.users_id')
                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                ->where('neraca.cabang_id', '!=', 0)
                ->orderBy('tgl_neraca','ASC')->get()->chunk(100);

        }elseif($bulan == null && $cabang != null){

            $data['judul']  = "";
            $data['cabang'] = "Neraca Keuangan ".$nama_cabang;
            $data['neraca'] = Neraca::selectRaw('dc.nama_cabang, neraca.kode_reff, neraca.tgl_neraca, neraca.informasi, neraca.debit, neraca.kredit, u.name')
                ->join('users as u', 'u.id', 'neraca.users_id')
                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                ->where('neraca.cabang_id', '!=', 0)
                ->where('neraca.cabang_id', '=', $cabang)
                ->orderBy('tgl_neraca','ASC')->get()->chunk(100);

        }else{

            $data['judul']  = "";
            $data['cabang'] = "Neraca Keuangan Semua Cabang";
            $data['neraca'] = Neraca::selectRaw('dc.nama_cabang, neraca.kode_reff, neraca.tgl_neraca, neraca.informasi, neraca.debit, neraca.kredit, u.name')
                ->join('users as u', 'u.id', 'neraca.users_id')
                ->join('daftar_cabangs as dc', 'dc.id_cabang', 'neraca.cabang_id')
                ->join('akun as a', 'a.no_reff', 'neraca.kode_reff')
                ->where('neraca.cabang_id', '!=', 0)
                ->orderBy('tgl_neraca','ASC')->get()->chunk(100);

        }
        
        $pdf = PDF::loadView('neraca.cetak_cabang', $data)
                  ->setPaper('a4', 'landscape');

        return $pdf->stream('Cetak Laporan Keuangan.pdf');
    }

}