<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Authentic
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
          if (Auth::user()->is_banned == 0) {
            return $next($request);
          }else{
            Auth::logout();
            $url = route('login').'?next_url='.$request->path();
            return redirect($url)->with('title', 'Maaf !!')->with('message', 'Akun sedang di Non-Aktfikan, Silahkan menghubungin Admin untuk Aktifkan Akun !!')->with('type', 'warning');
          }
        }else{
            $url = route('login').'?next_url='.$request->path();
            return redirect($url);
        }
        // $url = route('suspended');
        // return redirect($url);
    }
}
