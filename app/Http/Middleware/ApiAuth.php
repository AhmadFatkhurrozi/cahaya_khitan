<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Users;
use Auth;

class ApiAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->token){
            $cek = Users::where('token',$request->token)->first(); //get data user berdasarkan token
            /* cek data user*/
            if ($cek) {
                return $next($request);
            } else {
                $return = ['status'=>'unauthorized', 'code'=>401, 'message' => 'Permintaan anda tidak bisa diproses.'];
                return response()->json($return);
            }
        }else {
            $return = ['status'=>'unauthorized', 'code'=>401, 'message' => 'Permintaan anda tidak bisa diproses.'];
            return response()->json($return);
        }
        return $next($request);
    }
}
