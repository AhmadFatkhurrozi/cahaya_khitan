<?php

namespace App;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CahayaEmail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {  
       return $this->from('cahayakhitan68@gmail.com')
                   ->view('mail.reset')
                   ->with(
                    [
                        'nama' => 'Cahaya Khitan',
                        'website' => 'cahayakhitan.com',
                    ]);
    }
}