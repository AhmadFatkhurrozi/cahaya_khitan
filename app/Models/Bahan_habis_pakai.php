<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Bahan_habis_pakai extends Model
{
    protected $table = 'bahan_habis_pakais';
    protected $primaryKey = 'id_bhp';
    // public $timestamps = true;

    public static function getJsonBhp($input)
    {
      $table  = 'bahan_habis_pakais';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'hargaSa', 'new_name' => 'harga_satuan'],
        ['old_name' => 'hargaGro', 'new_name' => 'harga_grosir'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->where('cabang_id', Auth::getUser()->cabang_id);
      });
      return $data;
    }
}
