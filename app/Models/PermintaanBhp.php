<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class PermintaanBhp extends Model
{
  protected $table = 'permintaan_bhps';
  protected $primaryKey = 'id_permintaan_bhp';
  public $timestamps = true;

  public function daftar_cabang()
  {
      return $this->belongsTo('App\Models\Daftar_cabang','cabang_id');
  }

  public static function getJsonPermintaan($input)
  {
    $table  = 'permintaan_bhps as permin';
    $select = 'cab.nama_cabang,permin.*, bhp.nama_barang, harga_satuan, harga_grosir, stok_bhp';

    $replace_field  = [
      ['old_name' => 'hargaRe', 'new_name' => 'harga'],
      ['old_name' => 'status', 'new_name' => 'status_permintaan'],
    ];

    $param = [
      'input'         => $input->all(),
      'select'        => $select,
      'table'         => $table,
      'replace_field' => $replace_field
    ];
    $datagrid = new Datagrid;
    $data = $datagrid->datagrid_query($param, function($data){
      if (Auth::getUser()->level_user == 1) {
          return $data->join('daftar_cabangs as cab', 'cab.id_cabang', '=', 'permin.cabang_id')
                      ->join('admin_bhps as bhp', 'bhp.id', 'permin.bhp_id');
      }else{
        return $data->where('cabang_id', Auth::getUser()->cabang_id)
                  ->join('daftar_cabangs as cab', 'cab.id_cabang', '=', 'permin.cabang_id')
                  ->join('admin_bhps as bhp', 'bhp.id', 'permin.bhp_id');
      }
    });
    return $data;
  }
}

