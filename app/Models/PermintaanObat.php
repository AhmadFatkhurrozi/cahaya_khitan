<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class PermintaanObat extends Model
{
  protected $table = 'permintaan_obats';
  protected $primaryKey = 'id_permintaan_obat';
  public $timestamps = true;

  public function daftar_cabang()
  {
      return $this->belongsTo('App\Models\Daftar_cabang','cabang_id');
  }

  public static function getJsonPermintaan($input)
  {
    $table  = 'permintaan_obats as permin';
    $select = 'cab.nama_cabang, permin.*, obat.nama_obat, obat.harga_obat, obat.kode_obat, obat.jenis_obat, obat.satuan_obat';

    $replace_field  = [
      ['old_name' => 'hargaRe', 'new_name' => 'harga'],
      ['old_name' => 'status', 'new_name' => 'status_permintaan'],
    ];

    $param = [
      'input'         => $input->all(),
      'select'        => $select,
      'table'         => $table,
      'replace_field' => $replace_field
    ];
    $datagrid = new Datagrid;
    $data = $datagrid->datagrid_query($param, function($data){
      if (Auth::getUser()->level_user == 1) {
          return $data->join('daftar_cabangs as cab', 'cab.id_cabang', '=', 'permin.cabang_id')
                      ->join('admin_obats as obat', 'obat.id', 'permin.obat_id');
      }else{
        return $data->where('cabang_id', Auth::getUser()->cabang_id)
                  ->join('daftar_cabangs as cab', 'cab.id_cabang', '=', 'permin.cabang_id')
                  ->join('admin_obats as obat', 'obat.id', 'permin.obat_id');
      }
    });
    return $data;
  }
}

