<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Daftar_mainan extends Model
{
    protected $table = 'daftar_mainans';
    protected $primaryKey = 'id_mainan';
    // public $timestamps = true;

    public static function getJsonDaftarmainan($input)
    {
      $table  = 'daftar_mainans';
      $select = 'daftar_mainans.*';

      $replace_field  = [
        ['old_name' => 'hargaRe', 'new_name' => 'harga_mainan'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->where('cabang_id', Auth::getUser()->cabang_id);
      });
      return $data;
    }
}
