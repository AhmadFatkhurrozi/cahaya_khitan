<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class Item_permintaan extends Model
{
  protected $table = 'item_permintaan_obat';
  protected $primaryKey = 'id_items';
  // public $timestamps = true;

   public function obat()
    {
        return $this->belongsTo('App\Models\Obat','obat_id');
    }

  public static function getJsonItem($input)
  {
    $table  = 'item_permintaan_obat';
    $select = '*';

    $replace_field  = [
      ['old_name' => 'hargaRe', 'new_name' => 'harga'],
    ];

    $param = [
      'input'         => $input->all(),
      'select'        => $select,
      'table'         => $table,
      'replace_field' => $replace_field
    ];
    $datagrid = new Datagrid;
    $data = $datagrid->datagrid_query($param, function($data){
      return $data;
    });
    return $data;
  }
}
