<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class Admin_obat extends Model
{
    protected $table = 'admin_obats';
    protected $primaryKey = 'id';

    public static function getJsonObatAdmin($input)
    {
      $table  = 'admin_obats';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'hargaRe', 'new_name' => 'harga'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data;
      });
      return $data;
    }
}
