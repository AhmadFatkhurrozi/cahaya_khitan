<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class Daftar_cabang extends Model
{
    protected $table = 'daftar_cabangs';
    protected $primaryKey = 'id_cabang';
    // public $timestamps = true;

    public function users()
    {
        return $this->hasMany('App\Models\Users');
    }

    public function permintaan_obat()
    {
        return $this->hasMany('App\Models\PermintaanObat');
    }    

    public static function getJsonDaftarcabang($input)
    {
      $table  = 'daftar_cabangs';
      $select = 'daftar_cabangs.*';

      $replace_field  = [
        // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data;
      });
      return $data;
    }
}
