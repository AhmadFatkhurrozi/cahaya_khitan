<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Pasien extends Model
{
    protected $table = 'pasiens';
    protected $primaryKey = 'id_pasien';
    // public $timestamps = true;

    public function metode()
    {
        return $this->belongsTo('App\Models\Metode_sunat','metode_id');
    }

      public function users()
    {
        return $this->belongsTo('App\Models\Users','operator_id');
    }

    public static function getJsonPasien($input)
    {
      $table  = 'pasiens';
      $select = "*,concat(tempat, ':', ' ',tempat_khitan) as alamat_khitan";

      $replace_field  = [
        // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
	       if (Auth::getUser()->level_user == 1) {
            return $data->join('metode_sunats', 'metode_sunats.id_metode', 'pasiens.metode_id')
                       ->orderBy('status_khitan', 'DESC');
           }else{
            return $data->join('metode_sunats', 'metode_sunats.id_metode', 'pasiens.metode_id')
                       ->where('cabang_id', Auth::getUser()->cabang_id)
                       ->orderBy('status_khitan', 'DESC');
           }
      });
        return $data;
    }

}
