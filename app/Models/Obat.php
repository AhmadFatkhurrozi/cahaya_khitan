<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Obat extends Model
{
    protected $table = 'master_obat';
    protected $primaryKey = 'id_obat';
    // public $timestamps = true;

    public function Item_permintaan()
    {
        return $this->hasMany('App\Models\Item_permintaan');
    }

    public static function getJsonObat($input)
    {
      $table  = 'master_obat';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'hargaRe', 'new_name' => 'harga'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->where('cabang_id', Auth::getUser()->cabang_id);
      });
      return $data;
    }
}
