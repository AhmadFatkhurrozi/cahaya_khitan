<?php

namespace App\Models;

// use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Http\Libraries\Datagrid;
use Auth;

class Users extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
      'id','username','email','password','token', 'name','gender','address','phone','photo','level_user','cabang_id','is_banned','last_login','remember_token','created_at','updated_at',
    ];
    protected $primaryKey = 'id';
    protected $hidden = [
      'password', 'remember_token',
    ];

    public function pasien()
    {
        return $this->hasMany('App\Models\Pasien');
    }

    public function daftar_cabang()
    {
        return $this->belongsTo('App\Models\Daftar_cabang','cabang_id');
    }
    
    
    public static function getJsonAdmin($input)
    {
      $table  = 'users';
      $select = 'users.*';

      $replace_field  = [
        ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->where('level_user','1');
      });
      return $data;
    }
    public static function getJsonOwner($input)
    {
      $table  = 'users as u';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'u.cabang_id')->where('level_user','2');
      });
      return $data;
    }
    public static function getJsonKepalaCabang($input)
    {
      $table  = 'users as u';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'u.cabang_id')->where('level_user','3');
      });
      return $data;
    }
    public static function getJsonOperator($input)
    {
      $table  = 'users';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        if (Auth::getUser()->level_user == 1) {
            return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'users.cabang_id')
                        ->where('level_user','4');
        }else{
            return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'users.cabang_id')
                        ->where('level_user','4')->where('cabang_id', Auth::getUser()->cabang_id);
        }
      });
      return $data;
    }
    public static function getJsonBendahara($input)
    {
      $table  = 'users';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        if (Auth::getUser()->level_user == 1) {
            return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'users.cabang_id')
                        ->where('level_user','5');
        }else{
            return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'users.cabang_id')
                        ->where('level_user','5')->where('cabang_id', Auth::getUser()->cabang_id);
        }
      });
      return $data;
    }
    public static function getJsonMarketing($input)
    {
      $table  = 'users';
      $select = '*';

      $replace_field  = [
        ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        if (Auth::getUser()->level_user == 1) {
            return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'users.cabang_id')
                        ->where('level_user','6');
        }else{
            return $data->join('daftar_cabangs as dc', 'dc.id_cabang', 'users.cabang_id')
                        ->where('level_user','6')->where('cabang_id', Auth::getUser()->cabang_id);
        }
      });
      return $data;
    }
}
