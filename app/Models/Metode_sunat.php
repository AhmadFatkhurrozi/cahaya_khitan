<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class Metode_sunat extends Model
{
    protected $table = 'metode_sunats';
    protected $primaryKey = 'id_metode';
    // public $timestamps = true;

     public function fee_operator()
    {
        return $this->hasOne('App\Models\FeeOperator');
    }

    public static function getJsonMetodesunat($input)
    {
      $table  = 'metode_sunats';
      $select = 'metode_sunats.*';

      $replace_field  = [
        ['old_name' => 'hargaRe', 'new_name' => 'harga_metode'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
        return $data;
      });
      return $data;
    }
}
