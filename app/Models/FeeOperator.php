<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class FeeOperator extends Model
{
  protected $table = 'fee_masters';
  protected $primaryKey = 'id_fee';
  // public $timestamps = true;

  public function metode()
  {
      return $this->belongsTo('App\Models\Metode_sunat','metode_id');
  }
  
  public static function getJsonFeeoperator($input)
  {
    $table  = 'fee_masters';
    $select = '*';

    $replace_field  = [
      // ['old_name' => 'hargaRe', 'new_name' => 'harga_metode'],
    ];

    $param = [
      'input'         => $input->all(),
      'select'        => $select,
      'table'         => $table,
      'replace_field' => $replace_field
    ];
    $datagrid = new Datagrid;
    $data = $datagrid->datagrid_query($param, function($data){
      return $data->where('cabang_id', Auth::getUser()->cabang_id );
    });
    return $data;
  }
}
