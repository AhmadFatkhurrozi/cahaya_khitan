<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Neraca extends Model
{
    protected $guarded = [];
    
    protected $table = 'neraca';
    protected $primaryKey = 'id_neraca';
    public $timestamps = false;

    public static function getJsonNeraca($input)
    {
      $table  = 'neraca as n';

      if (Auth::getUser()->level_user == 1) {
        $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, n.informasi, n.debit, n.kredit, u.name";
      }else{
        $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, n.informasi, n.debit, n.kredit, u.name, dc.nama_cabang";
      }

      $replace_field  = [
        // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
            if (Auth::getUser()->level_user == 1) {
                return $data->join('users as u', 'u.id', 'n.users_id')
                            ->where('n.cabang_id', 0);
            }else{
                return $data->join('users as u', 'u.id', 'n.users_id')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'n.cabang_id')
                        ->where('n.cabang_id', Auth::getUser()->cabang_id);
            }
      });
        return $data;
    }

    public static function getJsonNeracaCabang($input)
    {
        $table  = 'neraca as n';
        $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, a.nama_reff, n.informasi, n.debit, n.kredit, u.name, dc.nama_cabang";

        $replace_field  = [
            // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
        ];

          $param = [
            'input'         => $input->all(),
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
          ];

          $datagrid = new Datagrid;
          
        $data = $datagrid->datagrid_query($param, function($data){
            return $data->join('users as u', 'u.id', 'n.users_id')
                        ->join('akun as a', 'a.no_reff', 'n.kode_reff')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'n.cabang_id');
        });
        
        return $data;
    }

    public static function getJsonBulan($input, $bulan)
    {
        $table  = 'neraca as n';

        if (Auth::getUser()->level_user == 1) {
            $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, n.informasi, n.debit, n.kredit, u.name";
        }else{
            $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, n.informasi, n.debit, n.kredit, u.name, dc.nama_cabang";
        }
        
        $replace_field  = [
            // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
        ];

          $param = [
            'input'         => $input->all(),
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
          ];

          $datagrid = new Datagrid;
          
        $data = $datagrid->datagrid_query($param, function($data) use ($bulan){

            if (Auth::getUser()->level_user == 1) {
                return $data->join('users as u', 'u.id', 'n.users_id')
                            ->join('akun as a', 'a.no_reff', 'n.kode_reff')
                            ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                            ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                            ->where('n.cabang_id', 0);
            }else{
                return $data->join('users as u', 'u.id', 'n.users_id')
                        ->join('akun as a', 'a.no_reff', 'n.kode_reff')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'n.cabang_id')
                        ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                        ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                        ->where('n.cabang_id', Auth::getUser()->cabang_id);
            }
        });
        
        return $data;
    }

    public static function getJsonFilter($input, $bulan, $cabang)
    {
        $table  = 'neraca as n';
        $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, a.nama_reff, n.informasi, n.debit, n.kredit, u.name, dc.nama_cabang";

        $replace_field  = [
            // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
        ];

          $param = [
            'input'         => $input->all(),
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
          ];

          $datagrid = new Datagrid;
          
        $data = $datagrid->datagrid_query($param, function($data) use ($bulan, $cabang){
        
            return $data->join('users as u', 'u.id', 'n.users_id')
                        ->join('akun as a', 'a.no_reff', 'n.kode_reff')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'n.cabang_id')
                        ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                        ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4))
                        ->where('n.cabang_id', $cabang);
        });
        
        return $data;
    }

    public static function getJsonFilterBulan($input, $bulan)
    {
        $table  = 'neraca as n';
        $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, a.nama_reff, n.informasi, n.debit, n.kredit, u.name, dc.nama_cabang";

        $replace_field  = [
            // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
        ];

          $param = [
            'input'         => $input->all(),
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
          ];

          $datagrid = new Datagrid;
          
        $data = $datagrid->datagrid_query($param, function($data) use ($bulan){
        
            return $data->join('users as u', 'u.id', 'n.users_id')
                        ->join('akun as a', 'a.no_reff', 'n.kode_reff')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'n.cabang_id')
                        ->whereMonth('tgl_neraca', '=', substr($bulan, 0, 2))
                        ->whereYear('tgl_neraca', '=', substr($bulan, 3, 4));
        });
        
        return $data;
    }

    public static function getJsonFilterCabang($input, $cabang)
    {
        $table  = 'neraca as n';
        $select = "n.id_neraca, n.kode_reff, n.tgl_neraca, a.nama_reff, n.informasi, n.debit, n.kredit, u.name, dc.nama_cabang";

        $replace_field  = [
            // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
        ];

          $param = [
            'input'         => $input->all(),
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
          ];

          $datagrid = new Datagrid;
          
        $data = $datagrid->datagrid_query($param, function($data) use ($cabang){
        
            return $data->join('users as u', 'u.id', 'n.users_id')
                        ->join('akun as a', 'a.no_reff', 'n.kode_reff')
                        ->join('daftar_cabangs as dc', 'dc.id_cabang', 'n.cabang_id')
                        ->where('n.cabang_id', $cabang);
        });
        
        return $data;
    }
}