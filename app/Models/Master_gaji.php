<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;
use Auth;

class Master_gaji extends Model
{
  protected $table = 'master_gajis';
  protected $primaryKey = 'id_gaji';
  // public $timestamps = true;

  public static function getJsonMasterGaji($input)
  {
    $table  = 'master_gajis';
    $select = 'id_gaji,posisi, nominal_gaji, 
            IF(users_level = "1", "Admin",
            IF(users_level = "2", "Owner",
            IF(users_level = "3", "Kepala Cabang",
            IF(users_level = "4", "Operator Cabang",
            IF(users_level = "5", "Bendahara Cabang",
            IF(users_level = "6", "Marketing Cabang", "tidak ada")))))) as jabatan';

    $replace_field  = [
      ['old_name' => 'hargaRe', 'new_name' => 'nominal_gaji'],
    ];

    $param = [
      'input'         => $input->all(),
      'select'        => $select,
      'table'         => $table,
      'replace_field' => $replace_field
    ];
    $datagrid = new Datagrid;
    $data = $datagrid->datagrid_query($param, function($data){
      return $data->join('users', 'users.level_user', 'master_gajis.users_level')->distinct();
    });
    return $data;
  }

  public function getJsonGajiCabang($input)
  {
    $table  = 'master_gajis';
    $select = 'id_gaji, username, nominal_gaji';

    $replace_field  = [
      ['old_name' => 'hargaRe', 'new_name' => 'nominal_gaji'],
    ];

    $param = [
      'input'         => $input->all(),
      'select'        => $select,
      'table'         => $table,
      'replace_field' => $replace_field
    ];
    $datagrid = new Datagrid;
    $data = $datagrid->datagrid_query($param, function($data){
      return $data->join('users', 'users.level_user', 'master_gajis.users_level')->where('cabang_id', Auth::getUser()->cabang_id);
    });
    return $data;
  }
}