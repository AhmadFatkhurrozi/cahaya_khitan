<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class Biaya extends Model
{
    protected $table = 'biayas';
    protected $primaryKey = 'id_biaya';
    // public $timestamps = true;

    public function metode()
    {
        return $this->belongsTo('App\Models\Metode_sunat','metode_id');
    }

    public function pasien()
    {
        return $this->belongsTo('App\Models\Pasien','id_pasien');
    }

    public function mainan()
    {
        return $this->belongsTo('App\Models\Daftar_mainan','id_mainan');
    }

    public static function getJsonBiaya($input)
    {
      $table  = 'biayas';
      $select = "*";

      $replace_field  = [
        // ['old_name' => 'isBanned', 'new_name' => 'is_banned'],
      ];

      $param = [
        'input'         => $input->all(),
        'select'        => $select,
        'table'         => $table,
        'replace_field' => $replace_field
      ];
      $datagrid = new Datagrid;
      $data = $datagrid->datagrid_query($param, function($data){
           return $data->join('metode_sunats', 'metode_sunats.id_metode', 'biayas.metode_id')
                       ->join('pasiens', 'pasiens.id_pasien', 'biayas.pasien_id')
                       ->join('daftar_mainans', 'daftar_mainans.id_mainan', 'biayas.mainan_id');
      });
        return $data;
    }

}
