<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Http\Libraries\Datagrid;

class Admin_bhp extends Model
{
    protected $table = 'admin_bhps';
    protected $primaryKey = 'id';

    public static function getJsonBhpAdmin($input)
    {
        $table  = 'admin_bhps';
        $select = '*';

        $replace_field  = [
            ['old_name' => 'hargaSa', 'new_name' => 'harga_satuan'],
            ['old_name' => 'hargaGro', 'new_name' => 'harga_grosir'],
        ];

        $param = [
            'input'         => $input->all(),
            'select'        => $select,
            'table'         => $table,
            'replace_field' => $replace_field
        ];
        $datagrid = new Datagrid;
        $data = $datagrid->datagrid_query($param, function($data){
            return $data;
        });
        return $data;
    }
}
