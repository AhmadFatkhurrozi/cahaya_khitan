<?php

return [

    /*
     * Path to the json file containing the credentials.
     */
    'service_account_credentials_json' => storage_path('app/google-calendar/service-account-credentials.json'),

    /*
     *  The id of the Google Calendar that will be used by default.
     */
    'calendar_id' => env('930648417383-6kj6mf28drmr4tie3sm9gb9t1p7gu3km.apps.googleusercontent.com'),
];
