<div class="box box-primary other-page">
    <div class="col-md-6 col-sm-6 col-xs-12 form-inline p-5">
        <div class="form-group">
            <span><i class="fa fa-users"></i> Per Operator - {{ $longMonth }} {{ $year }}</span>
        </div> 
    </div>
    <div class="col-md-6 col-sm-6 col-xs-12 form-inline panelSearch">
    </div>
    <div class="m-l-15">
      <button type="button" class="btn btn-warning btn-sm btn-cancel pull-right" style="margin-top: 10px; margin-right: 10px;"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>
    <div class='clearfix'></div>
    <div class="col-md-12 p-0">
        <table class="table table-responsive table-inverse">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Operator</th>
                    <th>Total Fee</th>
                    <th><center>Action</center></th>
                </tr>
            </thead>
            <tbody>
                <?php  $i = 1; ?>
                @foreach($operator as $k)
                <tr>
                    <td>{{ $i++ }}</td>
                    <td>{{ $k->name }}</td>
                    <td>@currency($k->total)</td>
                    <td align="center">
                        <input type="hidden" value="{{ $k->longMonth }}" name="longMonth" id="longMonth">
                        <a href="javascript:void(0)" onclick="rincian('{{ $k->name }}', '{{ $k->month }}', '{{ $k->year }}', '{{ $k->users_id }}')" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class='clearfix'></div>
</div>

<script type="text/javascript">
    function rincian(name, month, year, users_id) {
    var longMonth = $('#longMonth').val();
    $.post("{!! route('detail_fee_op') !!}", {name:name, month:month, year:year, longMonth:longMonth, users_id:users_id }).done(function(data){
        if(data.status == 'success'){
            $('.other-modal').html(data.content);
        }
    }).fail(function() {
        konfirmasi(rowIndex);
    });
}

$('.btn-cancel').click(function(e){
    e.preventDefault();
      $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('#main-layer').fadeIn();
    });
  });
</script>