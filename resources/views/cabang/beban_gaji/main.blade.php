@extends('component.layout')

@section('extended_css')
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Beban Gaji </li>
        <li class="active">{{ $data['title'] }}</li>
    </ol>
</section>

<!-- Main Loading -->
<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary" id="main-layer">
                <div class="col-md-6 col-sm-6 col-xs-12 form-inline p-5">
                    <div class="form-group">
                        <span><i class="fa fa-calendar"></i> Beban Gaji Bulan Ini</span>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-inline panelSearch">
                   
                </div>
                <div class='clearfix'></div>
                <div class="col-md-12 p-0">
                    <table class="table table-responsive table-inverse">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Jabatan</th>
                                <th>Gaji</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  $i = 1; $jumlah=0;?>
                            @foreach($gaji as $k)
                            @php $jumlah = $jumlah+$k->nominal_gaji; @endphp
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $k->name }}</td>
                                <td>{{ $k->jabatan }}</td>
                                <td>@currency($k->nominal_gaji)</td>
                            </tr>
                            @endforeach
                            <tr>
                               <td colspan="2"></td>
                               <td align="center"><b>Total</b></td> 
                               <td><b>@currency($jumlah)</b></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class='clearfix'></div>
            </div>
            <div class="other-page"></div>
            <div class="modal-dialog"></div>
        </div>
    </div>
</section>
@stop

@section('extended_js')
  
@stop
