@extends('component.layout')

@section('extended_css')
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Pen-debet-an </li>
        <li class="active">{{ $data['title'] }}</li>
    </ol>
</section>

<!-- Main Loading -->
<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary main-layer">
                <div class="col-md-6 col-sm-6 col-xs-12 form-inline p-5">
                    <button type="button" class="btn btn-success btn-xs btn-add"><i class="fa fa-plus"> Tambah Pengeluaran</i></button>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-12 form-inline main-layer panelSearch">
                    <form action="{!! route('search_exp') !!}" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <select class="input-sm form-control input-s-sm inline v-middle option-search" name="month" id="searchMonth">
                                @foreach($bulanTahun as $i)
                                <option value="{{ $i->month }}">{{ $i->longMonth }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <select class="input-sm form-control input-s-sm inline v-middle option-search" name="year" id="searchYear">
                                @foreach($bulanTahun as $i)
                                <option value="{{ $i->year }}">{{ $i->year }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-search"></i> Cari</button>
                        </div>
                    </form>
                </div>
                <div class='clearfix'></div>
                <div class="col-md-12 p-0">
                    <table class="table table-responsive table-inverse">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Bulan & Tahun</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php  $i = 1; ?>
                            @foreach($bulanTahun as $k)
                            <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $k->longMonth }} {{ $k->year }}</td>
                                <td>
                                    <input type="hidden" value="{{ $k->longMonth }}" name="longMonth" id="longMonth">
                                    <a href="javascript:void(0)" onclick="detail('{{ $k->month }}', '{{ $k->year }}')" class="btn btn-xs btn-success"><i class="fa fa-eye"></i> Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class='clearfix'></div>
            </div>
            <div class="other-page"></div>
            <div class="modal-dialog"></div>
        </div>
    </div>
</section>
@stop

@section('extended_js')
  <script type="text/javascript">
    $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formAddPengeluaran') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    function detail(month, year) {
      var longMonth = $('#longMonth').val();
        $.post("{!! route('detail_exp') !!}", {month:month, year:year, longMonth:longMonth }).done(function(data){
          if(data.status == 'success'){
            $('.modal-dialog').html(data.content);
            }
          }).fail(function() {
            konfirmasi(rowIndex);
        });
     }
  </script>
@stop
