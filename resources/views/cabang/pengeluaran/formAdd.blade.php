<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-plus-square iconLabel m-r-15"></i> Tambah Data Pengeluaran
    </h4>
    <hr class="m-t-0">

    <form class="form-save">
        <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="card">
                    <div class="card-body">
                        {{-- <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Pengeluaran<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="pengeluaran" value="tidak tetap" class="form-control input-sm customInput col-md-7 col-xs-12" readonly="">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>

                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Reff<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <select name="jenis" class="form-control input-sm customInput col-md-7 col-xs-12">
                                    <option disabled>.:: Pilih Jenis ::.</option>
                                    <option value="mainan">Mainan</option>
                                    <option value="obat">Obat</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Reff<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="reff" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan nama barang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Qty<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="qty" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan jumlah barang">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="nominal" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="harga per item">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div> --}}

                        <div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default btn-sm active" id="choose_obat">
                                <input type="radio" name="jenisreff">Obat
                            </label>
                            <label class="btn btn-default btn-sm" id="choose_bhp">
                                <input type="radio" name="jenisreff">Alkes/ BHP
                            </label>
                            <label class="btn btn-default btn-sm" id="choose_mainan">
                                <input type="radio" name="jenisreff">Mainan
                            </label>
                            <label class="btn btn-default btn-sm" id="choose_fee">
                                <input type="radio" name="jenisreff">Fee Operator
                            </label>
                            <label class="btn btn-default btn-sm" id="choose_gaji">
                                <input type="radio" name="jenisreff">Beban Gaji
                            </label>
                        </div>
                       
                        <div id="panel-obat">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Obat<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="obat" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan nama obat">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Qty<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="qty" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="masukkan jumlah obat">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="nominal" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="harga per item">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>

                        <div id="panel-bhp" style="display: none;">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Alkes/ BHP<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="bhp" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan nama alkes/ BHP">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Qty<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="qty" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="masukkan jumlah alkes/ BHP">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="nominal" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="harga per item">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>

                        <div id="panel-mainan" style="display: none;">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Mainan<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="mainan" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan nama Mainan">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Qty<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="qty" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="masukkan jumlah mainan">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="nominal" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="harga per item">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>

                        <div id="panel-fee" style="display: none;">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Bulan<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="bulan" class="form-control input-sm customInput col-md-7 col-xs-12" value="januari 2020" readonly="">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Total Fee<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="nominal" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="total fee">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>

                        <div id="panel-gaji" style="display: none;">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Bulan<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="bulan" class="form-control input-sm customInput col-md-7 col-xs-12" value="januari 2020" readonly="">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Total Beban Gaji<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="nominal" class="form-control input-sm customInput col-md-7 col-xs-12" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="total beban gaji">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>

    </form>
</div>

<script type="text/javascript">
  
    $('#choose_obat').click(function(){
        $('#panel-obat').show();
        $('#panel-bhp').hide();
        $('#panel-mainan').hide();
        $('#panel-gaji').hide();
        $('#panel-fee').hide();
    });

    $('#choose_bhp').click(function(){
        $('#panel-obat').hide();
        $('#panel-bhp').show();
        $('#panel-mainan').hide();
        $('#panel-gaji').hide();
        $('#panel-fee').hide();
    });

    $('#choose_mainan').click(function(){
        $('#panel-obat').hide();
        $('#panel-bhp').hide();
        $('#panel-mainan').show();
        $('#panel-gaji').hide();
        $('#panel-fee').hide();
    });

    $('#choose_gaji').click(function(){
        $('#panel-obat').hide();
        $('#panel-bhp').hide();
        $('#panel-mainan').hide();
        $('#panel-gaji').show();
        $('#panel-fee').hide();
    });

    $('#choose_fee').click(function(){
        $('#panel-obat').hide();
        $('#panel-bhp').hide();
        $('#panel-mainan').hide();
        $('#panel-gaji').hide();
        $('#panel-fee').show();
    });

    var onLoad = (function() {
            $('#panel-add').animateCss('bounceInUp');
        })();

        $('.btn-cancel').click(function(e){
            e.preventDefault();
            $('#panel-add').animateCss('bounceOutDown');
            $('.other-page').fadeOut(function(){
              $('.other-page').empty();
              $('.main-layer').fadeIn();
          });
        });

        $('.btn-submit').click(function(e){
            e.preventDefault();
            $('.btn-submit').html('Please wait...').attr('disabled', true);
            var data  = new FormData($('.form-save')[0]);
            $.ajax({
              url: "{{ route('addPengeluaran') }}",
              type: 'POST',
              data: data,
              async: true,
              cache: false,
              contentType: false,
              processData: false
          }).done(function(data){
              $('.form-save').validate(data, 'has-error');
              if(data.status == 'success'){
                swal("Success !", data.message, "success");
                $('.other-page').fadeOut(function(){
                  $('.other-page').empty();
                  $('.main-layer').fadeIn();
                  location.reload();
              });
            } else if(data.status == 'error') {
                $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
                swal('Whoops !', data.message, 'warning');
            } else {
                var n = 0;
                for(key in data){
                  if (n == 0) {var dt0 = key;}
                  n++;
              }
              $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
              swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
          }
      }).fail(function() {
          swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
          $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
      });
    });

</script>