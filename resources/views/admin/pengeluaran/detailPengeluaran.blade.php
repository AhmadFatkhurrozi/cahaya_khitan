<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
    <div class="modal-dialog modal-lg" >
        <div class="modal-content">
            <div class="modal-header" style="width: 100%">
                Detail
                <span style="float:right"><a data-dismiss="modal" onclick="closeModal()">Close</a></span>
            </div>
            <div class="modal-body">
                <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
                    <h4 style="font-weight: bold; color: #4682B4;">Pengeluaran Bulan {{ $bulan }} {{ $tahun }}</h4>
                    <hr style="  border: 1px solid DimGray;">
                    <div class="form-group m-t-0 m-b-25">
                        <table class="table table-responsive table-inverse">
                            <thead>
                                <tr>
                                    <th colspan="6"><u>Obat & BHP</u></th>
                                </tr>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal</th>
                                    <th>Jenis</th>
                                    <th>Nama Reff</th>
                                    <th>Qty</th>
                                    <th>Harga</th>
                                    <th>SubTotal</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  $i = 1; $mn=0; ?>
                                @foreach($data as $k)
                                <?php $jumlah = $k->qty*$k->nominal; 
                                $mn = $mn+$jumlah; ?>
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $k->created_at->format('d') }}</td>
                                    <td>{{ $k->jenis }}</td>
                                    <td>{{ $k->nama_reff }}</td>
                                    <td>{{ $k->qty }}</td>
                                    <td>@currency($k->nominal)</td>
                                    <td>@currency($jumlah)</td>
                                </tr>
                                @endforeach
                                <tr>
                                    <td colspan="5"></td>
                                    <td><strong>Total</strong></td>
                                    <td><strong>@currency($mn)</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="clearfix" style='padding-bottom:20px'></div>
        </div>
    </div>
</div>
<script type="text/javascript">

    var onLoad = (function() {
        $('#detail-dialog').find('.modal-dialog').css({
            'width'     : '65%'
        });
        $('#detail-dialog').modal('show');
    })();

    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    });

</script>
