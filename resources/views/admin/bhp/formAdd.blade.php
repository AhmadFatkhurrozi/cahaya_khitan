<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Data Bahan Habis Pakai
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        @if ($data != "")
        <input type="hidden" name="id" value="{{ $data->id }}">
        @endif
        <div class="box-body">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Barang<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="nama_barang" value="@if($data != ''){{ $data->nama_barang }}@endif" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan nama barang">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Stok<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="stok" value="@if($data != ''){{ $data->stok_bhp }}@endif" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan stok">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 p-0">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga Satuan<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="harga_satuan" id="harga" min="0" value="@if($data != ''){{ $data->harga_satuan }}@endif" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan harga satuan">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga Grosir<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="harga_grosir" id="harga_grosir" min="0" value="@if($data != ''){{ $data->harga_grosir }}@endif" class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan harga grosir">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">
    function submitBday() {
        var Q4A = "";
        var Bdate = document.getElementById('tgl_lahir').value;
        var Bday = +new Date(Bdate);
        Q4A += ~~ ((Date.now() - Bday) / (31557600000))+" tahun";
        var theBday = document.getElementById('resultBday');
        theBday.value = Q4A;
    }

    var onLoad = (function() {
        $('#panel-add').animateCss('bounceInUp');
    })();

    $(function() {
        $('#harga').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });
    $(function() {
        $('#harga_grosir').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('#panel-add').animateCss('bounceOutDown');
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });

    $('.btn-submit').click(function(e){
        e.preventDefault();
        $('.btn-submit').html('Please wait...').attr('disabled', true);
        var data  = new FormData($('.form-save')[0]);
        $.ajax({
            url: "{{ route('addBhpAdmin') }}",
            type: 'POST',
            data: data,
            async: true,
            cache: false,
            contentType: false,
            processData: false
        }).done(function(data){
            $('.form-save').validate(data, 'has-error');
            if(data.status == 'success'){
                swal("Success !", data.message, "success");
                $('.other-page').fadeOut(function(){
                    $('.other-page').empty();
                    $('.main-layer').fadeIn();
                    datagrid.reload();
                    // location.replace("{{ url('admin/pengeluaran') }}");
                });
            } else if(data.status == 'error') {
                $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
                swal('Whoops !', data.message, 'warning');
            } else {
                var n = 0;
                for(key in data){
                    if (n == 0) {var dt0 = key;}
                    n++;
                }
                $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
                swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
            }
        }).fail(function() {
            swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
            $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        });
    });
</script>
