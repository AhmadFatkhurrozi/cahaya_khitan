@extends('component.layout')

@section('extended_css')
@stop

@section('content')
<section class="content-header">
    <h1>
        {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Data Bahan Habis Pakai</li>
        <li class="active">{{ $data['title'] }}</li>
    </ol>
</section>

<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary main-layer">

                <div class="col-md-4 col-sm-4 col-xs-12 form-inline main-layer p-5">
                    @if(Auth::getUser()->level_user == 1)
                    <button type="button" class="btn btn-sm btn-primary btn-add">
                        <span class="fa fa-plus"></span> &nbsp Tambah BHP *(Bahan Habis Pakai)
                    </button>
                    @endif
                </div>
                <!-- Search -->
                <div class="col-md-8 col-sm-8 col-xs-12 form-inline main-layer panelSearch">
                    <div class="form-group">
                        <select class="input-sm form-control input-s-sm inline v-middle option-search" id="search-option"></select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="input-sm form-control" placeholder="Search" id="search">
                    </div>
                </div>

                <div class='clearfix'></div>

                <div class="col-md-12 p-0">
                    <!-- Datagrid -->
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light" id="datagrid"></table>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-1 hidden-xs">
                                <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                            </div>
                            <div class="col-sm-6 text-center">
                                <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                            </div>
                            <div class="col-sm-5 text-right text-center-xs">
                                <ul class="pagination pagination-sm m-t-none m-b-none" id="paging"></ul>
                            </div>
                        </div>
                    </footer>
                </div>

                <div class='clearfix'></div>
            </div>
            
            <div class="other-page"></div>
            <div class="modal-dialog"></div>
        </div>
    </div>
</section>
@stop

@section('extended_js')
  <script type="text/javascript">
    var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('getJsonBhpAdmin') !!}",
      primaryField          : 'id',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      columns               : [
        {field: 'nama_barang', title: 'Nama Barang', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'stok_bhp', title: 'Stok Barang', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'hargaSa', title: 'Harga Satuan', editable: false, sortable: true, width: 150, align: 'left', search: true,
          rowStyler: function(rowData, rowIndex) {
            return hargaSa(rowData, rowIndex);
          }
        },
        {field: 'hargaGro', title: 'Harga Grosir', editable: false, sortable: true, width: 100, align: 'left', search: true,
          rowStyler: function(rowData, rowIndex) {
            return hargaGro(rowData, rowIndex);
          }
        },
        {field: 'menu', title: 'Menu', sortable: false, width: 50, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return menu(rowData, rowIndex);
          }
        }
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

    $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formAddBhpAdmin') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    function menu(rowData, rowIndex) {
      var menu =
        '<div class="btn-group" style="box-shadow:none">' +
        '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-bars"></span></a>' +
        '<ul class="dropdown-menu pull-right">' +
        '<li onclick="detail('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-eye"></i> Detail</a></li>' +
        '<li onclick="updated('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Perbaharui</a></li>' +
        '<li onclick="deleted('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-trash-o"></i> Hapus</a></li>' +
        '</ul>' +
        '</div>';
      return menu;
    }

    function updated(rowIndex){
  		var rowData = datagrid.getRowData(rowIndex);
  		$('.loading').show();
  		$('.main-layer').hide();
  		$.post("{!! route('formAddBhpAdmin') !!}",{id:rowData.id}).done(function(data){
  			if(data.status == 'success'){
  				$('.loading').hide();
  				$('.other-page').html(data.content).fadeIn();
  			} else {
  				$('.main-layer').show();
  			}
  		});
  	}

    function detail(rowIndex){
  		var rowData = datagrid.getRowData(rowIndex);
  		$('.loading').show();
  		$('.main-layer').hide();
  		$.post("{!! route('showBhpAdmin') !!}",{id:rowData.id}).done(function(data){
  			if(data.status == 'success'){
  				$('.loading').hide();
  				$('.other-page').html(data.content).fadeIn();
  			} else {
  				$('.main-layer').show();
  			}
  		});
  	}

    function deleted(rowIndex){
		var rowData = datagrid.getRowData(rowIndex);
		swal(
			{
				title: "Apa anda yakin menghapus Data ini?",
				text: "Data akan dihapus dari sistem dan tidak dapat dikembalikan!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Saya yakin!",
				cancelButtonText: "Batal!",
				closeOnConfirm: true
			},
			function(){
				$.post("{!! route('deleteBhpAdmin') !!}", {id:rowData.id}).done(function(data){
					if(data.status == 'success'){
						datagrid.reload();
						$('.main-layer').show();
						$('.loading').hide();
						swal(data.title, data.message, data.type);
					}else if(data.status == 'error'){
						datagrid.reload();
						swal(data.title, data.message, data.type);
					}
				});
			}
		);
	}

  function hargaGro(rowData, rowIndex) {

      var harga = datagrid.getRowData(rowIndex).harga_grosir;
      var tag = '';
      if (harga == '' || harga == null) {
        tag = '-';
      }else{
        var bilangan = rowData.harga_grosir;
        var reverse = bilangan.toString().split('').reverse().join(''),
        ribuan 	= reverse.match(/\d{1,3}/g);
        ribuan	= ribuan.join('.').split('').reverse().join('');
        tag = 'Rp. '+ribuan;
      }
      return tag;
    }

    function hargaSa(rowData, rowIndex) {
      var bilangan = rowData.harga_satuan;
			var reverse = bilangan.toString().split('').reverse().join(''),
			ribuan 	= reverse.match(/\d{1,3}/g);
			ribuan	= ribuan.join('.').split('').reverse().join('');
			return 'Rp. '+ribuan
    }
  </script>
@stop
