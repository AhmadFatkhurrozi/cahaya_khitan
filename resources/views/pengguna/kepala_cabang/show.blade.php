<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-eye iconLabel m-r-15"></i> Data Kepala Cabang
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="m-l-15" style="margin-right: 20px;">
            <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
        <br><br>
        <div class="box-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Email<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="email" id='email' class="form-control input-sm customInput col-md-7 col-xs-12" value="{{ $data->email }}" readonly>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Username<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="username" id='username' class="form-control input-sm customInput col-md-7 col-xs-12" value="{{ $data->username }}" readonly>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="name"class="form-control input-sm customInput col-md-7 col-xs-12" value="{{ $data->name }}" readonly>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Kelamin<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="name"class="form-control input-sm customInput col-md-7 col-xs-12" value="{{ $data->gender }}" readonly>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <textarea name="address" class="form-control input-sm customInput col-md-7 col-xs-12" rows="2" readonly>{{ $data->address }}</textarea>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>No. Telepon<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="phone" class="form-control input-sm customInput col-md-7 col-xs-12" value="{{ $data->phone }}" readonly>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>

                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Klinik </label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="cabang" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" value="{{ $data->nama_cabang }}" readonly="">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="crop-edit">
                            <center>
                                @if ($data->photo == '')
                                <img src="{!! url('dist/img/no-images.png') !!}" class="img-polaroid m-r-4" height="200">
                                @endif
                                @if ($data->photo != '')
                                <img src="{{url('/')}}/{{ $data->photo }}" class="img-polaroid m-r-4" height="200">
                                @endif
                            </center>
                        </div>
                    </div>
                    <div class='clearfix p-b-5'></div>
                </div>
            </div>
        </form>
    </div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
    disabledBtn();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };

  function disabledBtn() {
    var stMail = $('#statusMail').val();
    var stUsername = $('#statusUsername').val();
    if (stMail == 'Ready' && stUsername == 'Ready') {
      $('.btn-submit').removeAttr('disabled');
    }else{
      $('.btn-submit').attr('disabled', true);
    }
  }

  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

</script>
