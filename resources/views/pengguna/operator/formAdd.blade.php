<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Tambah Data
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="box-body">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Email<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="email" id='email' onkeyup="cekMail()" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan email">
                        <div class='iconStatus text-green' id='icon_email'></div>
                        <p class='messageError errorMail'></p>
                        <input type='hidden' name='statusMail' value='Exist' id='statusMail' class='form-control'>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Username<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="username" id='username' onkeyup="cekUsername()" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan username">
                        <div class='iconStatus text-green' id='icon_username'></div>
                        <p class='messageError errorUsername'></p>
                        <input type='hidden' name='statusUsername' value='Exist' id='statusUsername' class='form-control'>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="name" required="required" autocomplete='off' placeholder="masukkan nama lengkap" class="form-control input-sm customInput col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Kelamin<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                            <label><input type="radio" name="gender" value="Laki - Laki" class="flat-red" id="genderL"> <span class="p-l-5">Laki - Laki</span></label>
                        </div>
                        <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                            <label><input type="radio" name="gender" value="Perempuan" class="flat-red" id="genderP"> <span class="p-l-5">Perempuan</span></label>
                        </div>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <textarea name="address" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2" placeholder="masukkan alamat"></textarea>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>No. Telepon<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="phone" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan no. telepon">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>

                @if(Auth::getUser()->level_user == 1)
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Pilih Klinik<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <select class="form-control select2" style="width: 100%;" name="cabang_id" id="pilihan">
                            <option value="" disabled selected>-- Please select --</option>
                            @foreach ($cabang as $key)
                            <option value="{{ $key->id_cabang }}">{{ $key->nama_cabang }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                @endif
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="crop-edit">
                            <center>
                                <img id="preview-photo" src="{!! url('dist/img/avatar/default.png') !!}" class="img-polaroid" height="250">
                            </center>
                        </div>
                    </div>
                    <div class='clearfix p-b-5'></div>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="file" class="upload" onchange="loadFilePhoto(event)" name="photo" accept="image/*" class="form-control customInput input-sm col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
    disabledBtn();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };

  function disabledBtn() {
    var stMail = $('#statusMail').val();
    var stUsername = $('#statusUsername').val();
    if (stMail == 'Ready' && stUsername == 'Ready') {
      $('.btn-submit').removeAttr('disabled');
    }else{
      $('.btn-submit').attr('disabled', true);
    }
  }

  function cekMail() {
    var email = $('#email').val();
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (email != '') {
      if (atpos < 1 || dotpos < atpos+2 || dotpos+2 >= email.length) {
        $('#grMail').attr('class','form-group has-error');
        $('#icon_email').attr('class','iconStatus text-red');
        $('#icon_email').html('<i class="fa fa-warning"></i>');
        $('.errorMail').html('Email Tidak Sesuai');
        $('#statusMail').val('Exist');
        disabledBtn();
      }else{
        $.post("{!! route('getEmail') !!}", {email:email}).done(function(data){
          if (data.status == 'success') {
            $('#grMail').attr('class','form-group has-error');
            $('#icon_email').attr('class','iconStatus text-red');
            $('#icon_email').html('<i class="fa fa-warning"></i>');
            $('.errorMail').html('Email Telah Terdaftar');
            $('#statusMail').val('Exist');
            disabledBtn();
          }else{
            $('#grMail').attr('class','form-group');
            $('#icon_email').attr('class','iconStatus text-green');
            $('#icon_email').html('<i class="fa fa-check-circle"></i>');
            $('.errorMail').html('');
            $('#statusMail').val('Ready');
            disabledBtn();
          }
        });
      }
    }else{
      $('#grMail').attr('class','form-group');
      $('#icon_email').attr('class','iconStatus');
      $('#icon_email').html('');
      $('.errorMail').html('');
      $('#statusMail').val('Exist');
      disabledBtn();
    }
  }

  function cekUsername() {
    var username = $('#username').val();
    if (username != '') {
      $.post("{!! route('getUsername') !!}", {username:username}).done(function(data){
        if (data.status == 'success') {
          $('#grUsername').attr('class','form-group has-error');
          $('#icon_username').attr('class','iconStatus text-red');
          $('#icon_username').html('<i class="fa fa-warning"></i>');
          $('.errorUsername').html('Username Telah Terdaftar');
          $('#statusUsername').val('Exist');
          disabledBtn();
        }else{
          $('#grUsername').attr('class','form-group');
          $('#icon_username').attr('class','iconStatus text-green');
          $('#icon_username').html('<i class="fa fa-check-circle"></i>');
          $('.errorUsername').html('');
          $('#statusUsername').val('Ready');
          disabledBtn();
        }
      });
    }else{
      $('#grUsername').attr('class','form-group');
      $('#icon_username').attr('class','iconStatus');
      $('#icon_username').html('');
      $('.errorUsername').html('');
      $('#statusUsername').val('Exist');
      disabledBtn();
    }
  }

  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addOperator') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>
