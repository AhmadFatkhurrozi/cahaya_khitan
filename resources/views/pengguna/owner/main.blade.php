@extends('component.layout')

@section('extended_css')
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Pengguna</li>
        <li class="active">{{ $data['title'] }}</li>
    </ol>
</section>

<!-- Main Loading -->
<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary main-layer">
                <div class="col-md-4 col-sm-4 col-xs-12 form-inline main-layer p-5">
                    <button type="button" class="btn btn-sm btn-primary btn-add">
                        <span class="fa fa-plus"></span> &nbsp Tambah Pengguna
                    </button>
                </div>
                <!-- Search -->
                <div class="col-md-8 col-sm-8 col-xs-12 form-inline main-layer panelSearch">
                    <div class="form-group">
                        <select class="input-sm form-control input-s-sm inline v-middle option-search" id="search-option"></select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="input-sm form-control" placeholder="Search" id="search">
                    </div>
                </div>
                <div class='clearfix'></div>
                <div class="col-md-12 p-0">
                    <!-- Datagrid -->
                    <div class="table-responsive">
                        <table class="table table-striped b-t b-light" id="datagrid"></table>
                    </div>
                    <footer class="panel-footer">
                        <div class="row">
                            <!-- Page Option -->
                            <div class="col-sm-1 hidden-xs">
                                <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                            </div>
                            <!-- Page Info -->
                            <div class="col-sm-6 text-center">
                                <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                            </div>
                            <!-- Paging -->
                            <div class="col-sm-5 text-right text-center-xs">
                                <ul class="pagination pagination-sm m-t-none m-b-none" id="paging"></ul>
                            </div>
                        </div>
                    </footer>
                </div>
                <div class='clearfix'></div>
            </div>
            <div class="other-page"></div>
            <div class="modal-dialog"></div>
        </div>
    </div>
</section>
@stop

@section('extended_js')
  <script type="text/javascript">
    var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('datagridOwner') !!}",
      primaryField          : 'id',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      columns               : [
        {field: 'username', title: 'Username', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'email', title: 'Email', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'name', title: 'Nama', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'gender', title: 'Jenis Kelamin', editable: false, sortable: true, width: 150, align: 'left', search: true,
          custom_search: {appendClass: 'input-sm form-control',
            option: [
              {text: 'Laki - Laki', value: 'Laki - Laki'},
              {text: 'Perempuan', value: 'Perempuan'},
            ]
          }
        },
        {field: 'phone', title: 'Telepon', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'nama_cabang', title: 'Klinik', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'isBanned', title: 'Status', editable: false, sortable: true, width: 100, align: 'center', search: true,
          custom_search: {appendClass: 'input-sm form-control',
            option: [
              {text: 'Aktif', value: '0'},
              {text: 'Banned', value: '1'},
            ]
          },
          rowStyler: function(rowData, rowIndex) {
            return isBanned(rowData, rowIndex);
          }
        },
        {field: 'menu', title: 'Menu', sortable: false, width: 50, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return menu(rowData, rowIndex);
          }
        }
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

    $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formAddOwner') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    function isBanned(rowData, rowIndex) {
      var tag;
      if (rowData.is_banned == '0') {
        tag = '<span class="label bg-green"><i class="fa fa-unlock"></i> Aktif</span>';
      }else{
        tag = '<span class="label label-danger"><i class="fa fa-lock"></i> Non-Aktif</span>';
      }
      return tag;
    }

    function menu(rowData, rowIndex) {
      var level_user = {{ Auth::getUser()->level_user }};
      if(level_user == 1){
        var menu =
          '<div class="btn-group">' +
          '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bars"></i></a>' +
          '<ul class="dropdown-menu pull-right">' +
          '<li onclick="detail('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-eye"></i> Detail</a></li>';
        if (rowData.is_banned == '0') {
          menu += '<li onclick="nonActive('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-lock"></i> Nonaktifkan Akun</a></li>';
        }else{
          menu += '<li onclick="active('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-unlock"></i> Aktifkan Akun</a></li>';
        };
        menu += '<li onclick="updated('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-pencil"></i> Perbaharui</a></li>'+
          '<li onclick="deleted('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-trash-o"></i> Hapus</a></li>' +
          '</ul>' +
          '</div>';
      }else{
        var menu = '<a onclick="detail('+rowIndex+')" href="javascript:void(0);" class="btn btn-xs btn-primary"><i class="fa fa-eye"></i> Detail</a>';
      }
      return menu;
    }

    function detail(rowIndex){
      var rowData = datagrid.getRowData(rowIndex);
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('showOwner') !!}",{id:rowData.id}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    }

    function detail(rowIndex){
      var rowData = datagrid.getRowData(rowIndex);
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('showOwner') !!}",{id:rowData.id}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    }

    function updated(rowIndex){
  		var rowData = datagrid.getRowData(rowIndex);
  		$('.loading').show();
  		$('.main-layer').hide();
  		$.post("{!! route('updateOwner') !!}",{id:rowData.id}).done(function(data){
  			if(data.status == 'success'){
  				$('.loading').hide();
  				$('.other-page').html(data.content).fadeIn();
  			} else {
  				$('.main-layer').show();
  			}
  		});
  	}

    function deleted(rowIndex){
		var rowData = datagrid.getRowData(rowIndex);
		swal(
			{
				title: "Apa anda yakin menghapus Data ini?",
				text: "Data akan dihapus dari sistem dan tidak dapat dikembalikan!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Saya yakin!",

				cancelButtonText: "Batal!",
				closeOnConfirm: true
			},
			function(){
				$.post("{!! route('deleteOwner') !!}", {id:rowData.id}).done(function(data){
					if(data.status == 'success'){
						datagrid.reload();
						$('.main-layer').show();
						$('.loading').hide();
						swal(data.title, data.message, data.type);
					}else if(data.status == 'error'){
						datagrid.reload();
						swal(data.title, data.message, data.type);
					}
				});
			}
		);
	}
  function nonActive(rowIndex) {
    var rowData = datagrid.getRowData(rowIndex);
    swal(
      {
        title: "Apa anda yakin menonaktifkan akun Owner ini?",
        text: "Akun Owner akan di Nonaktifkan dari sistem dan Akun tidak dapat Masuk ke dalam Sistem!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: false
      },
      function(){
        $.post("{!! route('nonActiveOwner') !!}", {id:rowData.id}).done(function(data){
          if(data.status == 'success'){
            datagrid.reload();
            swal("Berhasil !", "Akun Owner Berhasil di Nonaktifkan", "success");
          }
        });
      }
    );
  }
  function active(rowIndex) {
    var rowData = datagrid.getRowData(rowIndex);
    swal(
      {
        title: "Apa anda yakin meng-Aktifkan Akun Owner Ini?",
        text: "Akun Owner akan di Aktifkan dari sistem dan Akun dapat Masuk ke dalam Siste!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: false
      },
      function(){
        $.post("{!! route('activeOwner') !!}", {id:rowData.id}).done(function(data){
          if(data.status == 'success'){
            datagrid.reload();
            swal("Berhasil!", "Akun Owner Berhasil di Aktifkan", "success");
          }
        });
      }
    );
  }
  </script>
@stop
