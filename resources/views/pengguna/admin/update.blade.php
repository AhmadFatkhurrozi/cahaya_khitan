<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Tambah Data
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="box-body">
            <input type="hidden" class="form-control" name="id" value="{{$users->id}}">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group" id='grMail'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Email<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="email" id='email' onkeyup="cekMail()" value="{{ $users->email }}" readonly="" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12">

                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group" id='grUsername'>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Username<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="username" id='username' onkeyup="cekUsername()" value="{{ $users->username }}" readonly="" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="name" required="required" value="{{$users->name}}" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Kelamin<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                            <label><input type="radio" name="gender" value="Laki - Laki" class="flat-red" id="genderL" @if($users->gender == 'Laki - Laki') checked @endif> <span class="p-l-5">Laki - Laki</span></label>
                        </div>
                        <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                            <label><input type="radio" name="gender" value="Perempuan" class="flat-red" id="genderP" @if($users->gender == 'Perempuan') checked  @endif> <span class="p-l-5">Perempuan</span></label>
                        </div>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <textarea name="address" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">{{$users->address}}</textarea>
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
                <div class="form-group">
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>No. Telepon<span class="text-red">*</span></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="text" name="phone" required="required" value="{{$users->phone}}" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                <div class="form-group">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
                        <div class="crop-edit">
                            <center>
                                @if ($users->photo == '')
                                <img src="{!! url('dist/img/no-images.png') !!}" class="img-polaroid m-r-4" height="200px" width="150px">
                                @endif
                                @if ($users->photo != '')
                                <img src="{{url('/')}}/{{ $users->photo }}" class="img-polaroid m-r-4" height="200px" width="150px">
                                @endif
                            </center>
                        </div>
                    </div>
                    <div class='clearfix p-b-5'></div>
                    <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'></label>
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                        <input type="file" class="upload" onchange="loadFilePhoto(event)" name="photo" accept="image/*" class="form-control customInput input-sm col-md-7 col-xs-12">
                    </div>
                </div>
                <div class='clearfix p-b-5'></div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
    </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
    // disabledBtn();
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };

  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('doupdateAdmin') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>
