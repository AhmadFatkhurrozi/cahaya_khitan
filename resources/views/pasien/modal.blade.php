<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          Konfirmasi Pasien Selesai / Batal Khitan
        <span style="float:right"><a data-dismiss="modal" onclick="closeModal()">Close</a></span>
      </div>
      <div class="modal-body">
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
          <h4 style="font-weight: bold; color: #4682B4;">Keterangan Lengkap</h4>
          <hr style="  border: 1px solid DimGray;">
            <div class="form-group m-t-0 m-b-25">
              <input type="hidden" name="metode_id" id="metode_id" value="{{ $pasien->metode_id }}">
              <input type="hidden" name="harga_metode" id="harga_metode" value="{{ $pasien->metode->harga_metode }}">
              <input type="hidden" name="nama_metode" id="nama_metode" value="{{ $pasien->metode->nama_metode }}">
              <input type="hidden" name="nama_pasien" id="nama_pasien" value="{{ $pasien->nama }}">
              <table class="table">
                <tbody>
                  <tr>
                    <th>Nama Lengkap</th>
                      <td>: {{$pasien->nama}}</td>
                  </tr>
                  <tr>
                    <th>Usia</th>
                      <td>: {{$pasien->usia}}</td>
                  </tr>
                  <tr>
                    <th>BB</th>
                      <td>: {{$pasien->bb}}</td>
                  </tr>
                  <tr>
                    <th>Metode Khitan</th>
                      <td>: {{$pasien->metode->nama_metode}}</td>
                  </tr>
                  <tr>
                    <th>Tempat Khitan</th>
                      <td>: {{ $pasien->tempat}} {{$pasien->tempat_khitan}}</td>
                  </tr>
                  <tr>
                    <th>Tanggal</th>
                      <td>: {{$pasien->tanggal}}</td>
                  </tr>
                  <tr>
                    <th>Jam Khitan</th>
                      <td>: {{$pasien->jam}}</td>
                  </tr>
                  @if ($pasien->status_khitan == "menunggu")
                  <tr>
                    <th rowspan="2">Pilih Mainan</th>
                      <td>
                        <input type="radio" name="id_mainan" value="ya" id="pilihmainan" class="flat-red custom-control-input mainanYa"><span class="p-l-5">Ya</span>
                        <input type="radio" name="id_mainan" value="tidak" id="pilihmainan" class="flat-red custom-control-input mainanTidak"><span class="p-l-2">Tidak</span>
                      </td>
                  </tr>

                  <tr>
                    <td colspan="2">
                      <span class="panelMainan" style="display: none;">
                         <select class="form-control select2 panelMainan" style="display: none; width: 100%;" name="mainan_id" id="mainan_id">
                          <option disabled style="font-weight: bold;">Plih Mainan :</option>
                          @foreach ($mainan as $key)
                             <option value="{{ $key->id_mainan }}">{{ $key->nama_mainan }}</option>
                          @endforeach
                        </select>
                      </span>
                    </td>
                  </tr>

                  <tr>
                      <th>Pilih Obat</th>
                      <td colspan="2">
                        @foreach($obat as $k)
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="obat_id{{$k->id_obat}}" name="obat" value="{{ $k->id_obat }}"> 
                          <label class="custom-control-label">{{ $k->nama_obat }}</label>
                        </div>
                        @endforeach
                      </td>
                  </tr>
                  <tr>
                      <th>Pilih Alkes/ BHP</th>
                      <td colspan="2">
                        @foreach($bhp as $a)
                        <div class="custom-control custom-checkbox">
                          <input type="checkbox" class="custom-control-input" id="bhp_id{{$a->id_bhp}}" name="bhp" value="{{ $a->id_bhp }}"> 
                          <label class="custom-control-label">{{ $a->nama_barang }}</label>
                        </div>
                        @endforeach
                      </td>
                  </tr>
                  @endif
                  <tr>
                      <th>
                        @if ($pasien->status_khitan == 'menunggu')
                          <a href="javascript:void(0)" onclick="approve('{{ $pasien->id_pasien }}','khitan')" class="btn btn-success btn-approve btn-xs" id="btn-approve">
                              <i class="fa fa-check"><span> Khitan</span></i>
                          </a>
                          <a href="javascript:void(0)" onclick="approve('{{ $pasien->id_pasien }}','batal')" class="btn btn-danger btn-xs">
                              <i class="fa fa-times"><span> Batal Khitan</span></i>
                          </a>
                        @endif

                        @if ($pasien->status_khitan == 'khitan')
                          <button class="btn btn-success btn-xs" id="btn-approve">
                              <i class="fa fa-check"></i>Pasien selesai Khitan
                          </button>
                          <a href="javascript:void(0)"
                          onclick="window.location='{{ url('pasien/rincian_pasien/'.$biaya->pasien_id) }}'" class="btn btn-danger btn-xs">
                              <i class="fa fa-print"><span> Cetak Struk</span></i>
                          </a>
                        @endif

                          @if ($pasien->status_khitan == 'batal')
                          <button" class="btn btn-danger btn-xs" id="btn-approve">
                              <i class="fa fa-check"></i>Pasien Batal Khitan
                          </button>
                        @endif
                      </th>
                  </tr>
                </tbody>
              </table>
            </div>
        </div>
      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $('.mainanYa').click(function(){
    $('.panelMainan').show();
      });

    $('.mainanTidak').click(function(){
    $('.panelMainan').hide();
      });


  $().ready(function() {
    // validate the comment form when it is submitted
    $("#commentForm").validate();
  });
    var onLoad = (function() {
        $('#detail-dialog').find('.modal-dialog').css({
            'width'     : '65%'
        });
        $('#detail-dialog').modal('show');
    })();

    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    });

    function approve(id_pasien,status_khitan) {
      // swal(id_pasien+' '+status_khitan);
      if (status_khitan == 'khitan') {
        var ket ='Pasien selesai Khitan?';
      }else{
        var ket ='Batal Khitan?';
      }
      var metode_id = $('#metode_id').val();
      var mainan_id = $('#mainan_id').val();
      var harga_metode = $('#harga_metode').val();
      var nama_metode  = $('#nama_metode').val();
      var nama_pasien  = $('#nama_pasien').val();
      var obat_id = [];
      @foreach($obat as $k)
        var obat   = $('#obat_id{{$k->id_obat}}:checked').val();
        obat_id.push(obat);
      @endforeach

      var bhp_id = [];
      @foreach($bhp as $a)
        var bhp   = $('#bhp_id{{$a->id_bhp}}:checked').val();
        bhp_id.push(bhp);
      @endforeach
      
      var mainan = $('#pilihmainan:checked').val();
      swal({
       title:"Apakah anda akan "+ket,
       text:"Apakah anda yakin ?",
       type:"warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "Saya yakin!",
       cancelButtonText: "Batal!",
       closeOnConfirm: true
     },
     function(){
       $.post('{{ route('approve_khitan') }}',{id_pasien:id_pasien,status_khitan:status_khitan, metode_id:metode_id, mainan_id:mainan_id, mainan:mainan, obat_id:obat_id, bhp_id:bhp_id, harga_metode:harga_metode, nama_metode:nama_metode, nama_pasien:nama_pasien}).done(function(data){
         if(data.status == 'success'){
          if (data.status_khitan == 'khitan') {
            var hsl = '<a href="javascript:void(0)" class="btn btn-info"><i class="fa fa-check"><span> Pasien Sudah Di Khitan</span></i></a>';
          }else{
            var hsl = '<a href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-times"><span> Pasien Batal Khitan</span></i></a>';
          }
          $('.panel-btn-waiting'+id_pasien).html(hsl);
          $('#detail-dialog').modal('hide');
          // $('#detail-dialog').html('');
          $('.second-modal').html(data.content);

          // filePendukung(data.id_pasien);
           swal("Success!", "Data Diterima !", "success");
           if (status_khitan == 'khitan') {
                location.replace("{{ url('pasien/rincian_pasien/') }}"+'/'+data.id_pasien); 
           }else{
                swal("Success!", "Khitan Dibatalkan !", "success");
                datagrid.reload();
           }
    
         }else if(data.status=='fail'){
           swal("Maaf!", "Anda bukan pemilik berita ini !", "error");
           datagrid.reload();
         }else{
           swal("Maaf!", "Berita telah dihapus sebelum ini !", "error");
           datagrid.reload();
         }
       });
     });
    }
</script>
