<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
		<i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Data Pasien
	</h4>
  <hr class="m-t-0">

  <form class="form-save">
      <input type="hidden" name="id_pasien" value="{{ $pasien->id_pasien }}">
    <div class="box-body">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Pasien<span class="text-red">*</span></label>
      					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      						<input type="text" name="nama" value="@if($pasien != '') {{ $pasien->nama }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12">
      					</div>
      				</div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Lahir<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="tgl_lahir" id="tgl_lahir" onchange="submitBday()" value="@if($pasien != '') {{ $pasien->tgl_lahir }} @endif" class="form-control day" data-date-format="dd-mm-yyyy" placeholder="dd/mm/yyyy">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Usia<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="usia" id="resultBday" readonly="" value="@if($pasien != '') {{ $pasien->usia }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12 resultBday">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>BB<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="bb" id="bb" onchange="getBhp()" value="@if($pasien != '') {{ $pasien->bb }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>BHP<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="bhp" id="resultBhp" readonly="" value="@if($pasien != '') {{ $pasien->bhp }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama OrangTua<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="nama_orangtua" value="@if($pasien != '') {{ $pasien->nama_orangtua }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
               <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea name="alamat" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">@if($pasien != '') {{ $pasien->alamat }} @endif</textarea>
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Riwayat Alergi<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea name="riwayat_alergi" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">@if($pasien != '') {{ $pasien->riwayat_alergi }} @endif</textarea>
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
            </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
         <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Riwayat Terdahulu<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <textarea name="riwayat_terdahulu" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">@if($pasien != '') {{ $pasien->riwayat_terdahulu }} @endif</textarea>
          </div>
        </div>
         <div class='clearfix p-b-5'></div>
          <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Di Khitan<span class="text-red">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <input type="text" value="@if($pasien != '') {{ date('d-m-Y', strtotime($pasien->tanggal)) }} @endif" id="form_day" autocomplete="off" class="form-control day" data-date-format="dd-mm-yyyy" name="tanggal" placeholder="dd/mm/yyyy">
            </div>
          </div>
         <div class='clearfix p-b-5'></div>
         <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jam Khitan<span class="text-red">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <input type="text" name="jam" value="@if($pasien != '') {{ $pasien->jam }} @endif" id="timepicker" class="form-control input-sm customInput col-md-7 col-xs-12 timepicker">
            </div>
          </div>
         <div class='clearfix p-b-5'></div>
         <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tempat Khitan<span class="text-red">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                <label id="tempatR" ><input type="radio" name="tempat_khitan" value="Rumah Pasien" class="flat-red" @if($pasien != '') {{ $pasien->tempat }} checked @endif> <span class="p-l-5">Rumah</span></label>
              </div>
              <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                <label id="tempatK"><input type="radio" name="tempat_khitan" value="Klinik" class="flat-red" @if($pasien != '') {{ $pasien->tempat }} checked  @endif> <span class="p-l-5">Klinik</span></label>
              </div>
            </div>
          </div>
          <?php
            $tempat = "";
            if($pasien != '' && !empty($pasien)){
              $tempat = $pasien->tempat;
            }
          ?>
         <div class='clearfix p-b-5'></div>
         <span class="panelAlamatRumah" @if($tempat == 'Klinik' || $tempat == '') style="display:none;" @endif>
           <div class="form-group">
             <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='alamat_khitan'>Alamat Khitan<span class="text-red">*</span></label>
             <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
               <input name="alamat_khitan" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2" value=" @if($pasien != '') {{ $pasien->tempat_khitan }} @endif">
             </div>
           </div>
           <div class='clearfix p-b-5'></div>
         </span>
          <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Pilih Operator<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <select class="form-control select2" style="width: 100%;" name="users_id">
              <option selected="selected" style="font-weight: bold;">Plih Operator :</option>
              @foreach ($operator as $op)
                 <option value="{{ $op->id }}" @if($pasien != '') @if($pasien->users_id == $pasien->users_id) selected @endif @endif>{{ $op->name }}</option>
              @endforeach
            </select>
           </div>
        </div>
        <div class='clearfix p-b-5'></div>
         <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Pilih Metode<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <select class="form-control select2" style="width: 100%;" name="metode_id">
              <option selected="selected" style="font-weight: bold;">Plih salah satu :</option>
              @foreach ($metode_sunat as $ms)
                 <option value="{{ $ms->id_metode }}" @if($pasien != '') @if($pasien->metode_id == $pasien->metode_id) selected @endif @endif>{{ $ms->nama_metode }}</option>
              @endforeach
            </select>
           </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Referensi<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <select class="form-control select2" style="width: 100%;" name="referensi" id="referensi">
              <?php
                $val_referensi = ['Internet (website)','Teman','Brosur','Instagram','Facebook'];
                $key = 0;
                $select = ['','','','',''];

                if(in_array($pasien->referensi,$val_referensi)){
                  $key = array_keys($val_referensi,$pasien->referensi);
                  $select[$key[0]] = 'selected';
                }
              ?>

              <option selected="selected">Plih salah satu : {{$key[0]}}</option>
              <option value="Internet (website)" {{$select[0]}}>Internet (Website Cahaya Khitan)</option>
              <option value="Teman" {{$select[1]}}>Teman</option>
              <option value="Brosur" {{$select[2]}}>Brosur</option>
              <option value="Instagram" {{$select[3]}}>Instagram</option>
              <option value="Facebook" {{$select[4]}}>Facebook</option>
            </select>
           </div>
        </div>
        <div class='clearfix p-b-5'></div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
      <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>

  </form>
</div>

<script type="text/javascript">
  // $('input[name=tempat]').click(function(){
  //   swal('ini tempat');
  // })
  $('#tempatR').click(function(){
    $('#tempat_khitan').html('Alamat Khitan');
    $('.panelAlamatRumah').show();
    // $('.panelAlamatKlinik').hide();
  });
  $('#tempatK').click(function(){
    $('#tempat_khitan').html('Alamat Klinik');
    // $('.panelAlamatKlinik').show();
    $('.panelAlamatRumah').show();
  });

$(document).ready(function(){
    $('input.timepicker').timepicker({
      timeFormat: 'h:mm p',
      interval: 5,
      minTime: '6',
      maxTime: '6:00pm',
      defaultTime: '7',
      startTime: '07:00',
      dynamic: false,
      dropdown: true,
      scrollbar: true
    });
});

$(document).ready(function(){
  $('.day').datetimepicker({
    weekStart: 2,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0,
  });
});


  function submitBday() {
    var Q4A = "";
    var Bdate = document.getElementById('tgl_lahir').value;
    var Bday = +new Date(Bdate);
    Q4A += ~~ ((Date.now() - Bday) / (31557600000))+" tahun";
    var theBday = document.getElementById('resultBday');
    theBday.value = Q4A;
}

function getBhp(){
  var isi = "";
  var GBhp = document.getElementById('bb').value;
  if (GBhp < 12) {
    isi = "Drop";
  }else if (GBhp <= 25) {
    isi = "Sirup";
  }else {
    isi = "Tablet";
  }
  console.log(isi);
  $('#resultBhp').val(isi);
}

  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

 $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addUpdate') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });

  // $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
  //   checkboxClass: 'icheckbox_flat-blue',
  //   radioClass: 'iradio_flat-blue'
  // });
</script>
