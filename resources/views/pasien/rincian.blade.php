@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <section class="content-header">
    <h1>
      {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li>Pasien</li>
      <li class="active">{{ $data['title'] }}</li>
      <li>{{ $id }}</li>
    </ol>
  </section>

  <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
        <div class="loading" align="center" style="display: none;">
            <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
        </div>
    </div>

  <section class="content">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-primary main-layer">
            <div class="p-5 m-5">
                <a href="{{ route('pasien') }}" class="btn btn-sm btn-success">
                    <span class="fa fa-reply"></span> &nbsp Kembali
                </a>
                <a href="{{ url('pasien/cetak_struk').'/'.$biaya->pasien_id }}" target="_blank" class="btn btn-sm btn-danger">
                    <span class="fa fa-print"></span> &nbsp Cetak
                </a>
            </div>
            <div class="col-md-12">
                <h2 class="text-center">Rincian Pasien</h2><hr>
                <div class="alert alert-secondary">
                    <div class="row">
                        <div class="col-md-6">
                            <table>
                                <tr>
                                    <th width="100px">ID Pasien</th>
                                    <td><span>: {{ $biaya->pasien_id }}</span></td>
                                </tr>
                                <tr>
                                    <th>Nama Pasien </th>
                                    <td><span>: {{ $biaya->nama }}</span></td>
                                </tr>
                                <tr>
                                    <th>Usia</th>
                                    <td><span>: {{ $biaya->usia }} tahun</span></td>
                                </tr>
                                <tr>
                                    <th>BB</th>
                                    <td><span>: {{ $biaya->bb }} kg</span></td>
                                </tr>
                                <tr>
                                    <th>Obat</th>
                                    <td><span>: {{ $biaya->bhp }}</span></td>
                                </tr>
                                <tr>
                                    <th></th>
                                    <td>
                                        @foreach($obat as $a) 
                                            - {{$a->nama_obat}} <br>
                                        @endforeach
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table>
                               <tr>
                                    <th>Alkes/ BHP</th>
                                    <td>
                                        @foreach($bhp as $a) 
                                            - {{$a->nama_barang}} <br>
                                        @endforeach
                                    </td>
                               </tr>
                               <tr>
                                    <th width="100px">Metode Sunat</th>
                                    <td><span>: </span> {{ $biaya->nama_metode }}</td>
                                </tr>
                                <tr>
                                    <th>Mainan</th>
                                    <td><span>: </span>@if(empty($biaya->mainan)) Tidak @else Ya @endif</td>
                                </tr>
                                @if($biaya->mainan != '')
                                <tr>
                                    <th></th>
                                    <td>- {{ $biaya->nama_mainan }}</td>
                                </tr>
                                @endif
                                <tr>
                                    <th>Tempat Khitan</th>
                                    <td><span>: {{ $biaya->tempat.' '.$biaya->tempat_khitan }}</span></td>
                                </tr>
                                <tr>
                                    <th>Tanggal</th>
                                    <td><span>: {{ $biaya->tanggal }}</span></td>
                                </tr>
                                <tr>
                                    <th>Jam</th>
                                    <?php $jam = substr($biaya->jam, 0, 5); ?>
                                    <td><span>: {{ $jam }}</span></td>
                                </tr>
                            </table>
                        </div>    
                    </div>
                </div>
            </div>
            <div class='clearfix'></div>
        </div>
      </div>
    </div>
  </section>
@stop

@section('extended_js')
  <script type="text/javascript">
   
  </script>
@stop
