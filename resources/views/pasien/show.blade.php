<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
		<i class="fa fa-eye iconLabel m-r-15"></i> Data Pasien
	</h4>
    <div class="m-l-15">
      <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5" style="margin-top: 10px;"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>
  <hr class="m-t-0">
  <form class="form-save">
    <div class="box-body" style="font-weight: bold;">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Pasien<span class="text-red">*</span></label>
      					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      						<input readonly="" name="nama" value="{{ $data->nama }}" class="form-control input-sm customInput col-md-7 col-xs-12">
      					</div>
      				</div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Lahir<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input readonly="" name="tgl_lahir" value="{{ $data->tgl_lahir }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Usia<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input readonly="" name="usia" value="{{ $data->usia }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>BB<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input readonly="" name="bb" value="{{ $data->bb }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama OrangTua<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input readonly="" name="nama_orangtua" value="{{ $data->nama_orangtua }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
               <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea readonly="" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">{{ $data->alamat }}</textarea>
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Riwayat Alergi<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea readonly="" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">{{ $data->riwayat_alergi }}</textarea>
                </div>
              </div>
               <div class='clearfix p-b-5'></div>              
            </div>
        </div>
      </div>

      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Riwayat Terdahulu<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <textarea readonly="" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">{{ $data->riwayat_terdahulu }}</textarea>
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Metode<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input readonly="" name="pil_metode" value="{{ $data->metode->nama_metode }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>        
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Referensi<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input value="{{ $data->referensi }}" readonly="" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Di Khitan<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input name="pil_metode" value="{{ $data->tanggal }}" readonly="" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
         <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jam Khitan<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input readonly="" name="jam" value="{{ $data->jam }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tempat Khitan<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input readonly="" name="pil_metode" value="{{ $data->tempat }}. Alamat: {{$data->tempat_khitan}}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Operator / Dokter<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input readonly="" name="pil_metode" value="{{ $data->metode->nama_metode }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };

  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

</script>
