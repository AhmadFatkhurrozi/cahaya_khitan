<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<style type="text/css">   
    body {
        background: grey;
        margin-top: 70px;
        margin-bottom: 120px;
    }
</style>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body p-0">
                    <div class="row p-5">
                        <div class="col-md-6">
                            <img height="170px" src="{{ url('dist/img/logo/cahaya khitan full.png') }}">
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="font-weight-bold mb-1">Invoice #{{ $biaya->id_biaya }}</p>
                            <p class="text-muted">{{ date('d-m-Y') }}</p>
                        </div>
                    </div>

                    <hr class="my-5">

                    <div class="row pb-5 p-5">
                        <div class="col-md-6">
                            <p class="font-weight-bold mb-4">Informasi Klinik</p>
                            <p class="mb-1">{{ $biaya->nama_cabang }}</p>
                            <p class="mb-1">{{ $biaya->alamat }}</p>
                            <p class="mb-1">{{ $biaya->telepon }}</p>
                        </div>

                        <div class="col-md-6 text-right">
                            <p class="mb-1"><span class="text-muted">Nama Operator</span> <br>{{ $biaya->name }}</p>
                        </div>
                    </div>

                    <div class="row p-5">
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th class="border-0 text-uppercase small font-weight-bold">Nama Pasien</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Metode</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Keterangan</th>
                                        <th class="border-0 text-uppercase small font-weight-bold">Biaya</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $biaya->nama }}</td>
                                        <td>{{ $biaya->nama_metode }}</td>
                                        <td>{{ $biaya->keterangan }}</td>
                                        <td>@currency( $biaya->harga_metode )</td>
                                    </tr>                                   
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="d-flex flex-row-reverse bg-dark text-white p-4">
                        <div class="py-3 px-5 text-right">
                            <div class="mb-2">Total Biaya</div>
                            <div class="h2" style="color: #FFFFFF; font-weight: bold;">@currency( $biaya->harga_metode )</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>    
    <div class="text-light mt-5 mb-5 text-center small">by : <a class="text-light" target="_blank" href="https://cahayakhitan.com/">cahayakhitan.com</a></div>

</div>

<script>
    window.print();
</script>