<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
		<i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Data Pasien
	</h4>
  <hr class="m-t-0">

  <form class="form-save">
    <div class="box-body">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Pasien<span class="text-red">*</span></label>
      					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
      						<input type="text" name="nama" class="form-control input-sm customInput col-md-7 col-xs-12">
      					</div>
      				</div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Lahir<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="tgl_lahir" id="tgl_lahir" onchange="submitBday()" class="form-control day" data-date-format="dd-mm-yyyy" placeholder="dd-mm-yyyy">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Usia<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="usia" id="resultBday" readonly="" class="form-control input-sm customInput col-md-7 col-xs-12 resultBday">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>BB<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="bb" id="bb" onchange="getBhp()" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>BHP<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="bhp" id="resultBhp" readonly="" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama OrangTua<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" name="nama_orangtua"  class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
               <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea name="alamat" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="1"></textarea>
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Riwayat Alergi<span class="text-red">*</span></label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <textarea name="riwayat_alergi" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="1"></textarea>
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
               <div class="form-group">
                 <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Riwayat Terdahulu<span class="text-red">*</span></label>
                 <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                   <textarea name="riwayat_terdahulu" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="1"></textarea>
                 </div>
               </div>
              <div class='clearfix p-b-5'></div>
            </div>
        </div>
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
          <div class='clearfix p-b-5'></div>
          <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Di Khitan<span class="text-red">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <input type="text"  id="form_day" autocomplete="off" class="form-control day" data-date-format="dd-mm-yyyy" name="tanggal" placeholder="dd/mm/yyyy">
            </div>
          </div>
         <div class='clearfix p-b-5'></div>
         <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jam Khitan<span class="text-red">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <input type="text" name="jam" id="timepicker" class="form-control input-sm customInput col-md-7 col-xs-12 timepicker">
            </div>
          </div>
         <div class='clearfix p-b-5'></div>
         <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tempat Khitan<span class="text-red">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                <label id="tempatR" ><input type="radio" name="tempat_khitan" value="Rumah Pasien" class="flat-red"> <span class="p-l-5">Rumah</span></label>
              </div>
              <div class="col-lg-6 co-md-6 col-sm-12 col-xs-12">
                <label id="tempatK"><input type="radio" name="tempat_khitan" value="Klinik" class="flat-red"> <span class="p-l-5">Klinik</span></label>
              </div>
            </div>
          </div>

         <div class='clearfix p-b-5'></div>
         <span class="panelAlamatRumah">
           <div class="form-group">
             <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='alamat_khitan'>Alamat Khitan<span class="text-red">*</span></label>
             <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
               <input name="alamat_khitan" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2" >
             </div>
           </div>
           <div class='clearfix p-b-5'></div>
         </span>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Pilih Operator<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <select class="form-control select2" style="width: 100%;" name="users_id" id="pilihan">
              <option selected="selected" style="font-weight: bold;">Plih Operator :</option>
              @foreach ($operator as $op)
                 <option value="{{ $op->id }}">{{ $op->name }}</option>
              @endforeach
            </select>
           </div>
        </div>
        <div class='clearfix p-b-5'></div>
         <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Pilih Metode<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <select class="form-control select2" style="width: 100%;" name="metode_id" id="pilihan3">
              <option selected="selected" style="font-weight: bold;">Plih salah satu :</option>
              @foreach ($metode_sunat as $ms)
                 <option value="{{ $ms->id_metode }}">{{ $ms->nama_metode }}</option>
              @endforeach
            </select>
           </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Referensi<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <select class="form-control select2" style="width: 100%;" name="referensi" id="pilihan2">
              <option selected="selected">Plih salah satu :</option>
              <option value="Internet (website)" >Internet (Website Cahaya Khitan)</option>
              <option value="Teman">Teman</option>
              <option value="Brosur">Brosur</option>
              <option value="Instagram">Instagram</option>
              <option value="Facebook">Facebook</option>
            </select>
           </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Gratis Biaya : <span class="text-red">(Optional)</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input type="checkbox" name="gratis_biaya" value="gratis">Fee Operator & Fee Asisten.<br>
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
         <div class="form-group">
           <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Keterangan/Alasan<span class="text-red">*</span></label>
           <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
             <textarea name="keterangan" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="1">-</textarea>
             <small>Beri alasan / keterangan singkat jika Pasien Khitan Gratis</small>
           </div>
         </div>
        <div class='clearfix p-b-5'></div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
      <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>

  </form>
</div>

<script type="text/javascript">
  // $('input[name=tempat]').click(function(){
  //   swal('ini tempat');
  // })
  $('#tempatR').click(function(){
    $('#tempat_khitan').html('Alamat Khitan');
    $('.panelAlamatRumah').show();
    // $('.panelAlamatKlinik').hide();
  });
  $('#tempatK').click(function(){
    $('#tempat_khitan').html('Alamat Klinik');
    // $('.panelAlamatKlinik').show();
    $('.panelAlamatRumah').show();
  });

    $('#pilihan').chosen();
    $('#pilihan2').chosen();
    $('#pilihan3').chosen();


$(document).ready(function(){
    $('input.timepicker').timepicker({
      timeFormat: 'h:mm p',
      interval: 5,
      minTime: '6',
      maxTime: '6:00pm',
      defaultTime: '7',
      startTime: '07:00',
      dynamic: false,
      dropdown: true,
      scrollbar: true
    });
});

$(document).ready(function(){
  $('.day').datetimepicker({
    weekStart: 2,
    todayBtn:  1,
    autoclose: 1,
    todayHighlight: 1,
    startView: 2,
    minView: 2,
    forceParse: 0,
  });
});


 function submitBday() {
    // var Q4A = "";
    // var Bdate = document.getElementById('tgl_lahir').value;
    // var Bday = +new Date(Bdate);
    // Q4A += ~~ ((Date.now() - Bday) / (31557600000))+" tahun";
    // var theBday = document.getElementById('resultBday');
    // theBday.value = Q4A;
    var date = new Date();
    
    var a = document.getElementById('tgl_lahir').value;
    var theBday = document.getElementById('resultBday');

    var tahunSekarang = date.getFullYear();
    var tahunInput = a.split('-')[2];
    var usia = tahunSekarang - tahunInput;

    theBday.value = usia;
   }

function getBhp(){
  var isi = "";
  var GBhp = document.getElementById('bb').value;
  if (GBhp < 12) {
    isi = "Drop";
  }else if (GBhp <= 25) {
    isi = "Sirup";
  }else {
    isi = "Tablet";
  }
  console.log(isi);
  $('#resultBhp').val(isi);
}

  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

 $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addPasien') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });

  // $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
  //   checkboxClass: 'icheckbox_flat-blue',
  //   radioClass: 'iradio_flat-blue'
  // });
</script>
