<?php use App\Models\Daftar_cabang; use App\Models\PermintaanObat; use App\Models\PermintaanBhp; ?>
<section class="sidebar">
    <div class="user-panel my-5">
        <div class="pull-left" style="margin-bottom: 20px;">
            @if (Auth::getUser()->photo != null && is_file(Auth::getUser()->photo))
                <img src="{!! url(Auth::getUser()->photo) !!}" height="50px" width="50px" class="img-circle" alt="User Image">
            @elseif (Auth::getUser()->gender == 'Laki - Laki')
                <img src="{{ url('/') }}/dist/img/avatar/user_man.png" height="50px" width="50px" class="img-circle" alt="User Image">
            @elseif (Auth::getUser()->gender == 'Perempuan')
                <img src="{{ url('/') }}/dist/img/avatar/user_girl.png" height="50px" width="50px" class="img-circle" alt="User Image">
            @else
                <img src="{{ url('/') }}/dist/img/avatar/default.png" height="50px" width="50px" class="img-circle" alt="User Image">
            @endif
        </div>

        <div class="pull-left info">
            <p>{{ Auth::getUser()->name }}</p>
            @if(Auth::getUser()->level_user == 4 || Auth::getUser()->level_user == 5 || Auth::getUser()->level_user == 6 || Auth::getUser()->level_user == 3)
            <p><small class="text-dark">{{ Daftar_cabang::where('id_cabang', Auth::getUser()->cabang_id)->first()->nama_cabang }}</small></p>
            @endif
            <a href="javascript:void(0)"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
    </div>

    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li @if($data['mn_active'] == "dashboard") class='active' @endif>
            <a href="{{ route('dashboard') }}">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
            </a>
        </li>
        @if (Auth::getUser()->level_user == 1 || Auth::getUser()->level_user == 2 || Auth::getUser()->level_user == 3)
        <li @if($data['mn_active'] == "pengguna") class='active treeview' @else class="treeview" @endif >
            <a href="javascript:void(0)">
                <i class="fa fa-users"></i> <span>Pengguna</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <!-- ADMIN -->
                @if (Auth::getUser()->level_user != 2 && Auth::getUser()->level_user != 3)
                <li @if($data['submn_active'] == "admin") class='active' @endif>
                    <a href="{{ route('admin') }}"><i class="fa fa-angle-double-right"></i> Admin</a>
                </li>
                <li @if($data['submn_active'] == "owner") class='active' @endif>
                    <a href="{{ route('owner') }}"><i class="fa fa-angle-double-right"></i> Owner</a>
                </li>
                <li @if($data['submn_active'] == "kepala_cabang") class='active' @endif>
                    <a href="{{ route('kepala_cabang') }}"><i class="fa fa-angle-double-right"></i> Kepala Cabang</a>
                </li>
                @endif
                <!-- Kepala Cabang & Owner -->
                @if (Auth::getUser()->level_user == 1 || Auth::getUser()->level_user == 2 || Auth::getUser()->level_user == 3)
                <li @if($data['submn_active'] == "operator") class='active' @endif>
                    <a href="{{ route('operator') }}"><i class="fa fa-angle-double-right"></i> Operator</a>
                </li>
                <li @if($data['submn_active'] == "bendahara") class='active' @endif>
                    <a href="{{ route('bendahara') }}"><i class="fa fa-angle-double-right"></i> Bendahara</a>
                </li>
                <li @if($data['submn_active'] == "marketing") class='active' @endif>
                    <a href="{{ route('marketing') }}"><i class="fa fa-angle-double-right"></i> Marketing</a>
                </li>
                @endif
            </ul>
        </li>
        @endif

        <!-- Operator -->
        @if (Auth::getUser()->level_user == 4 || Auth::getUser()->level_user == 6)
        <li @if($data['mn_active'] == "pasien") class='active' @endif>
            <a href="{{ route('pasien') }}">
                <i class="fa fa-user"></i> <span>Pasien Khitan</span>
            </a>
        </li>
        @endif

        <!-- ADMIN -->
        @if (Auth::getUser()->level_user == 1)
        @php $permin = PermintaanObat::where('status_permintaan', 'menunggu')->count() @endphp
        @php $perminn = PermintaanBhp::where('status_permintaan', 'menunggu')->count() @endphp
        
        <li @if($data['mn_active'] == "obat") class='active treeview' @else class="treeview" @endif >
            <a href="javascript:void(0)">
                <i class="fa fa-database"></i> <span>Data Obat</span> <i class="fa fa-angle-left pull-right"></i>
                @if($permin != 0)
                    <label class="badge badge-danger">{{ $permin }}</label>
                @endif  
            </a>
            <ul class="treeview-menu">
                <li @if($data['submn_active'] == "stok_obat") class='active' @endif>
                    <a href="{{ route('obatAdmin') }}">
                        <i class="fa fa-angle-double-right"></i><span>Data Obat</span>
                    </a>
                </li>
                <li @if($data['submn_active'] == "permintaan_obat") class='active' @endif>
                    <a href="{{ route('permintaanObat') }}">
                        <i class="fa fa-angle-double-right"></i><span>Permintaan Obat</span>
                        @if($permin != 0)
                            <label class="badge badge-danger">{{ $permin }}</label>
                        @endif
                    </a>
                </li>
            </ul>
        </li>
        <li @if($data['mn_active'] == "bhp") class='active treeview' @else class="treeview" @endif >
            <a href="javascript:void(0)">
                <i class="fa fa-database"></i> <span>Data Alkes & BHP</span> <i class="fa fa-angle-left pull-right"></i>
                @if($perminn != 0)
                    <label class="badge badge-danger">{{ $perminn }}</label>
                @endif
            </a>
            <ul class="treeview-menu">
                <li @if($data['submn_active'] == "bhp") class='active' @endif>
                    <a href="{{ route('bhpAdmin') }}">
                        <i class="fa fa-angle-double-right" aria-hidden="true"></i><span>Data Alkes/BHP</span>
                    </a>
                </li>
                <li @if($data['submn_active'] == "permintaan_bhp") class='active' @endif>
                    <a href="{{ route('permintaanBhp') }}">
                        <i class="fa fa-angle-double-right"></i><span>Permintaan Alkes/BHP</span>
                        @if($perminn != 0)
                            <label class="badge badge-danger">{{ $perminn }}</label>
                        @endif
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if (Auth::getUser()->level_user == 1 || Auth::getUser()->level_user == 5)
        <li @if($data['mn_active'] == "master_data") class='active treeview' @else class="treeview" @endif >
            <a href="javascript:void(0)">
                <i class="fa fa-database"></i> <span>Master Data</span> <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                @if (Auth::getUser()->level_user == 1)
                <li @if($data['submn_active'] == "metode_sunat") class='active' @endif>
                    <a href="{{ route('metode_sunat') }}"><i class="fa fa-angle-double-right"></i>Metode Sunat</a>
                </li>
                <li @if($data['submn_active'] == "daftar_cabang") class='active' @endif>
                    <a href="{{ route('daftar_cabang') }}"><i class="fa fa-angle-double-right"></i>Klinik</a>
                </li>
                <li @if($data['submn_active'] == "gaji") class='active' @endif>
                    <a href="{{ route('gaji') }}"><i class="fa fa-angle-double-right"></i>Gaji</a>
                </li>
                @else
                <li @if($data['submn_active'] == "daftar_mainan") class='active' @endif>
                    <a href="{{ route('daftar_mainan') }}"><i class="fa fa-angle-double-right"></i>Daftar Mainan</a>
                </li>
                @endif
            </ul>
        </li>
        @endif

        @if (Auth::getUser()->level_user == 1)
        <li class="header">REPORT</li>
        {{-- <li @if($data['mn_active'] == "pengeluaran") class='active' @endif>
            <a href="{{ route('pengeluaranAdmin') }}">
                <i class="fa fa-list-alt"></i><span>Pengeluaran</span>
            </a>
        </li> --}}

        <li @if($data['mn_active'] == "neraca_cabang") class="active" @endif>
            <a href="{{ route('neraca_cabang') }}" ><i class="fa fa-building"> Neraca Cabang</i></a>
        </li>
        @endif

        <!-- OPERATOR -->
        @if(Auth::getUser()->level_user == 4)
        <li @if($data['submn_active'] == "stok_obat") class='active' @endif>
            <a href="{{ route('obat') }}"><i class="fa fa-plus-square"></i> Data Obat</a>
        </li>
        <li @if($data['submn_active'] == "stok_bhp") class='active' @endif>
            <a href="{{ route('bhp') }}"><i class="fa fa-medkit"></i> Data Alkes/ BHP</a>
        </li>
        @endif

        <!-- BENDAHARA -->
        @if (Auth::getUser()->level_user == 5)
        @php $permint = PermintaanObat::where('cabang_id', Auth::getUser()->cabang_id)->where('status_permintaan', 'selesai')->count() @endphp
        @php $permintb = PermintaanBhp::where('cabang_id', Auth::getUser()->cabang_id)->where('status_permintaan', 'selesai')->count() @endphp

        <li @if($data['mn_active'] == "obat") class='active treeview' @else class="treeview" @endif >
            <a href="javascript:void(0)">
                <i class="fa fa-plus-square"></i> <span>Data Obat</span> <i class="fa fa-angle-left pull-right"></i>
                @if($permint != 0)
                    <label class="badge badge-danger">{{ $permint }}</label>
                @endif  
            </a>
            <ul class="treeview-menu">
                <li @if($data['submn_active'] == "stok_obat") class='active' @endif>
                    <a href="{{ route('obat') }}"><i class="fa fa-angle-double-right"></i> Data Obat</a>
                </li>
                <li @if($data['submn_active'] == "permintaan_obat") class='active' @endif>
                    <a href="{{ route('permintaanObat') }}"><i class="fa fa-angle-double-right"></i> Permintaan Obat 
                        @if($permint != 0)
                            <label class="badge badge-danger">{{ $permint }}</label>
                        @endif
                    </a>
                </li>
                <li @if($data['submn_active'] == "pengadaan_lokal") class='active' @endif>
                    <a href="{{ route('obatLokal') }}"><i class="fa fa-angle-double-right"></i> Pengadaan Lokal 
                    </a>
                </li>
            </ul>
        </li>

        <li @if($data['mn_active'] == "bhp") class='active treeview' @else class="treeview" @endif >
            <a href="javascript:void(0)">
                <i class="fa fa-medkit"></i> <span>Data Alkes & BHP</span>
                @if($permintb != 0)
                    <label class="badge badge-danger">{{ $permintb }}</label>
                @endif 
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li @if($data['submn_active'] == "stok_bhp") class='active' @endif>
                    <a href="{{ route('bhp') }}"><i class="fa fa-angle-double-right"></i> Data Alkes & BHP</a>
                </li>
                <li @if($data['submn_active'] == "permintaan_bhp") class='active' @endif>
                    <a href="{{ route('permintaanBhp') }}"><i class="fa fa-angle-double-right"></i> Permintaan Alkes & BHP
                    @if($permintb != 0)
                        <label class="badge badge-danger">{{ $permintb }}</label>
                    @endif
                    </a>
                </li>
                <li @if($data['submn_active'] == "pengadaan_lokal") class='active' @endif>
                    <a href="{{ route('bhpLokal') }}"><i class="fa fa-angle-double-right"></i> Pengadaan Lokal 
                    </a>
                </li>
            </ul>
        </li>
        @endif

        @if(Auth::getUser()->level_user == 4 || Auth::getUser()->level_user == 5)
        <li class="header">REPORT</li>
            @if(Auth::getUser()->level_user != 4)
            <li @if($data['submn_active'] == "beban_gaji") class='active' @endif>
                <a href="{{ route('beban_gaji') }}">
                    <i class="fa fa-dollar"></i><span>Beban Gaji</span>
                </a>
            </li>
            @endif
            <li @if($data['submn_active'] == "fee_operator") class='active' @endif>
                <a href="{{ route('fee_operator') }}">
                    <i class="fa fa-dollar"></i><span>Fee OP & Asisten</span>
                </a>
            </li>
        {{-- <li @if($data['mn_active'] == "pengeluaran") class='active' @endif>
            <a href="{{ route('pengeluaran') }}">
                <i class="fa fa-list-alt"></i><span>Pengeluaran</span>
            </a>
        </li>
        <li @if($data['mn_active'] == "pemasukan") class='active' @endif>
            <a href="{{ route('pemasukan') }}"><i class="fa fa-list-alt"></i> Pemasukan</a>
        </li> --}}
        @endif

        <li @if($data['mn_active'] == "neraca") class="active" @endif>
            <a href="{{ route('neraca') }}" ><i class="fa fa-balance-scale"> Neraca</i></a>
        </li>

        @if(Auth::getUser()->level_user == 5)
            <li @if($data['mn_active'] == "bagi_hasil") class="active" @endif>
                <a href="{{ route('bagi_hasil') }}" ><i class="fa fa-share"> Bagi Hasil</i></a>
            </li>
        @endif
    </ul>
</section>
