<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $data['title'] }} | Cahaya Khitan</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/')}}/dist/img/logo/icon.png"/>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ url('/')}}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{ url('/')}}/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="{{ url('/')}}/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/animate.css">
    <link rel="stylesheet" href="{{ url('/') }}/plugins/sweetalert/sweetalert.css">

    <style media="screen">
      .login-box-body {
        padding-bottom: 5px !important;
        border-radius: 5px !important;
      }
      .login-box-msg {
        font-weight: bold;
        font-size: 16px;
      }
    </style>
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <img src="{{ url('/')}}/dist/img/logo/cahaya khitan text.png" alt="Cahaya Khitan" width="100%">
      </div>
      <!-- /.login-logo -->
      <div class="login-box-body" >
        <p class="login-box-msg">Login use your Account</p>

        <form class="form-login">
          <div class="form-group has-feedback">
            <input type="text" name="email" class="form-control" placeholder="Email">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox"> Remember Me
                </label>
              </div>
              <a href="{{ route('reset') }}"><span>Lupa Password ?</span></a>
            </div>
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat btn-login">Sign In</button>
            </div>
          </div>
        </form>
        <hr style="border-color:#ababab;margin: 5px 0;">
        <div class="social-auth-links text-center">
          ©2019 Cahaya Khitan<br/>Dikembangkan oleh <a href="https://www.natusi.co.id" target='blank'>Natusi</a><br/>
        </div>
      </div>
    </div>

    <script src="{{ url('/')}}/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="{{ url('/')}}/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('/')}}/plugins/iCheck/icheck.min.js"></script>
    <script src="{{ url('/')}}/js/animate.js"></script>
    <script src="{{ url('/') }}/plugins/sweetalert/sweetalert-dev.js"></script>
    <script src="{{ url('/') }}/js/validate.js"></script>
    
	<script type="text/javascript" src="{!! url('/') !!}/js/jquery.backstretch.min.js"></script>
    <script>
      $('.login-logo').animateCss('bounceInDown');
      $('.login-box-body').animateCss('flipInY');
			$.backstretch("{!! url('dist/img/background/sunat.png') !!}", {speed: 500});
		</script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    <script type="text/javascript">
      $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
      });

      $('.form-login').submit(function(e){
        e.preventDefault();
        var button = $('.btn-login');
        var data = $('.form-login').serialize();
        button.html('Please wait...').attr('disabled', true);
        $.post("{!! route('doLogin') !!}", data).done(function(data){
          button.html('Login').removeAttr('disabled');
          $('.form-login').validate(data.result, 'has-error');
          if(data.status == 'success'){
            window.location.replace("{!! url($data['next_url']) !!}");
          } else if(data.status == 'error' || data.status == 'warning'){
            swal('Maaf', data.message, data.status);
          } else {
            button.animateCss('shake');
          }
        }).fail(function() {
          swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
          button.html('Login').removeAttr('disabled');
        });
      });

      @if(!empty(Session::get('message')))
        swal({
          title : "{{ Session::get('title') }}",
          text : "{{ Session::get('message') }}",
          type : "{{ Session::get('type') }}",
          showConfirmButton: true
        });
      @endif
    </script>
  </body>
</html>
