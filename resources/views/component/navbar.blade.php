<?php use App\Models\Daftar_cabang; ?>
<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('dashboard') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
            <img src="{{ url('/')}}/dist/img/logo/icon.png" alt="Cahaya Khitan" width="50%">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
            <img src="{{ url('/')}}/dist/img/logo/cahaya khitan text.png" alt="Cahaya Khitan" width="75%">
        </span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="javascript:void(0)" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
                        @if (Auth::getUser()->photo != null && is_file(Auth::getUser()->photo))
                            <img src="{!! url(Auth::getUser()->photo) !!}" class="user-image" alt="User Image">
                        @elseif (Auth::getUser()->gender == 'Laki - Laki')
                            <img src="{{ url('/') }}/dist/img/avatar/user_man.png" class="user-image" alt="User Image">
                        @elseif (Auth::getUser()->gender == 'Perempuan')
                            <img src="{{ url('/') }}/dist/img/avatar/user_girl.png" class="user-image" alt="User Image">
                        @else
                            <img src="{{ url('/') }}/dist/img/avatar/default.png" class="user-image" alt="User Image">
                        @endif
                        <span class="hidden-xs">{{ Auth::getUser()->name }}</span>
                        <i class='fa fa-caret-down p-l-5'></i>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if (Auth::getUser()->photo != null && is_file(Auth::getUser()->photo))
                                <img src="{!! url(Auth::getUser()->photo) !!}" class="img-circle" alt="User Image">
                            @elseif (Auth::getUser()->gender == 'Laki - Laki')
                                <img src="{{ url('/') }}/dist/img/avatar/user_man.png" class="img-circle" alt="User Image">
                            @elseif (Auth::getUser()->gender == 'Perempuan')
                                <img src="{{ url('/') }}/dist/img/avatar/user_girl.png" class="img-circle" alt="User Image">
                            @else
                                <img src="{{ url('/') }}/dist/img/avatar/default.png" class="img-circle" alt="User Image">
                            @endif

                            <p>{{ Auth::getUser()->name }}</p>

                            @if(Auth::getUser()->level_user == 4 || Auth::getUser()->level_user == 5 || Auth::getUser()->level_user == 6 || Auth::getUser()->level_user == 3)
                            <p><small class="text-dark">{{ Daftar_cabang::where('id_cabang', Auth::getUser()->cabang_id)->first()->nama_cabang }}</small></p>
                            @endif

                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

    </nav>
</header>
