<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ $data['title'] }} | Cahaya Khitan</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ url('/')}}/dist/img/logo/icon.png"/>
    <!-- Tell the browser to be responsive to screen width -->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">

    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ url('/') }}/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ url('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
    <link rel="stylesheet" href="{{ url('/') }}/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="{{ url('/') }}/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="{{ url('/') }}/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" type="text/css" href="{{ url('/') }}/css/chosen/chosen.css">

    <!-- iCheck for checkboxes and radio inputs -->
    <link rel="stylesheet" href="{{ url('/') }}/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="{{ url('/') }}/plugins/iCheck/all.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/animate.css">
    <link rel="stylesheet" href="{{ url('/') }}/plugins/sweetalert/sweetalert.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/util.css">
    <link rel="stylesheet" href="{{ url('/') }}/css/custome.css">
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
    <link href="{!! url('css/bootstrap-datetimepicker.min.css') !!}" rel="stylesheet" type="text/css" />

    @yield('extended_css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        @include('component.navbar')

        <aside class="main-sidebar">
            @include('component.sidebar')
        </aside>

        <div class="content-wrapper">
            @yield('content')
        </div>

        <footer class="main-footer">
            <div class="pull-right hidden-xs">
                <b>Version</b> 1.0.0
            </div>
            <strong>Copyright &copy; 2019 Cahaya Khitan. Development by <a href="http://natusi.co.id">CV. Natusi</a>.</strong> All rights reserved.
        </footer>

    </div>
    <script src="{{ url('/') }}/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="{{ url('/') }}/dist/js/jquery.maskMoney.js" charset="utf-8"></script>
    <script src="{{ url('/') }}/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ url('/') }}/plugins/fastclick/fastclick.min.js"></script>
    <script src="{{ url('/') }}/dist/js/app.min.js"></script>
    <script src="{{ url('/') }}/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="{{ url('/') }}/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="{{ url('/') }}/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="{{ url('/') }}/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
    <script src="{!! url('js/bootstrap-datetimepicker.min.js') !!}"></script>

    <script src="{{ url('/')}}/js/animate.js"></script>
    <script src="{{ url('/') }}/plugins/sweetalert/sweetalert-dev.js"></script>
    <script src="{{ url('/') }}/js/validate.js"></script>
    <script src="{{ url('/') }}/js/datagrid.js"></script>
    <script src="{{ url('/') }}/js/chosen.jquery.min.js"></script>

    <script src="{{ url('/') }}/plugins/iCheck/icheck.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function Kosong() {
            swal({
                title: "MAAF !",
                text: "Fitur Belum Bisa Digunakan !!",
                type: "warning",
                timer: 2000,
                showConfirmButton: false
            });
        }

        @if(!empty(Session::get('message')))
        swal({
            title : "{{ Session::get('title') }}",
            text : "{{ Session::get('message') }}",
            type : "{{ Session::get('type') }}",
            showConfirmButton: true
        });
        @endif
    </script>

    @yield('extended_js')

</body>
</html>
