<?php use App\Models\Pasien; use App\Models\Daftar_cabang;?>
@extends('component.layout')

@section('extended_css')
<style media="screen">
    #calendar {
        max-width: 900px;
        margin: 0 auto;
    }
</style>
<link href="{{ asset('assets/packages/core/main.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/packages/daygrid/main.css') }}" rel="stylesheet" />
@stop

@section('content')
<section class="content-header">
    <h1>
        Dashboard
        <small>Version 2.0</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
    </ol>
</section>

<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<section class="content">

    @if(Auth::user()->level_user != 4 && Auth::user()->level_user != 6)
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Sudah Dikhitan</span>
                    <span class="info-box-number">{{ $di_khitan }}</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Gagal Khitan</span>
                    <span class="info-box-number">{{ $ggl_khitan }}</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-yellow"><i class="fa fa-download" aria-hidden="true"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Omset</span>
                    <span class="info-box-number">@currency ($omset->omset)</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>

        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-dollar"></i></span>
                <div class="info-box-content">
                    <span class="info-box-text">Laba Rugi</span>
                    <span class="info-box-number">@currency ($labarugi->laba_rugi)</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>
    </div>
    @else
    <div class="row">
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="info-box">
                <a href="{{ route('pasien') }}">
                    <span class="info-box-icon bg-aqua" style="text-decoration: none;"><i class="fa fa-users"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text"> Sudah Dikhitan</span>
                    <span class="info-box-number"> {{ $di_khitan }}</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="info-box">
                <a href="{{ route('pasien') }}">
                    <span class="info-box-icon bg-red"><i class="fa fa-users"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Gagal Khitan</span>
                    <span class="info-box-number">{{ $ggl_khitan }}</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="info-box">
                <a href="{{ route('pasien') }}">
                    <span class="info-box-icon bg-green"><i class="fa fa-users"></i></span>
                </a>
                <div class="info-box-content">
                    <span class="info-box-text">Menunggu</span>
                    <span class="info-box-number">{{ $menunggu }}</span>
                    <span>Per - {{ $waktu->format('F Y') }}</span>
                </div>
            </div>
        </div>
    </div>
    @endif

    <div class="row">
        <div class="col-md-12">

            {{-- Grafik Admin --}}

            @if(Auth::user()->level_user == 1)
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Pasien Di Khitan</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="khitan_year" style="width: 100%; height: 400px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Pasien Gagal Khitan</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ggl_khitan_year" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header with-border">
                    @if(Auth::getUser()->level_user == 1)
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Omset Cabang</strong></h3>
                    @else
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Omset</strong></h3>
                    @endif
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pemasukan" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header with-border">
                    @if(Auth::getUser()->level_user == 1)
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Laba Rugi Cabang</strong></h3>
                    @else
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Laba Rugi</strong></h3>
                    @endif
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="laba_rugi" style="width:100%; height:400px;"></div>
                        </div>
                    </div>
                </div>    
            </div>

            @endif

            @if(Auth::getUser()->level_user == 1 || Auth::getUser()->level_user == 2 || Auth::getUser()->level_user == 3 || Auth::getUser()->level_user == 4)
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title"><i class="fa fa-calendar" aria-hidden="true"></i> <strong>Jadwal Khitan Terdekat</strong></h3>
                        <div class="box-tools pull-right">
                            <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div id="calendar"></div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            @if(Auth::user()->level_user != 4 && Auth::user()->level_user != 6 && Auth::user()->level_user != 1)
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Omset</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="pemasukan" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Laba Rugi</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="laba_rugi" style="width:100%; height:400px;"></div>
                        </div>
                    </div>
                </div>    
            </div>
            @endif

            @if(Auth::user()->level_user != 1)
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Pasien Di Khitan</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="khitan_year" style="width: 100%; height: 400px;"></div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Pasien Gagal Khitan</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="ggl_khitan_year" style="width: 100%; height: 500px;"></div>
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i> <strong>Grafik Referensi</strong></h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div>
                </div>

                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div id="referensi" style="width:100%; height:400px;"></div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>

@stop

@section('extended_js')

  <script src="{{ url('/') }}/plugins/chartjs/Chart.min.js"></script>
  <script src="{{ url('/') }}/dist/js/pages/dashboard2.js"></script>
  <script src="{{ url('/') }}/dist/js/demo.js"></script>
  <script src="{{ url('/') }}/dist/js/chart/highcharts.js"></script>
  <script src="{{ asset('assets/packages/core/main.js') }}"></script>
  <script src="{{ asset('assets/packages/interaction/main.js') }}"></script>
  <script src="{{ asset('assets/packages/daygrid/main.js') }}"></script>

  <script type="text/javascript">
    Highcharts.chart('referensi', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Referensi'
        },
        xAxis: {
            categories: [
                ''
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! json_encode($referensi) !!}
    });
  </script>

  <script type="text/javascript">
    Highcharts.chart('omset', {
      chart: {
          type: 'column'
      },
      title: {
          text: 'Grafik Omset & Laba'
      },
      xAxis: {
          categories: [
              'Omset & Laba'
          ],
          crosshair: true
      },
      yAxis: {
          min: 0,
          title: {
              text: 'Jumlah'
          }
      },
      tooltip: {
          headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
          pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
              '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
          footerFormat: '</table>',
          shared: true,
          useHTML: true
      },
      plotOptions: {
          column: {
              pointPadding: 0.2,
              borderWidth: 0
          }
      },
      series: [{
          name: 'Omset',
          data: [{!! json_encode($omset->omset) !!}]

      }, {
          name: 'Laba',
          data: [{!! json_encode($labarugi->laba_rugi) !!}]

      }]
    });
  </script>

  <script type="text/javascript">
    Highcharts.chart('khitan_year', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Pasien Dikhitan'
        },
        subtitle: {
            text: {!! json_encode($tahun) !!}
        },
        xAxis: {
            categories: [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            allowDecimals: false,
            title: {
                text: 'Jumlah Pasien'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} Pasien</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! json_encode($grafik_dk) !!}
    });
  </script>

  <script type="text/javascript">
    Highcharts.chart('ggl_khitan_year', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Gagal Khitan'
        },
        subtitle: {
            text: {!! json_encode($tahun) !!}
        },
        xAxis: {
            categories: [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            allowDecimals: false,
            title: {
                text: 'Jumlah Pasien'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f} Pasien</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! json_encode($grafik_gk) !!}
    });
  </script>

   <script type="text/javascript">
    Highcharts.chart('pemasukan', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Pemasukan Cabang'
        },
        subtitle: {
            text: {!! json_encode($tahun) !!}
        },
        xAxis: {
            categories: [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            allowDecimals: false,
            title: {
                text: 'Total'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! json_encode($grafik_pc) !!}
    });
  </script>

  <script type="text/javascript">
    Highcharts.chart('laba_rugi', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Grafik Laba Rugi Cabang'
        },
        subtitle: {
            text: {!! json_encode($tahun) !!}
        },
        xAxis: {
            categories: [
                'Januari',
                'Februari',
                'Maret',
                'April',
                'Mei',
                'Juni',
                'Juli',
                'Agustus',
                'September',
                'Oktober',
                'November',
                'Desember'
            ],
            crosshair: true
        },
        yAxis: {
            allowDecimals: false,
            title: {
                text: 'Total'
            }
        },
        credits: {
            enabled: false
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.0f}</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: {!! json_encode($grafik_lr) !!}
    });
  </script>

  {{-- Kalender Jadwal Khitan Terdekat  --}}
  <script>
        var acara = {!! $kalender !!};
        console.log(acara);
        var calendarEl = document.getElementById('calendar');
        var calendar = new FullCalendar.Calendar(calendarEl, {
          plugins: [ 'interaction', 'dayGrid' ],
          header: {
            left: 'prevYear,prev,next,nextYear today',
            center: 'title',
            right: 'dayGridMonth,dayGridWeek,dayGridDay'
          },
          defaultDate: "{{date('Y-m-d')}}",
          navLinks: true,
              // can click day/week names to navigate views
              editable: true,
              eventLimit: true,
              // allow "more" link when too many events
              events: acara,
            });
        calendar.render();
          // }

          // $(document).ready(function(){
          //   set_kalendar(acara);
          // });
  </script>
@stop
