@extends('component.layout')

@section('extended_css')
<style type="text/css">
  @media (max-width: 992px) {
    .modal-dialog {
      width: 80% !important;
      margin: auto 10% !important;
    }
  }
  @media (max-width: 768px) {
    .modal-dialog {
      width: 90% !important;
      margin: auto 5% !important;
    }
  }
</style>
@stop

@section('content')
<section class="content-header">
  <h1>
    {!! $data['title'] !!}
    <small>{!! $data['smallTitle'] !!}</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    <li><i class="fa fa-balance-scale"></i> Neraca</li>
    <li class="active">{!! $data['title'] !!}</li>
  </ol>
</section>

<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<section class="content">
  <div class="row">
    <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
      <div class="box box-danger main-layer">
        <div class="col-md-12 col-sm-12 col-xs-12 form-inline p-10">

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 p-0">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group p-t-0 p-b-0">
              <div class="input-group-addon">
                <i class="fa fa-building"></i>
              </div>
              <select name="cabang" id="cabang" class="form-control">
                <option value="">.:: Pilih Cabang ::.</option>
                @foreach($cabang as $a)
                  <option value="{{ $a->id_cabang }}">{{ $a->nama_cabang }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 p-0">
              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 input-group p-t-0 p-b-0">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input type="text" name='month_search' autocomplete='off' class="form-control input pull-right"  id='date_search' data-date-format="mm-yyyy" placeholder="Bulan" required='required'>
              </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 p-0" style="margin-left: 10px;">
                <button type="button" class="btn btn-success doFilter" title="Filte"><i class="fa fa-search"></i>&nbsp</button>
                <button type="button" class="btn btn-danger doPrint" title="Cetak"><i class="fa fa-print"></i>&nbsp</button>
            </div>
          </div>

          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 p-0" style="text-align: right; ">
            <div class="form-group">
                <select class="input-sm form-control input-s-sm inline v-middle option-search" id="search-option"></select>
            </div>
            <div class="form-group">
                <input type="text" class="input-sm form-control" placeholder="Search" id="search">
            </div>
          </div>
        </div>

        <div class="clearfix"></div>

        <div class="col-md-12 p-0">

          <div class="table-responsive">
            <table class="table table-striped b-t b-light" id="datagrid"></table>
          </div>

          <footer class="panel-footer">
            <div class="row">
              <div class="col-lg-12">
                <div class="col-lg-4 pull-right" style="font-size:18px;">
                  <label>Total Kredit : </label><span class="m-l-5 PanelKredit" style="color:red;font-weight:bold"></span>
                </div>
                <div class="col-lg-4 pull-right" style="font-size:18px;">
                  <label>Total Debit : </label><span class="m-l-5 panelDebit"></span>
                </div>
              </div>
            </div>
            <div class="row">

              <div class="col-sm-1 hidden-xs">
                <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
              </div>

              <div class="col-sm-6 text-center">
                <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
              </div>

              <div class="col-sm-5 text-right text-center-xs">
                <ul class="pagination pagination-sm m-t-none m-b-none" id="paging"></ul>
              </div>

            </div>
          </footer>
        </div>
        <div class='clearfix'></div>
      </div>
      <div class="other-page"></div>
      <div class="modal-dialog"></div>
    </div>
  </div>
</section>
@stop

@section('extended_js')
  <script type="text/javascript">
    $('#btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formAddNeraca') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    function showData() {
      $("#datagrid").html('');
      $("#search-option").html('');
      var bulan = $('#date_search').val();
      var cabang = $('#cabang').val();
      var datagrid = $("#datagrid").datagrid({
        url                 : "{!! route('datagridNeracaCabang') !!}?bulan="+bulan+"&cabang="+cabang,
        primaryField        : 'id_neraca',
        rowNumber           : true,
        rowCheck            : false,
        searchInputElement  : '#search',
        searchFieldElement  : '#search-option',
        pagingElement       : '#paging',
        optionPagingElement : '#option',
        pageInfoElement     : '#info',
        columns             : [
          {field: 'kode_reff', title: 'Kode', editable: false, sortable: true, width: 100, align: 'center', search: true},
          {field: 'tgl_neraca', title: 'Tgl Transaksi', editable: false, sortable: true, width: 100, align: 'center', search: false},
          {field: 'name', title: 'Nama', editable: false, sortable: true, width: 150, align: 'center', search: true},

          @if(Auth::getUser()->level_user == 1)
          {field: 'nama_cabang', title: 'Klinik', editable: false, sortable: true, width: 150, align: 'center', search: true},
          @endif

          {field: 'informasi', title: 'Informasi', editable: false, sortable: true, width: 250, align: 'center', search: true},
          {field: 'debit_', title: 'Debit', editable: false, sortable: false, width: 150, align: 'right', search: false,
            rowStyler: function(rowData, rowIndex) {
              return debit(rowData, rowIndex);
            }
          },
          {field: 'kredit_', title: 'Kredit', editable: false, sortable: false, width: 150, align: 'right', search: false,
            rowStyler: function(rowData, rowIndex) {
              return kredit(rowData, rowIndex);
            }
          },
          @if(Auth::getUser()->level_user != 4)
          // {field: 'actions', title: 'Aksi', sortable: false, width: 170, align: 'center', search: false,
          //   rowStyler: function(rowData, rowIndex) {
          //     return actions(rowData, rowIndex);
          //   }
          // }
          @endif
        ]
      });
      datagrid.run();
    }

    function cekNilai() {
      var bulan = $('#date_search').val();
      var cabang = $('#cabang').val();
      $.post("{!! route('getTotalNeracaCabang') !!}", {bulan:bulan, cabang:cabang}).done(function(data){
                if(data.status == 'success'){
          var bilanganDebit = data.data.debit;
                var reverseDebit = bilanganDebit.toString().split('').reverse().join(''),
                ribuanDebit     = reverseDebit.match(/\d{1,3}/g);
                ribuanDebit = ribuanDebit.join('.').split('').reverse().join('');
          var debit = 'Rp. '+ribuanDebit;

          var bilanganKredit = data.data.kredit;
                var reverseKredit = bilanganKredit.toString().split('').reverse().join(''),
                ribuanKredit    = reverseKredit.match(/\d{1,3}/g);
                ribuanKredit    = ribuanKredit.join('.').split('').reverse().join('');
          var kredit = 'Rp. '+ribuanKredit;
          $('.panelDebit').html(debit);
          $('.PanelKredit').html(kredit);
                }
            });
    }

    $(document).ready(function() {
      showData();
      cekNilai();
    });
    
    $('.doFilter').click(function(){
      showData();
      cekNilai();
    });

    $('.doPrint').click(function() {
      var bulan = $('#date_search').val();
      var cabang = $('#cabang').val();
      window.open("{{ route('reportNeracaCabang') }}?bulan="+bulan+"&cabang="+cabang, '_blank');
    });

    function debit(rowData, rowIndex) {
        var bilangan = rowData.debit;
        var reverse = bilangan.toString().split('').reverse().join(''),
        ribuan  = reverse.match(/\d{1,3}/g);
        ribuan  = ribuan.join('.').split('').reverse().join('');
        return 'Rp. '+ribuan
    }

    function kredit(rowData, rowIndex) {
        var bilangan = rowData.kredit;
        var reverse = bilangan.toString().split('').reverse().join(''),
        ribuan  = reverse.match(/\d{1,3}/g);
        ribuan  = ribuan.join('.').split('').reverse().join('');
        return 'Rp. '+ribuan
    }

    // function actions(rowData, rowIndex) {
    //   var l = rowData.jenis_akun;
    //   var tag = '';
    //     if (l != 3) {
    //         tag += '<a href="javascript:void(0)" class="btn btn-xs btn-warning m-0 m-r-5" onclick="updated('+rowData+')"><span class="fa fa-pencil"></span> &nbsp Ubah</a>';
    //         tag += '<a href="javascript:void(0)" class="btn btn-xs btn-danger m-0" onclick="deleted('+rowData+')"><span class="fa fa-trash-o"></span> &nbsp Hapus</a>';
    //     }
    //     return tag;
    // }

    function deleted(rowIndex){
        var rowData = datagrid.getRowData(rowIndex);
        swal(
            {
                title: "Apa anda yakin menghapus Data ini?",
                text: "Data akan dihapus dari sistem dan tidak dapat dikembalikan!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Saya yakin!",
                cancelButtonText: "Batal!",
                closeOnConfirm: true
            },
            function(){
                $.post("{!! route('deleteNeraca') !!}", {id_pasien:rowData.id_pasien}).done(function(data){
                    if(data.status == 'success'){
                        datagrid.reload();
                        $('.main-layer').show();
                        $('.loading').hide();
                        swal(data.title, data.message, data.type);
                    }else if(data.status == 'error'){
                        datagrid.reload();
                        swal(data.title, data.message, data.type);
                    }
                });
            }
        );
    }

    $('#date_search').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 4,
        minView: 3,
        forceParse: 0,
      });

  </script>
@stop
