<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Laporan Neraca</title>
    <style media="screen">
      table {
          font-family: arial, sans-serif;
          border-collapse: collapse;
          width: 100%;
      }
      .detail, .detail tbody td,.detail tbody tr, .detail thead tr,.detail thead th{
        /*border :0.5px solid black;*/
        border-spacing: 0.5px;
        /*text-align: center;*/
        padding-top: 5px;
        padding-bottom: 5px;
        font-size: 12px;
      }
      .detail thead tr {
        background-color: royalblue;
        color: #fff;
      }
      .detail tbody td{
        padding : 5px;
        /*text-align: center;*/
      }
      .detail tbody tr:nth-child(odd){
        background: #ebebeb;
      }
      .detail tbody tr:nth-child(even){
        background: #d7d7d7;
      }
      @page { margin: 5px; }
      body{margin:5px;}
      .title, .dept {
        font-size: 18px;
        font-weight: bold;
        width: 100%;
      }
      .dept {
        font-size: 13px !important;
      }
    </style>
  </head>
  <body>
    <table>
      <tr>
        <td width='160px' valign='bottom'>
          <center>
            <img src="{{ url('/')}}/dist/img/logo/cahaya khitan full.png" alt="Cahaya Khitan" width="50%" style="margin-bottom: -20px;">
          </center>
        </td>
        <td style="padding-left:40px;">
          <div class="title">LAPORAN KEUANGAN</div>
          <div>{{ $cabang }}</div>
          <div class="dept">{{ $judul }}</div>
        </td>
      </tr>
    </table>
    <br>
    <table class='detail' style="border-collapse: collapse;">
      <thead>
        <tr>
          <th><center>No</center></th>
          <th><center>Kode</center></th>
          <th><center>Nama</center></th>
          <th><center>Nama Cabang</center></th>
          <th><center>Tanggal</center></th>
          <th style="padding-left:10px;">Informasi</th>
          <th><center>Debit</center></th>
          <th><center>Kredit</center></th>
        </tr>
      </thead>
      <tbody>
        <?php $no = 1; $debit = 0; $kredit = 0; $saldo = 0; ?>
        @if(count($neraca) > 0)
          @for($i = 0; $i < count($neraca); $i++)
            @foreach($neraca[$i] as $nrc)
              <tr>
                <td align='center'>{{ $no++ }}</td>
                <td align='center'>{{ $nrc->kode_reff }}</td>
                <td align="center">{{ $nrc->name }}</td>
                <td align="center">{{ $nrc->nama_cabang }}</td>
                <td align='center'>{{ date('d-m-Y', strtotime($nrc->tgl_neraca)) }}</td>
                <td>{{ $nrc->informasi }}</td>
                
                <?php $debit = $debit + $nrc->debit;  ?>
                <td align='right'>
                  <div style='position:relative'>
                    <span style='position:absolute;left:0px'>Rp. </span>
                  </div>
                  {{ number_format($nrc->debit,0) }}
                </td>

                <?php $kredit = $kredit + $nrc->kredit;  ?>
                <td align='right'>
                  <div style='position:relative'>
                    <span style='position:absolute;left:0px'>Rp. </span>
                  </div>
                  {{ number_format($nrc->kredit,0) }}
                </td>
                <?php $saldo = $saldo + $nrc->debit - $nrc->kredit; ?>
              </tr>
            @endforeach
          @endfor

          <tr style="background-color: royalblue;color: #fff;font-weight:bold;font-size:18px;">
            <td colspan=6 align='right' style="font-size:13px;"> TOTAL</td>
            <td align='right' style="font-size:13px;">
              <div style='position:relative'>
                <span style='position:absolute;left:0px'>Rp. </span>
              </div>
              {{ number_format($debit,0) }}
            </td>
            <td align='right' style="font-size:13px;">
              <div style='position:relative'>
                <span style='position:absolute;left:0px'>Rp. </span>
              </div>
              {{ number_format($kredit,0) }}
            </td>
          </tr>
        @else
          <tr>
            <td colspan="10" align="center"><i>--== Data Tidak Ditemukan ==--</i></td>
          </tr>
        @endif
      </tbody>
    </table>
  </body>
</html>
