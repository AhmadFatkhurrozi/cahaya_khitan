<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3 p-0">
  <div class="box box-success b-t-non" id='panel-add'>
    <h4 class="labelForm formRed">
      <i class="fa fa-plus-square iconLabel" style="margin: 15px 10px;"></i> Tambah Data Neraca
    </h4>
    <hr class="m-t-0">
    <form class="form-add">
      <div class="box-body">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
          <div class="form-group m-b-0">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal<span class="colorRed">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 input-group p-t-0 p-b-0 p-l-15 p-r-15">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              <input type="text" name='tgl_neraca' id="tgl_neraca" autocomplete='off' placeholder='dd-mm-yyyy hh:ii:ss' class="form-control input-sm pull-right" data-date-format="dd-mm-yyyy hh:ii:ss" required='required'>
            </div>
          </div>
          <div class='clearfix p-b-10'></div>
          <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis<span class="colorRed">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
               <div class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default btn-sm" id="choose_debit">
                        <input type="radio" name="jenis" value="debit">DEBIT
                    </label>
                    <label class="btn btn-default btn-sm" id="choose_kredit">
                        <input type="radio" name="jenis" value="kredit">KREDIT
                    </label>
                </div>
            </div>
          </div>

          <div class='clearfix p-b-10'></div>
          <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Kode Neraca<span class="colorRed">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-10 col-xs-10">
              <select name="kode_reff" id="chooseCode" required="required" class="form-control customInput col-md-7 col-xs-12">
                <option value='' disabled selected> .:: Pilih Kode ::. </option>
                @foreach($akun as $a)
                    <option value="{{$a->no_reff }}"> {{ $a->no_reff.' '.$a->nama_reff.' ('.$a->keterangan.')'}}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class='clearfix p-b-10'></div>
          <div class="form-group">
            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Informasi<span class="colorRed">*</span></label>
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
              <input type="text" name="informasi" id="informasi" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12">
            </div>
          </div>

          <span id="panel-debit" style="display:none">
            <div class='clearfix p-b-10'></div>
            <div class="form-group">
              <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Debit<span class="colorRed">*</span></label>
              <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="text" name="debit" min="0" id="debit" autocomplete='off' value="0" class="form-control input-sm customInput col-md-7 col-xs-12" required="required">
              </div>
            </div>
          </span>

          <span id="panel-kredit" style="display:none">
            <div class='clearfix p-b-10'></div>
            <div class="form-group">
              <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Kredit<span class="colorRed">*</span></label>
              <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="text" name="kredit" min="0" id="kredit" autocomplete='off' value="0" class="form-control input-sm customInput col-md-7 col-xs-12" required="required">
              </div>
            </div>
          </span>

          <div class='clearfix p-b-10'></div>
        </div>
      </div>
      <div class="box-footer">
        <button type="submit" class="btn btn-primary pull-right btn-submit m-l-15">Simpan <span class="fa fa-save"></span></button>
        <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
    var onLoad = (function() {
        $('#panel-add').animateCss('bounceInUp');
    })();

    $('.btn-cancel').click(function(e){
        e.preventDefault();
        $('#panel-add').animateCss('bounceOutDown');
        $('.other-page').fadeOut(function(){
            $('.other-page').empty();
            $('.main-layer').fadeIn();
        });
    });

    $('#choose_debit').click(function(){
        $('#panel-debit').show();
        $('#panel-kredit').hide();
    });

    $('#choose_kredit').click(function(){
        $('#panel-debit').hide();
        $('#panel-kredit').show();
    });

    $(function() {
        $('#debit').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });
    $(function() {
        $('#kredit').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });

    $('#tgl_neraca').datetimepicker({
        weekStart: 2,
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
        startView: 2,
        minView: 0,
        forceParse: 0,
      });


    $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-add')[0]);
    $.ajax({
      url: "{{ route('addNeraca') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-add').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          location.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>