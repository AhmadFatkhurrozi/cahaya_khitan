<div class="box box-primary other-page">
    <div class="col-md-6 col-sm-6 col-xs-12 form-inline p-5">
        <div class="form-group">
            <span><i class="fa fa-calendar"></i> Bulan {{ $bulan_ini }}</span>
        </div>
    </div>

    <div class="col-md-6 col-sm-6 col-xs-12 form-inline panelSearch">
        <button type="button" class="btn btn-sm btn-success btn-cancel"> &nbsp Kembali </button>
    </div>
    <div class='clearfix'></div>

    <div class="col-md-12 p-0">

        <div class="table-responsive" style="margin: 5px 20px;">
            <table class="table table-striped table-hover b-t b-light" border="0">
                <tr  class="header">
                    <th colspan="3" style="font-size: 18px;">Pemasukan <span class="btn btn-success btn-xs pull-right" style="font-size: 18px; font-weight: bold; padding: 0px 5px;"> - </span>
                    </th>
                </tr>
                @php $jumlah = 0; @endphp
                @foreach($pemasukan as $a)
                @php $jumlah = $jumlah+$a->debit; @endphp
                <tr>
                    <td>{{ $a->tgl_neraca }}</td>
                    <td>{{ $a->informasi }}</td>
                    <td align="right">@currency($a->debit)</td>
                </tr>
                @endforeach

                <tr>
                    <td colspan="2">Total</td>
                    <td align="right" style="font-weight: bold;">@currency($jumlah)</td>
                </tr>

                <tr  class="header">
                    <th colspan="3" style="font-size: 18px;">Pengeluaran <span class="btn btn-success btn-xs pull-right" style="font-size: 18px; font-weight: bold; padding: 0px 5px;"> - </span>
                    </th>
                </tr>
                <tr>
                    <td colspan="2">Pengadaan Mainan</td>
                    <td align="right"> @currency($mainan->total) </td>
                </tr>
                <tr>
                    <td colspan="2">Pengadaan BHP/ Alkes</td>
                    <td align="right"> @currency($bhp->total) </td>
                </tr>
                <tr>
                    <td colspan="2">Pengadaan Obat</td>
                    <td align="right"> @currency($obat->total) </td>
                </tr>
                <tr>
                    <td colspan="2">Beban Gaji</td>
                    <td align="right">
                        @if($gaji->total == 0)
                        <i><small>belum ada pengeluaran</small></i>
                        @else
                        @currency($gaji->total) 
                        @endif
                    </td>
                </tr>
                <tr>
                    <td colspan="2">Beban Fee</td>
                    <td align="right">
                        @if($fee->total == 0)
                        <i><small>belum ada pengeluaran</small></i>
                        @else
                        @currency($fee->total) 
                        @endif
                    </td>
                </tr>
                @php 
                $kredit = $mainan->total + $bhp->total + $obat->total + $gaji->total + $fee->total; 
                $bagihasil = $jumlah - $kredit;
                @endphp
                <tr>
                    <td colspan="2">Total</td>
                    <td align="right" style="font-weight: bold;">@currency($kredit)</td>
                </tr>

                <tr  class="header">
                    <th colspan="3" style="font-size: 18px;">Cahaya Khitan <span class="btn btn-success btn-xs pull-right" style="font-size: 18px; font-weight: bold; padding: 0px 5px;"> - </span>
                    </th>
                </tr>
                <tr>
                    <td colspan="2">Total</td>
                    <td align="right" style="font-weight: bold;">@currency($bagihasil)</td>
                </tr>
            </table>
        </div>
        <footer class="panel-footer">
            <div class="row text-center">
                <div class="col-lg-6" style="font-size:16px;">
                    <label>Dr. Inensa Khoirul Harap : <span class="badge badge-primary"> @if($bagihasil > 0) @currency($bagihasil/2) @else @currency(0) @endif </span></label><span class="m-l-5" style="color:red;font-weight:bold"></span>
                </div>
                <div class="col-lg-6" style="font-size:16px;">
                    <label>Andi Puji Kristanto S.Kep.Ners, CH, CHt : <span class="badge badge-primary"> @if($bagihasil > 0) @currency($bagihasil/2) @else @currency(0) </span>@endif </label><span class="m-l-5" style="font-weight:bold"></span>
                </div>
            </div>
        </footer>
    </div>
    <div class='clearfix'></div>
</div>

<script type="text/javascript">

function tampilkan(){
    var month = $('#searchMonth').val();
    var year = $('#searchYear').val();
    $('.loading').show();
    $('.main-layer').hide();
    $.post("{!! route('bagihasil_per') !!}",{month:month, year:year}).done(function(data){
        if(data.status == 'success'){
            $('.loading').hide();
            $('.other-page').html(data.content).fadeIn();
        } else {
            $('.main-layer').show();
        }
    });
}

$('.header').click(function(){
   $(this).find('span').text(function(_, value){return value=='+'?'-':'+'});
    $(this).nextUntil('tr.header').slideToggle(100, function(){
    });
});

$('.btn-cancel').click(function(e){
    e.preventDefault();
      $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('#main-layer').fadeIn();
    });
  });
</script>
