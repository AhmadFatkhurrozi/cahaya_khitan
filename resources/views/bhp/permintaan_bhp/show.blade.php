 <div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
    <i class="fa fa-plus-square iconLabel m-r-15"></i> Details Permintaan Obats
  </h4>
  <hr class="m-t-0">

<div class="row">
    <div class="col-md-12 float-right">
        <button type="button" class="btn btn-warning btn-cancel btn-sm pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>   
</div>
  <form class="form-save">
    <div class="box-body">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
                <?php   
                    $no =1;
                 ?>
                @foreach ($permin as $key)              
              <table class="table table-striped">
                  <tbody>
                      <tr>
                        <th width="150">Klinik</th>
                        <td>{{ $key->nama_cabang }}</td>
                      </tr>
                      <tr>
                        <th width="150">Tanggal Permintaan</th>
                        <td>{{ $key->created_at->format('d-m-Y') }}</td>
                      </tr>                                                                                        
                  </tbody>
                </table>
              <div class='clearfix p-b-5'></div>
              <hr style="border: 1px solid DimGray;"></hr>            
            </div>
        </div>
      </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
          <div class="card-body">                 
            <table class="table table-striped b-t b-light table-bordered">
              <thead>
                <tr>
                  <th><center>No</center></th>
                  <th><center>Nama Alkes/ BHP</center></th>  
                  <th><center>Qty</center></th>
                  <th><center>Stok Yang Tersedia</center></th>
                  <th><center>Keterangan</center></th>
                  <th><center>Status</center></th>
                </tr>
              </thead>
              <tbody>
                <tr align="center">
                    <td>{{ $no++ }}</td>
                    <td>{{ $key->nama_barang }}</td>
                    <td>{{ $key->qty }}</td>
                    <td>{{ $stok->stok_bhp }}</td>
                    <td width="70px">
                        {{ $key->keterangan }}
                    </td>
                    <td align="center">
                        <span>{{ $key->status_permintaan }}</span>
                    </td>
                </tr> 
                @endforeach
              </tbody>
              <div class="clearfix" style="padding-bottom:5px"></div>
            </table>
          </div>
        </div>
      </div>      
    </div>
  </form>
</div>
<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  
</script>
