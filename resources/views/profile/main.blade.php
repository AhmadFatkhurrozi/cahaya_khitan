@extends('component.layout')

@section('extended_css')
  <style>
    .table-profile{
      margin: 20px 10px;
    }

    td{
      padding-right: 20px;
      padding-bottom: 15px;
      font-size: 16px;
    }
  </style>
@stop
<?php use App\Models\Daftar_cabang; ?>
@section('content')
<section class="content-header">
  <h1>
    {{ $data['title'] }}
  </h1>
  <ol class="breadcrumb">
    <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li>My Profile</li>
    <li class="active">{{ $data['title'] }}</li>
  </ol>
</section>

<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
  <div class="loading" align="center" style="display: none;">
    <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
  </div>
</div>

<section class="content">
  <div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="box box-primary main-layer">
        <div class="row">
          <div class="col-md-12">
            <div class="row table-profile">
              <div class="col-md-3 text-center">
                @if (Auth::getUser()->photo != null && is_file(Auth::getUser()->photo))
                    <img src="{!! url(Auth::getUser()->photo) !!}" class="user-image mr-4 mt-2" width="200px;" alt="User Image">
                @else
                    <img _ngcontent-rlb-c22="" class="align-self-start mr-4 mt-2" src="https://ui-avatars.com/api?name={{ Auth::getUser()->name }}&amp;background=4a69b2&amp;color=ffffff&amp;size=200" alt="{{ Auth::getUser()->name }}">
                @endif
              </div>
              <div class="col-md-9 panel-profile">
                <table>
                  <tr>
                    <td><b>Username</b></td>
                    <td>{{ Auth::getUser()->username }}</td>
                  </tr>
                  <tr>
                    <td><b>Email</b></td>
                    <td>{{ Auth::getUser()->email }}</td>
                  </tr>
                  <tr>
                    <td><b>Password</b></td>
                    <td><b>*****</b></td>
                  </tr>
                  <tr>
                    <td><b>Nama</b></td>
                    <td>{{ Auth::getUser()->name }}</td>
                  </tr>
                  <tr>
                    <td><b>Jenis Kelamin</b></td>
                    <td>{{ Auth::getUser()->gender }}</td>
                  </tr>
                  <tr>
                    <td><b>Alamat</b></td>
                    <td>{{ Auth::getUser()->address }}</td>
                  </tr>
                  <tr>
                    <td><b>No. Telp</b></td>
                    <td>{{ Auth::getUser()->phone }}</td>
                  </tr>

                  @if(Auth::getUser()->level_user != 1)
                  <tr>
                    <td><b>Cabang</b></td>
                    <td>{{ Daftar_cabang::where('id_cabang', Auth::getUser()->cabang_id)->first('nama_cabang')->nama_cabang }}</td>
                  </tr>
                  @endif

                  <tr>
                    <td><b></b></td>
                    <td><button class="btn btn-sm btn-success" id="edit">Edit Profile</button></td>
                  </tr>

                </table>
              </div>

              <div class="col-md-9 panel-edit" style="display: none;">
                <form method="POST" action="{{ route('update_profile') }}">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{ Auth::getUser()->id }}">
                    <table>
                      <tr>
                        <td><b>Username</b></td>
                        <td width="400px">{{ Auth::getUser()->username }}</td>
                      </tr>
                      <tr>
                        <td><b>Email</b></td>
                        <td><input type="text" class="form-control" name="email" value="{{ Auth::getUser()->email }}"></td>
                      </tr>
                      <tr>
                        <td><b>Password</b></td>
                        <td><input type="password" class="form-control" name="password" placeholder="kosongi jika tidak ingin mengubah password"> </td>
                      </tr>
                      <tr>
                        <td><b>Nama</b></td>
                        <td><input type="text" class="form-control" name="nama" value="{{ Auth::getUser()->name }}"></td>
                      </tr>
                      <tr>
                        <td><b>Jenis Kelamin</b></td>
                        <td>{{ Auth::getUser()->gender }}</td>
                      </tr>
                      <tr>
                        <td><b>Alamat</b></td>
                        <td><textarea class="form-control" name="alamat">{{ Auth::getUser()->address }}</textarea></td>
                      </tr>
                      <tr>
                        <td><b>No. Telp</b></td>
                        <td>
                            <input type="text" class="form-control" name="telp" value="{{ Auth::getUser()->phone }}">
                        </td>
                      </tr>

                      @if(Auth::getUser()->level_user != 1)
                      <tr>
                        <td><b>Cabang</b></td>
                        <td>{{ Daftar_cabang::where('id_cabang', Auth::getUser()->cabang_id)->first('nama_cabang')->nama_cabang }}</td>
                      </tr>
                      @endif

                      <tr>
                        <td><b></b></td>
                        <td>
                          <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-refresh"></i> Perbarui</button>
                          <a href="#" class="btn btn-sm btn-success" id="kembali"> Kembali</a>
                        </td>
                      </tr>

                    </table>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="other-page"></div>
      <div class="modal-dialog"></div>
    </div>
  </div>
</section>
@stop

@section('extended_js')
  <script type="text/javascript">
    $('button#edit').click(function(){
        $('.panel-edit').show();
        $('.panel-profile').hide();
    });

    $('#kembali').click(function(){
        $('.panel-edit').hide();
        $('.panel-profile').show();
    });
  </script>
@stop
