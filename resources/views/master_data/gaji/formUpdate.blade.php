<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-plus-square iconLabel m-r-15"></i> Tambah Data Gaji
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tingkat Jabatan<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="hidden" name="id_gaji" value="{{ $data->id_gaji }}">
                                <input type="text" class="form-control input-sm customInput col-md-7 col-xs-12" name="jabatan" value="@if($data->users_level == 1) Admin @elseif($data->users_level == 2) Owner @elseif($data->users_level == 3) Kepala Cabang @elseif($data->users_level == 4) Operator Cabang @elseif($data->users_level == 5 && $data->position_id == 1) Bendahara Pusat @elseif($data->users_level == 5 && $data->position_id == 2) Bendahara Cabang  @elseif($data->users_level == 6 && $data->position_id == 1) Marketing Pusat @elseif($data->users_level == 6 && $data->position_id == 2) Marketing Cabang @endif" readonly="readonly">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nominal Gaji<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="nominal_gaji" value="{{ $data->nominal_gaji }}" class="form-control input-sm customInput col-md-7 col-xs-12" id="harga">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Perbarui <span class="fa fa-refresh"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>

    </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });


    $(function() {
        $('#harga').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });

 $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('updateGaji') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          location.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });


</script>
