<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
    <i class="fa fa-plus-square iconLabel m-r-15"></i> Tambah Data Gaji
  </h4>
  <hr class="m-t-0">

  <form class="form-save">
    <div class="box-body">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
          <div class="card-body">
            <div class="form-group">
              <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Posisi<span class="text-red">*</span></label>
              <div class="btn-group">
                <label>
                  <input type="radio" value="Pusat" name="posisi">Pusat 
                </label>
                <label>
                  <input type="radio" value="Cabang" name="posisi">Cabang
                </label>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tingkat Jabatan<span class="text-red">*</span></label>
              <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <select class="form-control select2" name="users_level" id="pilihan">
                  <option disabled selected style="font-weight: bold;"> .:: Pilih Tingkat Jabatan ::. </option>
                  <option value="1">Admin</option>
                  <option value="2">Owner</option>
                  <option value="3">Kepala Cabang</option>
                  <option value="4">Operator</option>
                  <option value="5">Bendahara</option>
                  <option value="6">Marketing</option>
                </select>
              </div>
            </div>
            <div class='clearfix p-b-5'></div>
            <div class="form-group">
              <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nominal Gaji<span class="text-red">*</span></label>
              <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <input type="text" name="nominal_gaji" class="form-control input-sm customInput col-md-7 col-xs-12" id="harga" />
              </div>
            </div>
            <div class='clearfix p-b-5'></div>
          </div>
        </div>
      </div>
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
      <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>
  </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

    $('#pilihan').chosen();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  $(function() {
        $('#harga').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });

 $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addGaji') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          location.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Data Gagal Disimpan !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });


</script>
