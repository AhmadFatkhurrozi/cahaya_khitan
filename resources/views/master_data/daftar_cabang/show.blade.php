<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-eye iconLabel m-r-15"></i> Data Semua Cabang
    </h4>
    <hr class="m-t-0">
    <form class="form-save">
        <div class="m-l-15">
            <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>
        <div class="box-body" style="font-weight: bold;">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Cabang<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input readonly="" value="{{ $data->nama_cabang }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                            </div>
                        </div>             
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>No Telepon (yang bisa dihubungi)<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input readonly="" value="{{ $data->telepon }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                            </div>
                        </div>                          
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <textarea readonly="" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">{{ $data->alamat }}</textarea>
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Keterangan Cabang<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <textarea readonly="" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">{{ $data->tentang }}</textarea>
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };

  $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-blue',
    radioClass: 'iradio_flat-blue'
  });

</script>
