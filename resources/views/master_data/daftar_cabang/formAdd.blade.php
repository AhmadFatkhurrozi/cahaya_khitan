<div class="box box-primary b-t-non" id='panel-add'>
    <h4 class="labelBlue">
        <i class="fa fa-plus-square iconLabel m-r-15"></i> Daftar Semua Klinik
    </h4>
    <hr class="m-t-0">

    <form class="form-save">
        @if ($data != "")
        <input type="hidden" name="id_cabang" value="{{ $data->id_cabang }}">
        @endif
        <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Posisi Klinik<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <label class="radio-inline">
                                    <input type="radio" name="posisi" value="Pusat">Pusat
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="posisi" value="Cabang">Cabang
                                </label>
                            </div>
                        </div><br><br>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Klinik<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="nama_cabang" value="@if($data != '') {{ $data->nama_cabang }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Telepon<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <input type="text" name="telepon" value="@if($data != '') {{ $data->telepon }} @endif" class="form-control input-sm customInput col-md-7 col-xs-12">
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <textarea name="alamat" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">@if($data != '') {{ $data->alamat }} @endif</textarea>
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                        <div class="form-group">
                            <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Keterangan<span class="text-red">*</span></label>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <textarea name="tentang" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="2">@if($data != '') {{ $data->keterangan }} @endif</textarea>
                            </div>
                        </div>
                        <div class='clearfix p-b-5'></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="box-footer">
            <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
            <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
        </div>

    </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

 $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addDaftarcabang') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });


</script>
