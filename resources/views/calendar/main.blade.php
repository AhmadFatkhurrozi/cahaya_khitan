@extends('component.layout')

@section('extended_css')
<style media="screen">
  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
</style>
<link href="{{ asset('assets/packages/core/main.css') }}" rel="stylesheet" />
<link href="{{ asset('assets/packages/daygrid/main.css') }}" rel="stylesheet" />
@stop

@section('content')
<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Calendar</li>
        <li class="active">{{ $data['title'] }}</li>
    </ol>
</section>

<!-- Main Loading -->
<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary main-layer">
                <div class="col-md-12 p-0">
                    <div id='calendar'></div>
                </div>
                <div class='clearfix'></div>
            </div>
            <div class="other-page"></div>
            <div class="modal-dialog"></div>
        </div>
    </div>
</section>
@stop

@section('extended_js')
<script src="{{ asset('assets/packages/core/main.js') }}"></script>
<script src="{{ asset('assets/packages/interaction/main.js') }}"></script>
<script src="{{ asset('assets/packages/daygrid/main.js') }}"></script>
<script>
  // {
  //     title: 'All Day Event',
  //     start: '2019-08-01'
  //   },
  // function set_kalendar(acara){
    var acara = {!! $data['pasien'] !!};
    console.log(acara);
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
      plugins: [ 'interaction', 'dayGrid' ],
      header: {
        left: 'prevYear,prev,next,nextYear today',
        center: 'title',
        right: 'dayGridMonth,dayGridWeek,dayGridDay'
      },
      defaultDate: "{{date('Y-m-d')}}",
      navLinks: true,
          // can click day/week names to navigate views
          editable: true,
          eventLimit: true,
          // allow "more" link when too many events
          events: acara,
        });
    calendar.render();
  // }

  // $(document).ready(function(){
  //   set_kalendar(acara);
  // });
</script>
@stop
