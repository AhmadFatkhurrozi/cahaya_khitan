@extends('component.layout')

@section('extended_css')
@stop

@section('content')
<section class="content-header">
    <h1>
        {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li>Obat</li>
        <li class="active">{{ $data['title'] }}</li>
    </ol>
</section>

<div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
        <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
</div>

<section class="content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-primary main-layer">
                <h4 class="labelBlue">
                    <i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Tambah Obat
                </h4>
                <hr class="m-t-0">
                <form action="{{ route('addObatLokal') }}" method="POST" class="form-save">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Kode Obat :<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="kode_obat" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="exp. 'OB001'">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Obat :<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="nama_obat" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan nama obat">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Obat :<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <select name="jenis" class="form-control select2" id="pilihan">
                                        <option disabled="">.:: Pilih Jenis ::.</option>
                                        <option value="Drop">Drop</option>
                                        <option value="Sirup">Sirup</option>
                                        <option value="Tablet">Tablet</option>            
                                    </select>
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga Obat :<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="harga" id="harga" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan harga obat">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Satuan :<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="satuan" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan satuan obat">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                            <div class="form-group">
                                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Stok :<span class="text-red">*</span></label>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <input type="text" name="stok" required="required" autocomplete='off' class="form-control input-sm customInput col-md-7 col-xs-12" placeholder="masukkan stok obat">
                                </div>
                            </div>
                            <div class='clearfix p-b-5'></div>
                        </div>

                    </div>
                    @if($errors->any())
                        <h4>{{$errors->first()}}</h4>
                    @endif
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15" style="margin-right: 15px;">Simpan <span class="fa fa-save"></span></button>
                    </div>
                </form>
            </div>
            <div class="other-page"></div>
            <div class="modal-dialog"></div>
        </div>
    </div>
</section>
@stop

@section('extended_js')
  <script type="text/javascript">
    $(function() {
        $('#harga').maskMoney({prefix:'Rp ', allowNegative: true, thousands:'.', decimal:',', precision:0, affixesStay: false});
    });
  </script>
@stop
