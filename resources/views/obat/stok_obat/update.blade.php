<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
		<i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Update Obat
	</h4>
  <hr class="m-t-0">
  <form class="form-save">
    <div class="box-body">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <input type="hidden" class="form-control" name="id_obat" value="{{ $obat->id_obat }}">
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Kode Obat<span class="text-red">*</span></label>
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<input type="text" name="kode_obat" value="{{ $obat->kode_obat }}" class="form-control input-sm customInput col-md-7 col-xs-12">
					</div>
				</div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Obat<span class="text-red">*</span></label>
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<input type="text" name="nama_obat" value="{{ $obat->nama_obat }}" class="form-control input-sm customInput col-md-7 col-xs-12">
					</div>
				</div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Jenis Obat<span class="text-red">*</span></label>
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<input type="text" name="jenis" value="{{ $obat->jenis }}" class="form-control input-sm customInput col-md-7 col-xs-12">
					</div>
				</div>
				<div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>jumlah<span class="text-red">*</span></label>
					<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
						<input type="text" name="jumlah" value="{{ $obat->jumlah }}" class="form-control input-sm customInput col-md-7 col-xs-12">
					</div>
				</div>
				<div class='clearfix p-b-5'></div>
      </div>

       <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 p-0">
        <input type="hidden" class="form-control" name="id_obat" value="{{ $obat->id_obat }}">
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Harga<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input type="text" name="harga" value="{{ $obat->harga }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Satuan<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input type="text" name="satuan" value="{{ $obat->satuan }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Stok<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input type="text" name="stok" value="{{ $obat->stok }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
        <div class="form-group">
          <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Tanggal Exp<span class="text-red">*</span></label>
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <input type="text" name="tanggal_exp" value="{{ $obat->tanggal_exp }}" class="form-control input-sm customInput col-md-7 col-xs-12">
          </div>
        </div>
        <div class='clearfix p-b-5'></div>
      </div>

    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
      <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>
  </form>
</div>

<script type="text/javascript">
  // var onLoad = (function() {
  //   $('#panel-add').animateCss('bounceInUp');
  //   disabledBtn();
  // })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

  function loadFilePhoto(event) {
    var image = URL.createObjectURL(event.target.files[0]);
    $('#preview-photo').fadeOut(function(){
      $(this).attr('src', image).fadeIn().css({
        '-webkit-animation' : 'showSlowlyElement 700ms',
        'animation'         : 'showSlowlyElement 700ms'
      });
    });
  };



  $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('doupdateObat') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>
