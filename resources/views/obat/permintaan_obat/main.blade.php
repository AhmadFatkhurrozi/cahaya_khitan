@extends('component.layout')

@section('extended_css')
@stop

@section('content')
  <section class="content-header">
    <h1>
      {{ $data['title'] }}
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
      <li>Data Permintaan Obat</li>
      <li class="active">{{ $data['title'] }}</li>
    </ol>
  </section>

  <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
    <div class="loading" align="center" style="display: none;">
      <img src="{!! url('dist/img/loading.gif') !!}" width="60%">
    </div>
  </div>

  <section class="content">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="box box-primary main-layer">
         <div class="col-md-4 col-sm-4 col-xs-12 form-inline main-layer p-5">
            @if(Auth::getUser()->level_user != 1)
            <button type="button" class="btn btn-sm btn-primary btn-add">
              <span class="fa fa-plus"></span> &nbsp Buat Permintaan Obat
            </button>
            @endif
          </div>    

          <div class="col-md-12 col-sm-12 col-xs-12 form-inline main-layer panelSearch">
            <div class="form-group">
              <select class="input-sm form-control input-s-sm inline v-middle option-search" id="search-option"></select>
            </div>
            <div class="form-group">
              <input type="text" class="input-sm form-control" placeholder="Search" id="search">
            </div>
          </div>
          <div class='clearfix'></div>
          <div class="col-md-12 p-0">

            <div class="table-responsive">
              <table class="table table-striped b-t b-light" id="datagrid"></table>
            </div>
            <footer class="panel-footer">
              <div class="row">

                <div class="col-sm-1 hidden-xs">
                  <select class="input-sm form-control input-s-sm inline v-middle option-page" id="option"></select>
                </div>

                <div class="col-sm-6 text-center">
                  <small class="text-muted inline m-t-sm m-b-sm" id="info"></small>
                </div>

                <div class="col-sm-5 text-right text-center-xs">
                  <ul class="pagination pagination-sm m-t-none m-b-none" id="paging"></ul>
                </div>
              </div>
            </footer>
          </div>
          <div class='clearfix'></div>
        </div>
        <div class="other-page"></div>
        <div class="modal-dialog"></div>
      </div>
    </div>
  </section>
@stop

@section('extended_js')
  <script type="text/javascript">
    var datagrid = $("#datagrid").datagrid({
      url                   : "{!! route('datagridPermintaan') !!}",
      primaryField          : 'id_permintaan_obat',
      rowNumber             : true,
      rowCheck              : false,
      searchInputElement    : '#search',
      searchFieldElement    : '#search-option',
      pagingElement         : '#paging',
      optionPagingElement   : '#option',
      pageInfoElement       : '#info',
      columns               : [
        {field: 'nama_obat', title: 'Nama Obat', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'harga_obat', title: 'Harga', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'qty', title: 'Qty', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'hargaRe', title: 'Total Bayar', editable: false, sortable: false, width: 200, align: 'left', search: false,
          rowStyler: function(rowData, rowIndex) {
            return hargaRe(rowData, rowIndex);
          }
        },
        @if(Auth::getUser()->level_user == 1)
        {field: 'nama_cabang', title: 'Nama Cabang', editable: false, sortable: true, width: 200, align: 'left', search: true},
        @endif
        {field: 'keterangan', title: 'Keterangan', editable: false, sortable: true, width: 200, align: 'left', search: true},
        {field: 'created_at', title: 'Tanggal', editable: false, sortable: true, width: 100, align: 'left', search: true},
        {field: 'status', title: 'Status', editable: false, sortable: true, width: 200, align: 'center', search: true,
          rowStyler: function(rowData, rowIndex) {
            return status(rowData, rowIndex);
          }
        },
        {field: 'menu', title: 'Action', sortable: false, width: 50, align: 'center', search: false,
          rowStyler: function(rowData, rowIndex) {
            return menu(rowData, rowIndex);
          }
        }
      ]
    });

    $(document).ready(function() {
      datagrid.run();
    });

    function menu(rowData, rowIndex) {
      var status = datagrid.getRowData(rowIndex).status_permintaan;
      var menu = '';

      if (status == 'menunggu') {
      menu =
        '<div class="btn-group" style="box-shadow:none">' +
        '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-bars"></span></a>' +
        '<ul class="dropdown-menu pull-right">' +
        '<li onclick="detail('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-eye"></i>Details</a></li>' +

        @if (Auth::getUser()->level_user == 1 || (Auth::getUser()->level_user == 5 && Auth::getUser()->position == 'Pusat'))
        '<li onclick="validasi('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-check"></i> Validasi</a></li>' +
        '<li onclick="tolak('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-times"></i> Tolak</a></li>' +
        @endif

        '<li onclick="deleted('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-trash-o"></i>Delete</a></li>' +
        '</ul>' +
        '</div>';
       
       }else{
        menu =
        '<div class="btn-group" style="box-shadow:none">' +
        '<a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-bars"></span></a>' +
        '<ul class="dropdown-menu pull-right">' +
        '<li onclick="detail('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-eye"></i>Details</a></li>' +
        '<li onclick="deleted('+rowIndex+')"><a href="javascript:void(0);"><i class="fa fa-trash-o"></i>Delete</a></li>' +
        '</ul>' +
        '</div>';
       }
      return menu;
    }

     $('.btn-add').click(function(){
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('formAddPermintaan') !!}").done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    });

    function detail(rowIndex){
      var rowData = datagrid.getRowData(rowIndex);
      $('.loading').show();
      $('.main-layer').hide();
      $.post("{!! route('showPermintaan') !!}",{id_permintaan_obat:rowData.id_permintaan_obat, obat_id:rowData.obat_id}).done(function(data){
        if(data.status == 'success'){
          $('.loading').hide();
          $('.other-page').html(data.content).fadeIn();
        } else {
          $('.main-layer').show();
        }
      });
    }

    function validasi(rowIndex){
        var rowData = datagrid.getRowData(rowIndex);
        swal(
            {
                title: "Validasi Permintaan ?",
                text: "Data permintaan akan di validasi!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Saya yakin!",
                cancelButtonText: "Batal!",
                closeOnConfirm: true
            },
            function(){
                $.post("{!! route('val_permin_obat') !!}",{id_permintaan_obat:rowData.id_permintaan_obat, users_id:rowData.users_id, nama_obat:rowData.nama_obat, obat_id:rowData.obat_id, harga_obat:rowData.harga_obat, qty:rowData.qty, total:rowData.harga_obat*rowData.qty, cabang_id:rowData.cabang_id, kode_obat:rowData.kode_obat, nama_cabang:rowData.nama_cabang}).done(function(data){
                    if(data.status == 'success'){
                        $('.main-layer').show();
                        $('.loading').hide();
                        swal("Success!", "Data Berhasil Divalidasi !", "success");
                        datagrid.reload();
                    }else if(data.status == 'error'){
                        swal(data.title, data.message, data.type);
                        datagrid.reload();
                    }
                });
            }
        );
    }

    function tolak(rowIndex){
        var rowData = datagrid.getRowData(rowIndex);
        swal(
            {
                title: "Tolak Permintaan ?",
                text: "Data permintaan akan di tolak!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Saya yakin!",
                cancelButtonText: "Batal!",
                closeOnConfirm: true
            },
            function(){
                $.post("{!! route('rejectPerminObat') !!}",{id_permintaan_obat:rowData.id_permintaan_obat}).done(function(data){
                    if(data.status == 'success'){
                        $('.main-layer').show();
                        $('.loading').hide();
                        swal("Success!", "Data Berhasil Ditolak !", "success");
                        datagrid.reload();
                    }else if(data.status == 'error'){
                        swal(data.title, data.message, data.type);
                        datagrid.reload();
                    }
                });
            }
        );
    }

    function konfirmasi_pesanan(rowIndex){
        var rowData = datagrid.getRowData(rowIndex);
        swal(
            {
                title: "Konfirmasi Pesanan ?",
                text: "Data permintaan sudah diterima!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Saya yakin!",
                cancelButtonText: "Batal!",
                closeOnConfirm: true
            },
            function(){
                $.post("{!! route('konfirmasi_pesanan_obat') !!}",{id_permintaan_obat:rowData.id_permintaan_obat, users_id:rowData.users_id, nama_obat:rowData.nama_obat, obat_id:rowData.obat_id, harga_obat:rowData.harga_obat, qty:rowData.qty, total:rowData.harga_obat*rowData.qty, cabang_id:rowData.cabang_id, kode_obat:rowData.kode_obat, nama_cabang:rowData.nama_cabang, jenis_obat:rowData.jenis_obat, satuan_obat:rowData.satuan_obat}).done(function(data){
                    if(data.status == 'success'){
                        $('.main-layer').show();
                        $('.loading').hide();
                        swal("Success!", "Ditambahkan ke stok obat !", "success");
                        datagrid.reload();
                    }else if(data.status == 'error'){
                        swal(data.title, data.message, data.type);
                        datagrid.reload();
                    }
                });
            }
        );
    }

    function deleted(rowIndex){
    var rowData = datagrid.getRowData(rowIndex);
    swal(
      {
        title: "Apa anda yakin menghapus Data ini?",
        text: "Data akan dihapus dari sistem dan tidak dapat dikembalikan!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Saya yakin!",
        cancelButtonText: "Batal!",
        closeOnConfirm: true
      },
      function(){
        $.post("{!! route('deletePermintaan') !!}", {id_permintaan_obat:rowData.id_permintaan_obat}).done(function(data){
          if(data.status == 'success'){
            datagrid.reload();
            $('.main-layer').show();
            $('.loading').hide();
            swal(data.title, data.message, data.type);
          }else if(data.status == 'error'){
            datagrid.reload();
            swal(data.title, data.message, data.type);
          }
        });
      }
    );
  }

    function status(rowData, rowIndex) {
      var status = datagrid.getRowData(rowIndex).status_permintaan;
      var tag = '';
      if (status == 'selesai') {

        @if(Auth::getUser()->level_user == 1)
        tag = '<a href="javascript:void(0)" onclick="konfirmasi('+rowIndex+')"  class="btn btn-xs btn-warning"><i class="fa fa-refresh"></i>Selesai <span class="text-red">*dalam Proses</span></a>';
        @else
        tag = '<a href="javascript:void(0)" onclick="konfirmasi('+rowIndex+')"  class="btn btn-xs btn-outline-primary" style="margin-right: 5px;">Selesai</a>'+
        '<a href="javascript:void(0)" onclick="konfirmasi_pesanan('+rowIndex+')"  class="btn btn-xs btn-success"><i class="fa fa-check"> </i>konfirmasi pesanan</a>';
        @endif

      }else if (status == 'tolak') {
        tag = '<a href="javascript:void(0)" onclick="konfirmasi('+rowIndex+')"  class="btn btn-xs btn-danger"><i class="fa fa-close"></i> Ditolak</a>';
      }else if (status == 'menunggu'){
        
        @if(Auth::getUser()->level_user == 1)
        tag = '<a href="javascript:void(0)" onclick="validasi('+rowIndex+')" class="btn btn-xs btn-warning"><i class="fa fa-clock-o"></i> Menunggu</a>';
        @else
        tag = '<a href="javascript:void(0)" class="btn btn-xs btn-outline-warning"><i class="fa fa-clock-o"></i> Menunggu</a>';
        @endif

      }else if (status == 'proses'){
        tag = '<a href="javascript:void(0)" onclick="konfirmasi('+rowIndex+')" class="btn btn-xs btn-warning"><i class="fa fa-check"></i>Dalam Proses</a>';
      }else{
        tag = '<a href="javascript:void(0)" onclick="konfirmasi('+rowIndex+')" class="btn btn-xs btn-primary"><i class="fa fa-check"></i> Selesai *Sudah Diterima</a>';
      }
      return tag;
    }

  $('#tempatR').click(function(){
    swal('uwu');
    $('.panelAlamatRumah').show();
  });

  function hargaRe(rowData, rowIndex) {
    var bilangan = rowData.harga_obat * rowData.qty;
    var reverse = bilangan.toString().split('').reverse().join(''),
    ribuan  = reverse.match(/\d{1,3}/g);
    ribuan  = ribuan.join('.').split('').reverse().join('');
    return 'Rp. '+ribuan
  }

  </script>
@stop
