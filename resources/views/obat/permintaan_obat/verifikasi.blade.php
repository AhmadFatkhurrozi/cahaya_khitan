<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          Konfirmasi PasienPermintaan Pengadaan Obat
        <span style="float:right"><a data-dismiss="modal" onclick="closeModal()">Close</a></span>
      </div>
      <div class="modal-body">
          <div class="m-l-15">
            <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
          </div>
          <div class="box-body">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
              <div class="card">
                  <div class="card-body">              
                    <table class="table table-striped">
                        <tbody>                      
                            <tr>
                              <th width="150">Keterangan singkat</th>
                                <td><input id="keterangan" value="{{$permin->keterangan}}" readonly="" class="form-control"></td>            
                            </tr>
                            <tr>
                              <th width="150">Nama Bendahara</th>
                                <td>: {{$permin->name}}</td>
                            </tr>
                             <tr>
                              <th width="150">Status Permintaan</th>
                                <td>: {{$permin->status_permintaan}}</td>            
                            </tr>
                            <tr>
                              <th width="150">Tanggal Permintaan</th>
                                <td>: {{$permin->tanggal_permintaan}}</td>
                            </tr>                                                                                        
                        </tbody>
                      </table>
                    <div class='clearfix p-b-5'></div>
                    <hr style="border: 1px solid DimGray;"></hr>            
                  </div>
              </div>
            </div>
             <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
              <div class="card">
                <div class="card-body">                 
                  <table class="table table-striped b-t b-light">
                    <thead>
                      <tr>
                        <th align="center">No</th>
                        <th align="center">Nama</th>  
                        <th align="center">Qty</th>
                        <th align="center">Qty Acc</th>
                        <th align="center">Keterangan singkat</th>
                        <th align="center">Konfirmasi</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php 
                          $no =1;
                          $i = 0;
                       ?>
                      @foreach ($data as $key)
                      <tr>
                          <td>{{ $no++ }}</td>
                          <td>{{$key->nama_obat}}</td>
                          <td width="75">{{$key->jumlah_permintaan}}</td>
                          <td width="75">{{$key->jumlah_acc}}</td>
                          <td width="400">{{$key->komentar}}</td>
                          <td><input type="checkbox" id="id_items{{$i}}" value="{{ $key->id_items }}" checked=""><br></td>
                      </tr> 
                      <?php 
                      $i++;
                      ?>
                      @endforeach
                    </tbody>
                    <div class="clearfix" style="padding-bottom:5px"></div>
                  </table>
                </div>
              </div>
            </div>      
          </div>
          <div class="box-footer">
            <button type="submit" class="btn btn-primary pull-right m-l-15" onclick="approve('{{ $permin->id_permintaan }}','selesai')">Konfirmasi <span class="fa fa-save"></span>
            </button>
          </div>
      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $('.mainanYa').click(function(){
    $('.panelMainan').show();
      });

    $('.mainanTidak').click(function(){
    $('.panelMainan').hide();
      });


  $().ready(function() {
    // validate the comment form when it is submitted
    $("#commentForm").validate();
  });
    var onLoad = (function() {
        $('#detail-dialog').find('.modal-dialog').css({
            'width'     : '60%'
        });
        $('#detail-dialog').modal('show');
    })();

    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    });

    function approve(id_permintaan,status_permintaan) {
      // swal(id_permintaan+' '+status_permintaan);
      if (status_permintaan == 'selesai') {
        var ket ='Verifikasi Permintaan Diterima?';
      }

      var arr_item = [];
      <?php 
      $i=0;
      ?>
      @foreach ($data as $key)
      var id_items = $('#id_items{{$i}}:checked').val();
      if(id_items!=null){
        arr_item.push(id_items);
      }
      <?php 
      $i++;
      ?>
      @endforeach
      swal({
       title:"Apakah anda akan "+ket,
       text:"Apakah anda yakin ?",
       type:"warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "Saya yakin!",
       cancelButtonText: "Batal!",
       closeOnConfirm: true
     },
     function(){
       $.post('{{ route('approve_list') }}',{id_permintaan:id_permintaan,status_permintaan:status_permintaan,id_items:arr_item}).done(function(data){
         if(data.status == 'success'){
          if (status_permintaan == 'selesai') {
            var hsl = '<a href="javascript:void(0)" class="btn btn-info"><i class="fa fa-check"><span> Permintaan sudah terverifikasi</span></i></a>';
          }
          $('.panel-btn-waiting'+id_permintaan).html(hsl);
          $('#detail-dialog').modal('hide');
          // $('#detail-dialog').html('');
          // $('.second-modal').html('');

          // filePendukung(data.id_permintaan);
           location.reload();
           swal("Success!", "Data Diterima !", "success");
         }else if(data.status=='fail'){
           // datagrid.reload();
           swal("Maaf!", "Anda bukan pemilik berita ini !", "error");
         }else{
           // datagrid.reload();
           swal("Maaf!", "Berita telah dihapus sebelum ini !", "error");
         }
       });
     });
    }
</script>
