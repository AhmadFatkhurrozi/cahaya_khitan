<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
    <i class="fa fa-plus-square iconLabel m-r-15"></i> Update Permintaan Pengadaan Barang
  </h4>
  <hr class="m-t-0">

  <form class="form-save">
    <div class="m-l-15">
      <button type="button" class="btn btn-warning btn-cancel pull-right m-r-5"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>
    <div class="box-body">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
              <input type="hidden" name="id_permintaan" id="id_permintaan" value="{{ $permin->id_permintaan }}">           
              <table class="table table-striped">
                  <tbody>                      
                      <tr>
                        <th width="150">Keterangan singkat</th>
                          <td><input type="text" name="keterangan" value="{{$permin->keterangan}}" class="form-control input-sm customInput"></td>            
                      </tr>
                      <tr>
                        <th width="150">Nama Bendahara</th>
                          <td>: {{$permin->name}}</td>
                      </tr>
                       <tr>
                        <th width="150">Status Permintaan</th>
                          <td>: {{$permin->status_permintaan}}</td>            
                      </tr>
                      <tr>
                        <th width="150">Tanggal Permintaan</th>
                          <td>: {{$permin->tanggal_permintaan}}</td>
                      </tr>                                                                                        
                  </tbody>
                </table>
              <div class='clearfix p-b-5'></div>
              <hr style="border: 1px solid DimGray;"></hr>            
            </div>
        </div>
      </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
          <div class="card-body">
            <table class="table table-striped b-t b-light">
              <thead>
                <tr>
                  <th align="center">No</th>
                  <th align="center">Nama</th>  
                  <th align="center">Qty</th>
                  <th align="center">Qty Acc</th>
                  <th align="center">Keterangan singkat</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    $no =1;
                 ?>
                @foreach ($data as $key)
                <tr>
                    <td>{{ $no++ }}<input type="hidden" name="id_items[]" id="id_items" value="{{ $key->id_items }}"></td>                     
                    <td>{{$key->nama_obat}}</td>
                    <td width="100">{{$key->jumlah_permintaan}}</td>
                    <td width="100"><input type="text"   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"  name="jumlah_acc[]" value="@if($key->jumlah_acc != '') {{$key->jumlah_acc}} @else input jml @endif" class="form-control input-sm customInput"></td>                    
                    <td width="400"><input type="text" name="komentar[]"  value="@if($key != '') {{$key->komentar}} @endif" class="form-control input-sm customInput"></td>                
                </tr> 
                @endforeach
              </tbody>
              <div class="clearfix" style="padding-bottom:5px"></div>
            </table>
          </div>
        </div>
      </div>      
    </div>

    <div class="box-footer">
      <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
      <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>
    
  </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
  })();

  $('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
    });
  });

   $('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('doupdatePermintaan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
    }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
        });
      } else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
      } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
        }
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
      }
    }).fail(function() {
      swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
    });
  });
</script>
