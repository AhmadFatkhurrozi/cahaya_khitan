<div class="box box-primary b-t-non" id='panel-add'>
  <h4 class="labelBlue">
    <i class="fa fa-plus-square iconLabel m-r-15"></i> Formulir Data Permintaan
  </h4>
  <hr class="m-t-0">

  <form class="form-save">
    <div class="box-body">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
            <div class="card-body">
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Cabang</label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" readonly="" value="{{ $permintaan->nama_cabang }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>             
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Alamat Cabang</label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text"  readonly="" value="{{ $permintaan->alamat }}"  class="form-control input-sm customInput col-md-7 col-xs-12 resultBday">
                </div>
              </div>              
              <div class='clearfix p-b-5'></div>
              <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Telepon</label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" readonly="" value="{{ $permintaan->telepon }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>             
              <div class='clearfix p-b-5'></div>
               <div class="form-group">
                <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Nama Bendahara</label>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                  <input type="text" readonly="" value="{{ $permintaan->name }}" class="form-control input-sm customInput col-md-7 col-xs-12">
                </div>
              </div>
              <div class='clearfix p-b-5'></div>
              <hr style="border: 1px solid DimGray;"></hr>            
            </div>
        </div>
      </div>
       <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 p-0">
        <div class="card">
          <div class="card-body">                 
            <table class="table table-striped b-t b-light table-bordered">
              <thead>
                <tr>
                  <th align="center">Nama</th>
                  <th align="center">Stok</th>  
                  <th align="center">Harga</th>
                  <th align="center">Qty</th>
                  <th align="center">Komentar</th>
                </tr>
              </thead>
                <tr>
                    <td>
                        <select name="obat_id" id="pilihan1" class="form-control" style="width: 100%;" required="required">
                            <option value="">.:: Obat Yang Tersedia ::.</option>
                            @foreach ($obat as $ob)
                            <option value="{{ $ob->id }}" data-stok="{{ $ob->stok_obat }}" data-harga="{{ $ob->harga_obat }}">
                                {{ $ob->nama_obat }} ({{$ob->jenis_obat}})
                            </option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="text" name="stok" class="form-control" id="resultStok" readonly="">
                    </td>
                    <td>
                        <input type="text" name="harga" class="form-control" id="resultHarga" readonly="">
                    </td>
                    <td>
                        <input type="text" min="0" onkeyup="this.value=this.value.replace(/[^0-9]/g,'')" placeholder="Qty" name="jumlah_permintaan" class="form-control" width="50px">
                    </td>
                    <td>
                        <input type="text" placeholder="Keterangan"  name="komentar" class="form-control">
                    </td>
                </tr>             
              <tbody id="hasil">
              </tbody>
              <div class="clearfix" style="padding-bottom:5px"></div>
            </table>       
          </div>
        </div>
      </div>      
    </div>
    <div class="box-footer">
      <button type="submit" class="btn btn-primary btn-submit pull-right m-l-15">Simpan <span class="fa fa-save"></span></button>
      <button type="button" class="btn btn-warning btn-cancel pull-right"><span class="fa fa-chevron-left"></span> Kembali</button>
    </div>

  </form>
</div>

<script type="text/javascript">
  var onLoad = (function() {
    $('#panel-add').animateCss('bounceInUp');
})();

$('.btn-cancel').click(function(e){
    e.preventDefault();
    $('#panel-add').animateCss('bounceOutDown');
    $('.other-page').fadeOut(function(){
      $('.other-page').empty();
      $('.main-layer').fadeIn();
  });
});

$('.btn-submit').click(function(e){
    e.preventDefault();
    $('.btn-submit').html('Please wait...').attr('disabled', true);
    var data  = new FormData($('.form-save')[0]);
    $.ajax({
      url: "{{ route('addPermintaan') }}",
      type: 'POST',
      data: data,
      async: true,
      cache: false,
      contentType: false,
      processData: false
  }).done(function(data){
      $('.form-save').validate(data, 'has-error');
      if(data.status == 'success'){
        swal("Success !", data.message, "success");
        $('.other-page').fadeOut(function(){
          $('.other-page').empty();
          $('.main-layer').fadeIn();
          datagrid.reload();
      });
    }else if(data.code == '400') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
    }else if(data.status == 'error') {
        $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
        swal('Whoops !', data.message, 'warning');
    } else {
        var n = 0;
        for(key in data){
          if (n == 0) {var dt0 = key;}
          n++;
      }
      $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
      swal('Whoops !', 'Kolom '+dt0+' Tidak Boleh Kosong !!', 'error');
  }
}).fail(function() {
  swal("MAAF !","Terjadi Kesalahan, Silahkan Ulangi Kembali !!", "warning");
  $('.btn-submit').html('Simpan <i class="fa fa-save fs-14 m-l-5"></i>').removeAttr('disabled');
});
});
    
</script>

<script>
    $('#pilihan1').on('change', function() {
        var stok = $(this).find(':selected').data('stok');
        var harga = $(this).find(':selected').data('harga');

        $('#resultStok').val(stok);
        $('#resultHarga').val(harga);
    })
</script>
