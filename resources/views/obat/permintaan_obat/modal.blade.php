<div class="modal fade" id="detail-dialog" tabindex="-1" role="dialog" aria-labelledby="product-detail-dialog">
  <div class="modal-dialog modal-lg" >
    <div class="modal-content">
      <div class="modal-header" style="width: 100%">
          Konfirmasi PasienPermintaan Pengadaan Obat
        <span style="float:right"><a data-dismiss="modal" onclick="closeModal()">Close</a></span>
      </div>
      <div class="modal-body">
        <div class='col-lg-12 col-md-12 col-sm-12 col-xs-12'>
          <h4 style="font-weight: bold; color: #4682B4;">Details Permintaan</h4>
          <hr style="  border: 1px solid DimGray;">
            <div class="form-group m-t-0 m-b-25">
              <input type="hidden" name="id_permintaan" id="id_permintaan" value="{{ $permintaan->id_permintaan }}">
              <table class="table">
                <tbody>
                  <tr>
                    <th width="150">Unit Cabang</th>
                      <td>: {{$permintaan->nama_cabang}}</td>            
                  </tr>                      
                  <tr>
                    <th width="150">Keterangan singkat</th>
                      <td>: {{$permintaan->keterangan}}</td>            
                  </tr>
                  <tr>
                    <th width="150">Nama Bendahara</th>
                      <td>: {{$permintaan->name}}</td>
                  </tr>                   
                  <tr>
                    <th width="150">Tanggal Permintaan</th>
                      <td>: {{$permintaan->tanggal_permintaan}}</td>
                  </tr> 

                  @if ($permintaan->status_permintaan == "menunggu")                
                  <tr>
                    <td colspan="2">
                       <div class="form-group">
                        <label class="control-label col-lg-3 col-md-3 col-sm-12 col-xs-12" id='label-input'>Keterangan / Alasan singkat<span class="text-red">*</span></label>
                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                          <textarea name="keterangan" id="keterangan" class="form-control input-sm customInput col-md-7 col-xs-12" autocomplete='off' rows="1">-</textarea>
                          <small style="color: red;">Tambahkan keterangan singkat jika Permintaan pengadaan barang ini tidak Diterima maupun Diterima</small>
                        </div>
                      </div>
                    </td>
                  </tr>
                  @endif

                  <tr>
                      <th>
                          <td>
                            @if ($permintaan->status_permintaan == 'menunggu')
                            <a href="javascript:void(0)" onclick="approve('{{ $permintaan->id_permintaan }}','terima')" class="btn btn-success btn-approve btn-xs" id="btn-approve">
                                <i class="fa fa-check"><span> Terima dan Proses</span></i>
                            </a>
                            <a href="javascript:void(0)" onclick="approve('{{ $permintaan->id_permintaan }}','tolak')" class="btn btn-danger btn-xs">
                                <i class="fa fa-times"><span> Tolak Permintaan</span></i>
                            </a>
                          @endif
                          </td>

                          <td>
                            @if ($permintaan->status_permintaan == 'terima')
                              <button class="btn btn-success btn-xs" id="btn-approve">
                                  <i class="fa fa-check"></i>Permintaan sudah diterima<span class="text-red">*dalam proses</span>
                              </button>                         
                            @endif

                            @if ($permintaan->status_permintaan == 'tolak')
                              <button" class="btn btn-danger btn-xs" id="btn-approve">
                                  <i class="fa fa-check"></i>Permintaan tidak diterima
                              </button>
                            @endif

                             @if ($permintaan->status_permintaan == 'proses')
                              <button" class="btn btn-danger btn-xs" id="btn-approve">
                                  <i class="fa fa-check"></i>Permintaan dalam Proses 
                              </button>
                            @endif 

                             @if ($permintaan->status_permintaan == 'selesai')
                              <button" class="btn btn-danger btn-xs" id="btn-approve">
                                  <i class="fa fa-check"></i>Obat sudah diterima oleh Unit
                              </button>
                            @endif                        
                          </td>
                      </th>
                  </tr>

                </tbody>
              </table>
            </div>
        </div>
      </div>
      <div class="clearfix" style='padding-bottom:20px'></div>
    </div>
  </div>
</div>
<script type="text/javascript">

    $('.mainanYa').click(function(){
    $('.panelMainan').show();
      });

    $('.mainanTidak').click(function(){
    $('.panelMainan').hide();
      });


  $().ready(function() {
    // validate the comment form when it is submitted
    $("#commentForm").validate();
  });
    var onLoad = (function() {
        $('#detail-dialog').find('.modal-dialog').css({
            'width'     : '50%'
        });
        $('#detail-dialog').modal('show');
    })();

    $('#detail-dialog').on('hidden.bs.modal', function () {
        $('.modal-dialog').html('');
    });

    function approve(id_permintaan,status_permintaan) {
      // swal(id_pasien+' '+status_permintaan);
      if (status_permintaan == 'terima') {
        var ket ='Terima Permintaan?';
      }else{
        var ket ='Tolak Permintaan?';
      }
      var keterangan = $('#keterangan').val();
      var mainan = $('#pilihmainan:checked').val();
      swal({
       title:"Apakah anda akan "+ket,
       text:"Apakah anda yakin ?",
       type:"warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "Saya yakin!",
       cancelButtonText: "Batal!",
       closeOnConfirm: true
     },
     function(){
       $.post('{{ route('approve_permintaan') }}',{id_permintaan:id_permintaan,status_permintaan:status_permintaan, keterangan:keterangan}).done(function(data){
         if(data.status == 'success'){
          if (status_permintaan == 'terima') {
            var hsl = '<a href="javascript:void(0)" class="btn btn-info"><i class="fa fa-check"><span> Permintaan sudah diterima</span></i></a>';
          }else{
            var hsl = '<a href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-times"><span> Permintaan tidak Diterima</span></i></a>';
          }
          $('.panel-btn-waiting'+id_permintaan).html(hsl);
          $('#detail-dialog').modal('hide');
          // $('#detail-dialog').html('');
          // $('.second-modal').html('');

          // filePendukung(data.id_pasien);
           location.reload();
           swal("Success!", "Data Diterima !", "success");
         }else if(data.status=='fail'){
           // datagrid.reload();
           swal("Maaf!", "Anda bukan pemilik berita ini !", "error");
         }else{
           // datagrid.reload();
           swal("Maaf!", "Berita telah dihapus sebelum ini !", "error");
         }
       });
     });
    }
</script>
