<?php

Route::get('/reset', 'MailController@index')->name('reset');
Route::post('/email', 'MailController@sendMail')->name('password_email');
Route::get('/reset_password/{id}/{email}', 'MailController@reset')->name('reset_password');
Route::post('/update_password', 'MailController@update')->name('update_password');
// Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password_email');

Route::get('/', function(){ return redirect()->route('dashboard'); });
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/doLogin', 'AuthController@doLogin')->name('doLogin');
Route::get('logout',    'AuthController@logout')->name('logout');

Route::group(['middleware' => 'auth'], function() {
    Route::get('/home', 'DashboardController@main')->name('dashboard');
    Route::post('/getEmail', 'OtherController@getEmail')->name('getEmail');
    Route::post('/getUsername', 'OtherController@getUsername')->name('getUsername');

    Route::group(array('prefix'=>'profile'), function(){
        Route::get('/', 'ProfileController@index')->name('profile');
        Route::post('/update', 'ProfileController@update')->name('update_profile');
        Route::post('/formEditPassword', 'ProfileController@formEditPassword')->name('formEditPassword');
        Route::post('/editPassword', 'ProfileController@editPassword')->name('editPassword');
    });

    Route::group(array('prefix'=>'pasien'), function(){
        Route::get('/',  'PasienController@main')->name('pasien');
        Route::post('/datagrid',  'PasienController@datagrid')->name('datagridPasien');
        Route::post('/formAdd',  'PasienController@formAdd')->name('formAddPasien');
        Route::post('/add',  'PasienController@add')->name('addPasien');
        Route::post('/show',  'PasienController@show')->name('showPasien');
        Route::post('/delete',  'PasienController@delete')->name('deletePasien');
        Route::post('/konfirmasi_pasien','PasienController@konfirmasi_pasien')->name('konfirmasi_pasien');
        Route::post('/approve_khitan','PasienController@approve_khitan')->name('approve_khitan');
        Route::get('/rincian_pasien/{id}',  'PasienController@rincian_pasien')->name('rincian_pasien');
        Route::get('/cetak_struk/{id}', 'PasienController@cetak_struk')->name('cetak_struk');

        Route::post('/formUpdate',  'PasienController@formUpdate')->name('formUpdatePasien');
        Route::post('/update',  'PasienController@addupdate')->name('addUpdate');
    });

    Route::group(array('prefix'=>'calendar'), function(){
        Route::get('/',  'CalendarController@main')->name('calendar');

    });

    Route::group(array('prefix'=>'admin'), function(){
        Route::group(array('prefix'=>'bhp'), function(){
            Route::get('/', 'Admin\AdminBhpController@main')->name('bhpAdmin');
            Route::post('/datagrid',  'Admin\AdminBhpController@datagrid')->name('getJsonBhpAdmin');
            Route::post('/formAdd',  'Admin\AdminBhpController@formAdd')->name('formAddBhpAdmin');
            Route::post('/add',  'Admin\AdminBhpController@add')->name('addBhpAdmin');
            Route::post('/show',  'Admin\AdminBhpController@show')->name('showBhpAdmin');
            Route::post('/delete',  'Admin\AdminBhpController@delete')->name('deleteBhpAdmin');
        });

        Route::group(array('prefix'=>'obat'), function() {
            Route::get('/', 'Admin\AdminObatController@main')->name('obatAdmin');
            Route::post('/datagrid',  'Admin\AdminObatController@datagrid')->name('getJsonObatAdmin');
            Route::post('/formAdd',   'Admin\AdminObatController@formAdd')->name('formAddObatAdmin');
            Route::post('/add',       'Admin\AdminObatController@add')->name('addObatAdmin');
            Route::post('/show',      'Admin\AdminObatController@show')->name('showObatAdmin');
            Route::post('/delete',    'Admin\AdminObatController@delete')->name('deleteObatAdmin');
        });

        Route::group(array('prefix'=>'pengeluaran'), function() {
            Route::get('/', 'Admin\AdminPengeluaranController@main')->name('pengeluaranAdmin');
            Route::post('/detail_exp','Admin\AdminPengeluaranController@detail_exp')->name('detail_exp_admin');
            Route::post('/admin_exp_obat', 'Admin\AdminPengeluaranController@formObat')->name('admin_exp_obat');
            Route::post('addExpObat', 'Admin\AdminPengeluaranController@addExpObat')->name('addExpObat');

            Route::post('/admin_exp_bhp', 'Admin\AdminPengeluaranController@formBhp')->name('admin_exp_bhp');
            Route::post('addExpBhp', 'Admin\AdminPengeluaranController@addExpBhp')->name('addExpBhp');
        });

    });

    Route::group(array('prefix'=>'fee_operator'), function(){
        Route::get('/', 'FeeOperatorController@main')->name('fee_operator');
        Route::post('/per_op', 'FeeOperatorController@perOp')->name('fee_per_operator');
        Route::post('/detail','FeeOperatorController@detail')->name('detail_fee');
        Route::post('/detail_op','FeeOperatorController@detailOp')->name('detail_fee_op');
        Route::post('/search_fee','FeeOperatorController@search')->name('search_fee');


        Route::post('/formAdd',    'FeeOperatorController@formAdd')->name('formAddFeeoperator');
        Route::post('/add',        'FeeOperatorController@add')->name('addFeeoperator');
        Route::post('/show',       'FeeOperatorController@show')->name('showFeeoperator');
        Route::post('/delete',     'FeeOperatorController@delete')->name('deleteFeeoperator');
    });

    Route::group(array('prefix'=>'neraca'), function(){
        Route::get('/', 'NeracaController@main')->name('neraca');
        Route::get('/neraca_cabang', 'NeracaController@neraca_cabang')->name('neraca_cabang');
        Route::post('/datagrid', 'NeracaController@datagrid')->name('datagridNeraca');
        Route::post('/datagrid_cabang', 'NeracaController@datagrid_cabang')->name('datagridNeracaCabang');
        Route::post('/get_total', 'NeracaController@total')->name('getTotalNeraca');
        Route::post('/total_n_cabang', 'NeracaController@totalNeracaCabang')->name('getTotalNeracaCabang');
        Route::post('/formAdd', 'NeracaController@formAdd')->name('formAddNeraca');
        Route::post('/add', 'NeracaController@add')->name('addNeraca');

        Route::post('/formUpdate', 'NeracaController@formUpdate')->name('formUpdateNeraca');
        Route::post('/updateNeraca', 'NeracaController@updateNeraca')->name('updateNeraca');
        
        Route::post('/delete', 'NeracaController@delete')->name('deleteNeraca');

        Route::get('/cetak', 'NeracaController@cetak')->name('reportNeraca');
        Route::get('/cetak_cabang', 'NeracaController@cetak_cabang')->name('reportNeracaCabang');

        Route::get('/bagi_hasil', 'NeracaController@bagi_hasil')->name('bagi_hasil');
        Route::post('/bagi_hasil_per', 'NeracaController@bagi_hasil_per')->name('bagihasil_per');
    });

    Route::group(array('prefix'=>'beban_gaji'), function(){
        Route::get('/', 'Cabang\BebanGajiController@main')->name('beban_gaji');
    });


    Route::group(array('prefix'=>'obat'), function() {
        Route::group(array('prefix'=>'stok_obat'), function(){
            Route::get('/',           'Cabang\ObatController@main')->name('obat');
            Route::post('/datagrid',  'Cabang\ObatController@datagrid')->name('datagridObat');
            Route::post('/formAdd',   'Cabang\ObatController@formAdd')->name('formAddObat');
            Route::post('/add',       'Cabang\ObatController@add')->name('addObat');
            Route::post('/show',      'Cabang\ObatController@show')->name('showObat');
            Route::post('/update',    'Cabang\ObatController@update')->name('updateObat');
            Route::post('/do_update', 'Cabang\ObatController@do_update')->name('doupdateObat');
            Route::post('/delete',    'Cabang\ObatController@delete')->name('deleteObat');
            Route::get('/lokal',    'Cabang\ObatController@lokal')->name('obatLokal');
            Route::post('/addObatLokal', 'Cabang\ObatController@addObatLokal')->name('addObatLokal');
        });

        Route::group(array('prefix'=>'permintaan_obat'), function(){
            Route::get('/',           'Cabang\PermintaanObatController@main')->name('permintaanObat');
            Route::post('/datagrid',  'Cabang\PermintaanObatController@datagrid')->name('datagridPermintaan');
            Route::post('/formAdd',   'Cabang\PermintaanObatController@formAdd')->name('formAddPermintaan');
            Route::post('/add',       'Cabang\PermintaanObatController@add')->name('addPermintaan');
            Route::post('/show',      'Cabang\PermintaanObatController@show')->name('showPermintaan');
            Route::post('/update',    'Cabang\PermintaanObatController@update')->name('updatePermintaan');
            Route::post('/do_update', 'Cabang\PermintaanObatController@do_update')->name('doupdatePermintaan');

            Route::post('/delete', 'Cabang\PermintaanObatController@delete')->name('deletePermintaan');
            Route::post('/validasi', 'Cabang\PermintaanObatController@konfirmasi_permintaan')->name('val_permin_obat');
            Route::post('/reject', 'Cabang\PermintaanObatController@reject')->name('rejectPerminObat');
            Route::post('/konfirmasi','Cabang\PermintaanObatController@konfirmasi_pesanan')->name('konfirmasi_pesanan_obat');
        });
    });

    Route::group(array('prefix'=>'bhp'), function() {
        Route::group(array('prefix'=>'stok_bhp'), function(){
            Route::get('/',           'Cabang\BhpController@main')->name('bhp');
            Route::post('/datagrid',  'Cabang\BhpController@datagrid')->name('datagridBhp');
            Route::post('/formAdd',   'Cabang\BhpController@formAdd')->name('formAddBhp');
            Route::post('/add',       'Cabang\BhpController@add')->name('addBhp');
            Route::post('/show',      'Cabang\BhpController@show')->name('showBhp');
            Route::post('/update',    'Cabang\BhpController@update')->name('updateBhp');
            Route::post('/do_update', 'Cabang\BhpController@do_update')->name('doupdateBhp');
            Route::post('/delete',    'Cabang\BhpController@delete')->name('deleteBhp');
            Route::get('/lokal',    'Cabang\BhpController@lokal')->name('bhpLokal');
            Route::post('/addBhpLokal',    'Cabang\BhpController@addBhpLokal')->name('addBhpLokal');
        });

        Route::group(array('prefix'=>'permintaan_bhp'), function(){
            Route::get('/',           'Cabang\PermintaanBhpController@main')->name('permintaanBhp');
            Route::post('/datagrid',  'Cabang\PermintaanBhpController@datagrid')->name('datagridPermintaanBhp');
            Route::post('/formAdd',   'Cabang\PermintaanBhpController@formAdd')->name('formAddPermintaanBhp');
            Route::post('/add',       'Cabang\PermintaanBhpController@add')->name('addPermintaanBhp');
            Route::post('/show',      'Cabang\PermintaanBhpController@show')->name('showPermintaanBhp');
            Route::post('/update',    'Cabang\PermintaanBhpController@update')->name('updatePermintaanBhp');
            Route::post('/do_update', 'Cabang\PermintaanBhpController@do_update')->name('doupdatePermintaan');
            Route::post('/validasi', 'Cabang\PermintaanBhpController@konfirmasi_permintaan')->name('val_permin_bhp');
            Route::post('/konfirmasi','Cabang\PermintaanBhpController@konfirmasi_pesanan')->name('konfirmasi_pesanan_bhp');
            Route::post('/reject','Cabang\PermintaanBhpController@reject')->name('rejectPerminBhp');

            Route::post('/delete','Cabang\PermintaanBhpController@delete')->name('deletePerminBhp');
     
        });
    });

    Route::group(array('prefix'=>'pengguna'), function(){
        Route::group(array('prefix'=>'admin'), function(){
            Route::get('/',  'Pengguna\AdminController@main')->name('admin');
            Route::post('/datagrid',  'Pengguna\AdminController@datagrid')->name('datagridAdmin');
            Route::post('/formAdd',  'Pengguna\AdminController@formAdd')->name('formAddAdmin');
            Route::post('/add',  'Pengguna\AdminController@add')->name('addAdmin');
            Route::post('/show',  'Pengguna\AdminController@show')->name('showAdmin');
            Route::post('/update',  'Pengguna\AdminController@update')->name('updateAdmin');
            Route::post('/do_update',  'Pengguna\AdminController@do_update')->name('doupdateAdmin');
            Route::post('/delete',  'Pengguna\AdminController@delete')->name('deleteAdmin');
            Route::post('/nonActive', 'Pengguna\AdminController@nonActive')->name('nonActiveAdmin');
            Route::post('/active', 'Pengguna\AdminController@active')->name('activeAdmin');

        });
        Route::group(array('prefix'=>'owner'), function(){
            Route::get('/',  'Pengguna\OwnerController@main')->name('owner');
            Route::post('/datagrid',  'Pengguna\OwnerController@datagrid')->name('datagridOwner');
            Route::post('/formAdd',  'Pengguna\OwnerController@formAdd')->name('formAddOwner');
            Route::post('/add',  'Pengguna\OwnerController@add')->name('addOwner');
            Route::post('/show',  'Pengguna\OwnerController@show')->name('showOwner');
            Route::post('/update',  'Pengguna\OwnerController@update')->name('updateOwner');
            Route::post('/do_update',  'Pengguna\OwnerController@do_update')->name('doupdateOwner');
            Route::post('/delete',  'Pengguna\OwnerController@delete')->name('deleteOwner');
            Route::post('/nonActive', 'Pengguna\OwnerController@nonActive')->name('nonActiveOwner');
            Route::post('/active', 'Pengguna\OwnerController@active')->name('activeOwner');

        });
        Route::group(array('prefix'=>'kepala_cabang'), function(){
            Route::get('/',  'Pengguna\KepalaCabangController@main')->name('kepala_cabang');
            Route::post('/datagrid',  'Pengguna\KepalaCabangController@datagrid')->name('datagridKepala_cabang');
            Route::post('/formAdd',  'Pengguna\KepalaCabangController@formAdd')->name('formAddKepala_cabang');
            Route::post('/add',  'Pengguna\KepalaCabangController@add')->name('addKepala_cabang');
            Route::post('/show',  'Pengguna\KepalaCabangController@show')->name('showKepala_cabang');
            Route::post('/update',  'Pengguna\KepalaCabangController@update')->name('updateKepala_cabang');
            Route::post('/do_update',  'Pengguna\KepalaCabangController@do_update')->name('doupdateKepala_cabang');
            Route::post('/delete',  'Pengguna\KepalaCabangController@delete')->name('deleteKepala_cabang');
            Route::post('/nonActive', 'Pengguna\KepalaCabangController@nonActive')->name('nonActiveKepala_cabang');
            Route::post('/active', 'Pengguna\KepalaCabangController@active')->name('activeKepala_cabang');


        });
        Route::group(array('prefix'=>'operator'), function(){
            Route::get('/',  'Pengguna\OperatorController@main')->name('operator');
            Route::post('/datagrid',  'Pengguna\OperatorController@datagrid')->name('datagridOperator');
            Route::post('/formAdd',  'Pengguna\OperatorController@formAdd')->name('formAddOperator');
            Route::post('/add',  'Pengguna\OperatorController@add')->name('addOperator');
            Route::post('/show',  'Pengguna\OperatorController@show')->name('showOperator');
            Route::post('/update',  'Pengguna\OperatorController@update')->name('updateOperator');
            Route::post('/do_update',  'Pengguna\OperatorController@do_update')->name('doupdateOperator');
            Route::post('/delete',  'Pengguna\OperatorController@delete')->name('deleteOperator');
            Route::post('/nonActive', 'Pengguna\OperatorController@nonActive')->name('nonActiveOperator');
            Route::post('/active', 'Pengguna\OperatorController@active')->name('activeOperator');

        });
        Route::group(array('prefix'=>'bendahara'), function(){
            Route::get('/',  'Pengguna\BendaharaController@main')->name('bendahara');
            Route::post('/datagrid',  'Pengguna\BendaharaController@datagrid')->name('datagridBendahara');
            Route::post('/formAdd',  'Pengguna\BendaharaController@formAdd')->name('formAddBendahara');
            Route::post('/add',  'Pengguna\BendaharaController@add')->name('addBendahara');
            Route::post('/show',  'Pengguna\BendaharaController@show')->name('showBendahara');
            Route::post('/update',  'Pengguna\BendaharaController@update')->name('updateBendahara');
            Route::post('/do_update',  'Pengguna\BendaharaController@do_update')->name('doupdateBendahara');
            Route::post('/delete',  'Pengguna\BendaharaController@delete')->name('deleteBendahara');
            Route::post('/nonActive', 'Pengguna\BendaharaController@nonActive')->name('nonActiveBendahara');
            Route::post('/active', 'Pengguna\BendaharaController@active')->name('activeBendahara');
        });
        Route::group(array('prefix'=>'marketing'), function(){
            Route::get('/',  'Pengguna\MarketingController@main')->name('marketing');
            Route::post('/datagrid',  'Pengguna\MarketingController@datagrid')->name('datagridMarketing');
            Route::post('/formAdd',  'Pengguna\MarketingController@formAdd')->name('formAddMarketing');
            Route::post('/add',  'Pengguna\MarketingController@add')->name('addMarketing');
            Route::post('/show',  'Pengguna\MarketingController@show')->name('showMarketing');
            Route::post('/update',  'Pengguna\MarketingController@update')->name('updateMarketing');
            Route::post('/do_update',  'Pengguna\MarketingController@do_update')->name('doupdateMarketing');
            Route::post('/delete',  'Pengguna\MarketingController@delete')->name('deleteMarketing');
            Route::post('/nonActive', 'Pengguna\MarketingController@nonActive')->name('nonActiveMarketing');
            Route::post('/active', 'Pengguna\MarketingController@active')->name('activeMarketing');
        });
    });

    Route::group(array('prefix'=>'data_master'), function(){
        Route::group(array('prefix'=>'daftar_cabang'), function(){
            Route::get('/',  'DaftarCabangController@main')->name('daftar_cabang');
            Route::post('/datagrid',  'DaftarCabangController@datagrid')->name('datagridDaftarcabang');
            Route::post('/formAdd',  'DaftarCabangController@formAdd')->name('formAddDaftarcabang');
            Route::post('/add',  'DaftarCabangController@add')->name('addDaftarcabang');
            Route::post('/show',  'DaftarCabangController@show')->name('showDaftarcabang');
            Route::post('/delete',  'DaftarCabangController@delete')->name('deleteDaftarcabang');
        });

        Route::group(array('prefix'=>'daftar_mainan'), function(){
            Route::get('/',  'DaftarMainanController@main')->name('daftar_mainan');
            Route::post('/datagrid',  'DaftarMainanController@datagrid')->name('datagridDaftarmainan');
            Route::post('/formAdd',  'DaftarMainanController@formAdd')->name('formAddDaftarmainan');
            Route::post('/add',  'DaftarMainanController@add')->name('addDaftarmainan');
            Route::post('/show',  'DaftarMainanController@show')->name('showDaftarmainan');
            Route::post('/delete',  'DaftarMainanController@delete')->name('deleteDaftarmainan');
        });

        Route::group(array('prefix'=>'metode_sunat'), function(){
            Route::get('/',  'MetodeSunatController@main')->name('metode_sunat');
            Route::post('/datagrid',  'MetodeSunatController@datagrid')->name('datagridMetodesunat');
            Route::post('/formAdd',  'MetodeSunatController@formAdd')->name('formAddMetodesunat');
            Route::post('/add',  'MetodeSunatController@add')->name('AddMetodesunat');
            Route::post('/show',  'MetodeSunatController@show')->name('showMetodesunat');
            Route::post('/delete',  'MetodeSunatController@delete')->name('deleteMetodesunat');
        });

        Route::group(array('prefix'=>'gaji'), function(){
            Route::get('/',  'MasterGajiController@main')->name('gaji');
            Route::post('/datagrid',  'MasterGajiController@datagrid')->name('datagridGaji');
            Route::post('/formAdd',  'MasterGajiController@formAdd')->name('formAddGaji');
            Route::post('/edit',  'MasterGajiController@edit')->name('formUpdateGaji');
            Route::post('/add',  'MasterGajiController@add')->name('addGaji');
            Route::post('/update',  'MasterGajiController@update')->name('updateGaji');
            Route::post('/show',  'MasterGajiController@show')->name('showGaji');
            Route::post('/delete',  'MasterGajiController@delete')->name('deleteGaji');
        });
    });

    Route::group(array('prefix'=>'pen_debetan'), function(){
        Route::group(array('prefix'=>'pengeluaran'), function(){
            Route::get('/',  'Cabang\PengeluaranController@main')->name('pengeluaran');
            Route::post('/datagridPengeluaran',  'Cabang\PengeluaranController@datagrid')->name('datagridPengeluaran');
            Route::post('/formAddExp',  'Cabang\PengeluaranController@formAdd')->name('formAddPengeluaran');
            Route::post('/addPengeluaran',  'Cabang\PengeluaranController@addPengeluaran')->name('addPengeluaran');
            Route::post('/formEditPengeluaran','Cabang\PengeluaranController@formEditPengeluaran')->name('formEditPengeluaran');
            Route::post('/detail_exp','Cabang\PengeluaranController@detailPeng')->name('detail_exp');
            Route::post('/search_exp','Cabang\PengeluaranController@searchPeng')->name('search_exp');
        });

        Route::group(array('prefix'=>'pemasukan'), function(){
            Route::get('/',  'Cabang\PemasukanController@main')->name('pemasukan');
            Route::post('/datagrid',  'Cabang\PemasukanController@datagrid')->name('datagridBiaya');
            Route::post('/detail_income','Cabang\PemasukanController@detailPem')->name('detail_income');
            Route::post('/search_income','Cabang\PemasukanController@searchPem')->name('search_income');
        });
    });

});
