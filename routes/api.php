<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
Route::post('/lupa_password', 'API\ProfileController@lupa_password');

Route::group(['middleware' => 'apiauth'], function(){
	Route::post('change_password', 'API\UserController@changePassword');
	Route::post('details', 'API\UserController@details');

	Route::group(array('prefix'=>'home'), function(){
		Route::post('/', 'API\HomeController@index');
        Route::post('/jadwal', 'API\HomeController@jadwal');
		Route::post('/chart', 'API\HomeController@chart');
	});

	Route::group(array('prefix'=>'bhp'), function(){
		Route::post('/', 'API\BahanHabisPakaiController@index');
	});

	Route::group(array('prefix'=>'obat'), function(){
		Route::post('/', 'API\ObatController@index');
	});

	Route::group(array('prefix'=>'fee_operator'), function(){
		Route::post('/', 'API\FeeOperatorController@index');
	});

    Route::group(array('prefix'=>'neraca'), function(){
        Route::post('/', 'API\NeracaController@index');
    });

     Route::group(array('prefix'=>'profile'), function(){
        Route::post('/', 'API\ProfileController@index');
        Route::post('/update', 'API\ProfileController@update');
        Route::post('/update_foto', 'API\ProfileController@UpdateFoto');
    });
});
